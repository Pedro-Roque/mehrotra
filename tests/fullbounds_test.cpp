#include <gtest/gtest.h>

#define MPC_NU 2
#define MPC_NX 4
#define MPC_HL 10
#define MPC_KU 2
#define MPC_HCU fullbounds
#define MPC_HCX simplebounds
#define MPC_HCT simplebounds
#define MPC_P PDMat

#include "mehrotra.hpp"

#include "ops.hpp"

using namespace mehrotra;

template <class float_t> class data : public ::testing::Test {
public:
  static constexpr size_t nx{MPC_NX}, nu{MPC_NU}, T{MPC_HL};
  static constexpr size_t ku{MPC_KU}, kx{MPC_KX}, kt{MPC_KT};

  std::array<float_t, nx * nx> A{0.1, 0.2, 0.1, 0.1, -0.1, 0.4, -0.2, -0.5,
                                 1,   0.9, 0.1, 0.2, 0,    0,   -0.1, -0.1};
  std::array<float_t, nu * nx> B{1, 0, 1, 0, 0, 1, 0, 1};
  std::array<float_t, nx> x0{1, 1, 1, 1};

  GeMat<float_t, ku, nu> Fu{std::array<float_t, ku * nu>{1, -1, 1, -1}};
  std::array<float_t, ku> fu{1, -0.6};

  std::array<float_t, nu> ubu{5, 5}, lbu{0, 0};
  std::array<float_t, nx> ubx{2.5, 2.5, 2.5, 2.5}, lbx{0, 0, 0, 0};

  std::array<float_t, nu> R{0.5, 5};
  std::array<float_t, nx> Q{0.1, 1, 10, 100};
  std::array<float_t, nx * nx> P;

  mpcdata<float_t, uint16_t> MyData;

  data() {
    MyData.A = A;
    MyData.B = B;
    MyData.R = R;
    MyData.Q = Q;
    P.fill(1);
    for (size_t idx = 0; idx < nx; idx++) {
      P[idx + nx * idx] += 8;
    }
    MyData.P = P;
    MyData.Fu = Fu;
    MyData.fu = fu;
    MyData.lbu = lbu;
    MyData.ubu = ubu;
    MyData.lbx = lbx;
    MyData.ubx = ubx;
    MyData.lbt = lbx;
    MyData.ubt = ubx;
  }
  void SetUp() {}
  void TearDown() {}
  ~data() {}
};

typedef ::testing::Types<float, double> MyTypes;

TYPED_TEST_SUITE(data, MyTypes,);

TYPED_TEST(data, empty_constructor) {
  solver<TypeParam, size_t> EmptyMPC;
  Ops<TypeParam>::expect_equal(EmptyMPC.fea_tol, 1e-2);
}

TYPED_TEST(data, constructor) {
  std::array<TypeParam, MPC_NU> u, uref{2.5, 2.5};
  std::array<TypeParam, MPC_NX> x, xref{1.25, 1.25, 1.25, 1.25};
  solver<TypeParam, uint16_t> MyMPC(this->MyData);
  for (size_t step = 0; step < MPC_HL; step++) {
    u = MyMPC.get_input(step);
    Ops<TypeParam>::expect_equal(u, uref);
  }
  for (size_t step = 1; step <= MPC_HL; step++) {
    x = MyMPC.get_state(step);
    Ops<TypeParam>::expect_equal(x, xref);
  }
}

TYPED_TEST(data, solve) {
  solver<TypeParam, uint16_t> MyMPC(this->MyData);
  MyMPC.set_x0(this->x0);
  MyMPC.solve();

  std::array<TypeParam, MPC_NX> x{0.9, 0, 2.4, 0};
  Ops<TypeParam>::expect_equal(MyMPC.get_state(), x);
  x = std::array<TypeParam, MPC_NX>{0.6489524046979347, 0.2571213659175377,
                                    0.841912149452887, 0.02356952605028613};

  std::array<TypeParam, MPC_NU> u{0.4, 0.2};
  Ops<TypeParam>::expect_equal(MyMPC.get_input(), u);

  u = std::array<TypeParam, MPC_NU>{0.48641149798615874, 0.11358850201621276};
  Ops<TypeParam>::expect_equal(MyMPC.get_input(MPC_HL - 1), u);
}

TYPED_TEST(data, initializer_solve) {
  solver<TypeParam, uint16_t> MyMPC{};
  MyMPC.initialize(this->MyData);
  MyMPC.set_x0(this->x0);
  MyMPC.solve();

  std::array<TypeParam, MPC_NX> x{0.9, 0, 2.4, 0};
  Ops<TypeParam>::expect_equal(MyMPC.get_state(), x);
  x = std::array<TypeParam, MPC_NX>{0.6489524046979347, 0.2571213659175377,
                                    0.841912149452887, 0.02356952605028613};

  std::array<TypeParam, MPC_NU> u{0.4, 0.2};
  Ops<TypeParam>::expect_equal(MyMPC.get_input(), u);

  u = std::array<TypeParam, MPC_NU>{0.48641149798615874, 0.11358850201621276};
  Ops<TypeParam>::expect_equal(MyMPC.get_input(MPC_HL - 1), u);
}

int main(int argc, char *argv[]) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
