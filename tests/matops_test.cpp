#include <gtest/gtest.h>

#include "mehrotra.hpp"
#include "ops.hpp"

using namespace mehrotra;

template <class T> class vector : public ::testing::Test {
public:
  static constexpr size_t N{5}, M{3};
  std::array<T, N> x, y, z, d{1, 2, 3, 4, 5};
  std::array<T, M * M> p{1, 2, 3, 2, 4, 5, 3, 5, 6};
  std::array<T, M * M> g{7, 8, 6, 5, 0, -1, -2, 0, 9.5};
  DiagMat<T, N> D;
  PDMat<T, M> P;
  GeMat<T, M, M> G;
  vector()
      : x{1, 1, 1, 1, 1}, y{-1, -2, -3, -4, -5}, z{5, 5, 6, 7, 9}, D{d}, P{p},
        G{g} {}
  void SetUp() {}
  void TearDown() {}
  ~vector() {}
};

template <class T> class colesky : public ::testing::Test {
public:
  static constexpr size_t N{4};
  PDMat<T, N> P1, P2, P3, P4;
  std::array<T, N> x;
  colesky()
      : P1{std::array<T, N * N>{6, 4, 4, -1, 4, 7, 4, 2, 4, 4, 5, 0, -1, 2, 0,
                                2}},
        P2{P1}, P3{std::array<T, N *(N + 1) / 2>{
                    2.449489742783178, 1.6329931618554523, 1.6329931618554523,
                    -0.4082482904638631, 2.0816659994661326, 0.6405126152203482,
                    1.2810252304406973, 1.3867504905630728,
                    -0.11094003924504543, 0.4242640687119279}},
        P4{std::array<T, N *(N + 1) / 2>{
            2.6457513110645907, 2.267786838055363, 2.6457513110645903,
            -1.889822365046136, 2.420153478013917, 1.6527877410826752,
            -0.7083376033211467, 2.065984676353344, -2.8215447865625656,
            2.442481174075716}},
        x{1, 2, 3, -4} {}
  void SetUp() {}
  void TearDown() {}
  ~colesky() {}
};

template <class T> class rank : public ::testing::Test {
public:
  static constexpr size_t N{3}, M{2};
  const std::array<T, N *(N + 1) / 2> p{1, 2, 3, 4, 5, 6};
  const std::array<T, N> v{7, 17, 24.2};
  PDMat<T, N> P;
  const std::array<T, N * M> g1{0.8, 7, 0.7, -1.2, 9, 23},
      g2{-9, 0.95, 0.91, 5.4, 45, 6.7};
  const GeMat<T, N, M> G1{g1};
  const GeMat<T, M, N> G2{g2};

  const T alpha{0.787};
  const std::array<T, M> Alpha{0.3, 0.9};
  const std::array<T, M> Alpha2{0.3, 0.9};

  const std::array<T, N *(N + 1) / 2> r1{39.563,  95.653,   136.3178,
                                         231.443, 328.7718, 466.89868},
      r2{2.63696,  -2.0923999999999987, -18.28048, 106.31,
         171.7653, 422.70862999999997},
      r3{65.4572675, -0.4082199999999996, -310.725745, 27.600634700000004,
         65.70131,   1635.00343},
      r4{2.488, -6.039999999999999, -21.672,
         91.6,  192.76999999999998, 482.24699999999996},
      r5{26.112249999999996, 4.16,   -112.7715, 30.492430000000002,
         49.84700000000001,  653.901};

  rank() : P{p} {}
  void SetUp() {}
  void TearDown() {}
  ~rank() {}
};

typedef ::testing::Types<float, double> MyTypes;

TYPED_TEST_SUITE(vector, MyTypes,);
TYPED_TEST_SUITE(colesky, MyTypes,);
TYPED_TEST_SUITE(rank, MyTypes,);

TYPED_TEST(vector, blas1) {
  TypeParam alpha{0.987}, beta{-1};
  std::array<TypeParam, TestFixture::N> x0{this->x};
  detail::MatOps<TypeParam>::vecsum(alpha, this->y, this->x);
  for (size_t idx = 0; idx < TestFixture::N; idx++) {
    Ops<TypeParam>::expect_equal(this->x[idx], alpha * this->y[idx] + x0[idx]);
  }
  x0 = this->x;
  detail::MatOps<TypeParam>::vecsum(beta, this->z, this->x);
  for (size_t idx = 0; idx < TestFixture::N; idx++) {
    Ops<TypeParam>::expect_equal(this->x[idx], beta * this->z[idx] + x0[idx]);
  }

  TypeParam r{detail::MatOps<TypeParam>::dotproduct(this->y, this->z)};
  TypeParam r2{0};
  for (size_t idx = 0; idx < TestFixture::N; idx++) {
    r2 += this->y[idx] * this->z[idx];
  }
  Ops<TypeParam>::expect_equal(r, r2);
}

TYPED_TEST(vector, blas2) {
  const size_t m{TestFixture::M}, n{TestFixture::N};

  TypeParam alpha{0.987}, beta{-1};
  std::array<TypeParam, m> x{1, 1, 1}, y{-1, -2, -3}, x2{0}, y2{y};
  const std::array<TypeParam, m> v1{5.922, 10.857, 13.817999999999998},
      v2{11.0, 8, 32.5}, v3{-40.467, -1.9739999999999998, -26.1555};
  std::array<TypeParam, n> z{9, 10, 11, 12, 13}, z2{z};

  detail::MatOps<TypeParam>::matvec(alpha, this->D, this->x, beta, z);
  for (size_t idx = 0; idx < n; idx++)
    Ops<TypeParam>::expect_equal(
        z[idx], alpha * this->D.mat[idx] * this->x[idx] + beta * z2[idx]);

  detail::MatOps<TypeParam>::matvec(alpha, this->P, x, beta, x2);
  for (size_t idx = 0; idx < TestFixture::M; idx++) {
    Ops<TypeParam>::expect_equal(x2[idx], v1[idx]);
  }

  detail::MatOps<TypeParam>::template matvec<'N'>(beta, this->G, y, 0., y2);
  for (size_t idx = 0; idx < m; idx++) {
    Ops<TypeParam>::expect_equal(y2[idx], v2[idx]);
  }
  detail::MatOps<TypeParam>::template matvec<'T'>(alpha, this->G, y, 0., y2);
  for (size_t idx = 0; idx < m; idx++) {
    Ops<TypeParam>::expect_equal(y2[idx], v3[idx]);
  }
}

TYPED_TEST(vector, backsolve) {
  std::array<TypeParam, TestFixture::M> rhs{0.98, 7.65, 1.22}, r1{rhs}, r2{rhs};
  const std::array<TypeParam, TestFixture::M> v1{0.98, 1.4225,
                                                 -1.4720833333333332},
      v2{-2.9466666666666668, 1.6583333333333334, 0.2033333333333333};

  std::array<TypeParam, TestFixture::M * 2> g1{9, 7, -5.5, 4, 0, 1};
  GeMat<TypeParam, TestFixture::M, 2> G1{g1}, G2{g1};
  const std::array<TypeParam, TestFixture::M * 2> V1{
      9, -2.75, -3.125, 4, -2, -0.16666666666666666},
      V2{5.958333333333334,  2.895833333333333,    -0.9166666666666666,
         3.9166666666666665, -0.20833333333333331, 0.16666666666666666};

  detail::MatOps<TypeParam>::backsolve('N', this->P, r1);
  detail::MatOps<TypeParam>::backsolve('T', this->P, r2);

  for (size_t idx = 0; idx < TestFixture::M; idx++) {
    Ops<TypeParam>::expect_equal(r1[idx], v1[idx]);
    Ops<TypeParam>::expect_equal(r2[idx], v2[idx]);
  }

  detail::MatOps<TypeParam>::backsolve('N', this->P, G1);
  detail::MatOps<TypeParam>::backsolve('T', this->P, G2);

  for (size_t idx = 0; idx < TestFixture::M; idx++) {
    Ops<TypeParam>::expect_equal(G1.mat[idx], V1[idx]);
    Ops<TypeParam>::expect_equal(G2.mat[idx], V2[idx]);
  }
}

TYPED_TEST(colesky, blas1) {
  detail::MatOps<TypeParam>::cholfact(this->P1);
  
  for (size_t idx = 0; idx < TestFixture::N; idx++)
    Ops<TypeParam>::expect_equal(this->P1.mat[idx], this->P3.mat[idx]);

  detail::MatOps<TypeParam>::cholupdate(this->P1, this->x);
  detail::MatOps<TypeParam>::cholfact(this->P2);
  detail::MatOps<TypeParam>::cholupdate2(this->P2, this->x);
  for (size_t idx = 0; idx < TestFixture::N; idx++) {
    Ops<TypeParam>::expect_equal(this->P1.mat[idx], this->P4.mat[idx]);
    Ops<TypeParam>::expect_equal(this->P2.mat[idx], this->P4.mat[idx]);
  }
}

TYPED_TEST(colesky, invert) {
  const std::array<TypeParam, 10> r{
      2.0000000000000067, -2.0000000000000067, -2.0014830212433607e-16,
      3.00000000000001,   2.555555555555562,   -0.44444444444444414,
      -3.555555555555565, 0.5555555555555555,  0.44444444444444414,
      5.5555555555555705};
  detail::MatOps<TypeParam>::cholfact(this->P1);
  detail::MatOps<TypeParam>::invert(this->P1);
  Ops<TypeParam>::expect_equal(this->P1.mat, r, 1e-10);
}

TYPED_TEST(colesky, diagsum) {
  std::array<TypeParam, TestFixture::N> d1{8, 8, 8, 8}, d2{0.1, 0.2, 0.3, 0.4};
  const std::array<TypeParam, TestFixture::N *(TestFixture::N + 1) / 2> r1{
      6.1, 4, 4, -1, 7.2, 4, 2, 5.3, 2.4},
      r2{14.2, 4, 4, -1, 15.4, 4, 2, 13.6, 10.8};

  detail::MatOps<TypeParam>::diagsum(this->P1, d2);
  for (size_t idx = 0; idx < TestFixture::N; idx++) {
    Ops<TypeParam>::expect_equal(this->P1.mat[idx], r1[idx]);
  }
  detail::MatOps<TypeParam>::diagsum(this->P1, d1, d2);
  for (size_t idx = 0; idx < TestFixture::N; idx++) {
    Ops<TypeParam>::expect_equal(this->P1.mat[idx], r2[idx]);
  }
}

TYPED_TEST(colesky, matsum) {
  detail::MatOps<TypeParam>::matsum(this->P1, this->P2);
  for (size_t idx = 0; idx < TestFixture::N; idx++) {
    Ops<TypeParam>::expect_equal(this->P1.mat[idx], 2 * this->P2.mat[idx]);
  }

  DiagMat<TypeParam, TestFixture::N> D{
      std::array<TypeParam, TestFixture::N>{1, 2, 3, 4}};
  detail::MatOps<TypeParam>::matsum(this->P1, D);
  const std::array<TypeParam, TestFixture::N * TestFixture::N> r{
      13, 8, 8, -2, 8, 16, 8, 4, 8, 8, 13, 0, -2, 4, 0, 8};
  for (size_t idx = 0; idx < TestFixture::N; idx++) {
    Ops<TypeParam>::expect_equal(this->P1.mat[idx], r[idx]);
  }
}

TYPED_TEST(rank, rank1) {
  detail::MatOps<TypeParam>::rank1(this->alpha, this->P, this->v);
  for (size_t idx = 0; idx < this->P.mat.size(); idx++) {
    Ops<TypeParam>::expect_equal(this->P.mat[idx], this->r1[idx]);
  }
}

TYPED_TEST(rank, rankk) {
  detail::MatOps<TypeParam>::rankk(this->alpha, this->P, this->G1);
  for (size_t idx = 0; idx < this->P.mat.size(); idx++) {
    Ops<TypeParam>::expect_equal(this->P.mat[idx], this->r2[idx]);
  }
}

TYPED_TEST(rank, rank_vector) {
  detail::MatOps<TypeParam>::rankk(this->Alpha, this->P, this->G1);
  for (size_t idx = 0; idx < this->P.mat.size(); idx++) {
    Ops<TypeParam>::expect_equal(this->P.mat[idx], this->r4[idx]);
  }
}

TYPED_TEST(rank, rankkt) {
  detail::MatOps<TypeParam>::rankkt(this->alpha, this->P, this->G2);
  Ops<TypeParam>::expect_equal(this->P.mat, this->r3, 1e-10);
}

TYPED_TEST(rank, rankkt_vector) {
  detail::MatOps<TypeParam>::rankkt(this->Alpha2, this->P, this->G2);
  for (size_t idx = 0; idx < this->P.mat.size(); idx++) {
    Ops<TypeParam>::expect_equal(this->P.mat[idx], this->r5[idx]);
  }
}

TYPED_TEST(rank, transpose) {
  GeMat<TypeParam, TestFixture::N, TestFixture::M> T1{this->G1};
  GeMat<TypeParam, TestFixture::M, TestFixture::N> T2{this->G2};

  detail::transpose(T1);
  detail::transpose(T2);

  for (size_t row = 0; row < TestFixture::N; row++) {
    for (size_t col = 0; col < TestFixture::M; col++) {
      Ops<TypeParam>::expect_equal(this->G1.mat[row + TestFixture::N * col],
                                   T1.mat[col + TestFixture::M * row]);
    }
  }

  for (size_t row = 0; row < TestFixture::M; row++) {
    for (size_t col = 0; col < TestFixture::N; col++) {
      Ops<TypeParam>::expect_equal(this->G2.mat[row + TestFixture::M * col],
                                   T2.mat[col + TestFixture::N * row]);
    }
  }
}

int main(int argc, char *argv[]) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
