#include <gtest/gtest.h>

#define MPC_NU 2
#define MPC_NX 4
#define MPC_HL 10
#define MPC_KU 4
#define MPC_HCU linearbounds
#define MPC_SCX simplebounds
#define MPC_SCT simplebounds
#define MPC_P PDMat

#include "mehrotra.hpp"

#include "ops.hpp"

using namespace mehrotra;

template <class float_t> class data : public ::testing::Test {
public:
  static constexpr size_t nx{MPC_NX}, nu{MPC_NU}, T{MPC_HL};
  static constexpr size_t ku{MPC_KU};

  std::array<float_t, nx * nx> A{1,  2, 1, 1, -1, 4, 2, -5,
                                 10, 9, 1, 2, 0,  0, 1, -1};
  std::array<float_t, nu * nx> B{1, 0, 1, 0, 0, 1, 0, 1};
  std::array<float_t, nx> x0{0.5, 0.5, 0.5, 0.5};

  GeMat<float_t, ku, nu> Fu{
      std::array<float_t, ku * nu>{1, 0, -1, 0, 0, 1, 0, -1}};
  std::array<float_t, ku> fu{5, 5, 0, 0};

  std::array<float_t, nx> ubx{10, 10, 10, 10}, lbx{-10, -10, -10, -10};

  std::array<float_t, nu> R{0.5, 5};
  std::array<float_t, nx> Q{0.1, 1, 10, 100};
  std::array<float_t, nx * nx> P;

  mpcdata<float_t, uint16_t> MyData;

  data() {
    std::transform(A.cbegin(), A.cend(), A.begin(),
                   [](float_t a) { return a / 10; });
    MyData.A = A;
    MyData.B = B;
    MyData.R = R;
    MyData.Q = Q;
    P.fill(1);
    for (size_t idx = 0; idx < nx; idx++) {
      P[idx + nx * idx] += 8;
    }
    MyData.P = P;
    MyData.Fu = Fu;
    MyData.fu = fu;
    MyData.subx = ubx;
    MyData.slbx = lbx;
    MyData.subt = ubx;
    MyData.slbt = lbx;
  }
  void SetUp() {}
  void TearDown() {}
  ~data() {}
};

typedef ::testing::Types<float, double> MyTypes;

TYPED_TEST_SUITE(data, MyTypes,);

TYPED_TEST(data, empty_constructor) {
  solver<TypeParam, size_t> EmptyMPC;
  Ops<TypeParam>::expect_equal(EmptyMPC.fea_tol, 1e-2);
}

TYPED_TEST(data, constructor) {
  std::array<TypeParam, MPC_NU> u, uref{};
  std::array<TypeParam, MPC_NX> x, xref{};
  solver<TypeParam, uint16_t> MyMPC(this->MyData);
  for (size_t step = 0; step < MPC_HL; step++) {
    u = MyMPC.get_input(step);
    for (size_t idx = 0; idx < u.size(); idx++) {
      Ops<TypeParam>::expect_equal(u[idx], uref[idx]);
    }
  }
  for (size_t step = 0; step < MPC_HL; step++) {
    x = MyMPC.get_state(step);
    for (size_t idx = 0; idx < x.size(); idx++) {
      Ops<TypeParam>::expect_equal(x[idx], xref[idx]);
    }
  }
}

TYPED_TEST(data, initializer) {
  std::array<TypeParam, MPC_NU> u, uref{};
  std::array<TypeParam, MPC_NX> x, xref{};
  solver<TypeParam, uint16_t> MyMPC;
  MyMPC.initialize(this->MyData);
  for (size_t step = 0; step < MPC_HL; step++) {
    u = MyMPC.get_input(step);
    for (size_t idx = 0; idx < u.size(); idx++) {
      Ops<TypeParam>::expect_equal(u[idx], uref[idx]);
    }
  }
  for (size_t step = 0; step < MPC_HL; step++) {
    x = MyMPC.get_state(step);
    for (size_t idx = 0; idx < x.size(); idx++) {
      Ops<TypeParam>::expect_equal(x[idx], xref[idx]);
    }
  }
}

TYPED_TEST(data, eq_residual) {
  const std::array<TypeParam, MPC_NX> re0{0.25, 0, 1.1, 0}, re1{};

  solver<TypeParam, uint16_t> MyMPC(this->MyData);
  MyMPC.set_x0(this->x0);
  MyMPC.equality_residual();
  auto re = MyMPC.nu_aff[0];
  Ops<TypeParam>::expect_equal(re, re0, 1e-8);
  for (size_t step = 1; step < MPC_HL; step++) {
    re = MyMPC.nu_aff[step];
    Ops<TypeParam>::expect_equal(re, re1, 1e-8);
  }
}

TYPED_TEST(data, solve) {
  solver<TypeParam, uint16_t> MyMPC(this->MyData);
  MyMPC.set_x0(this->x0);
  MyMPC.set_optimality_tolerance(1e-7);
  MyMPC.set_feasibility_tolerance(1e-7);
  MyMPC.solve();

  const std::array<TypeParam, MPC_NX> nu{5.3166845630249195, 8.9751729655173,
                                         14.53111728399386,
                                         -3.0965597381977465};
  Ops<TypeParam>::expect_equal(MyMPC.nu[0], nu);

  const std::array<TypeParam, MPC_NX> x{
      0.2500002350554479, 2.350554854076226e-7, 1.1000002863213076,
      2.863214727537976e-7};
  Ops<TypeParam>::expect_equal(MyMPC.x[0], x);

  const std::array<TypeParam, MPC_NU> u{7.077532485077268e-10,
                                        8.818519562921206e-10};
  Ops<TypeParam>::expect_equal(MyMPC.u[0], u);
}

TYPED_TEST(data, soft_solve) {
  solver<TypeParam, uint16_t> MyMPC(this->MyData);
  std::array<TypeParam, MPC_NX> x0{10, 10, 10, 10};
  MyMPC.set_x0(x0);
  MyMPC.set_optimality_tolerance(1e-7);
  MyMPC.set_feasibility_tolerance(1e-3);
  MyMPC.solve();

  const std::array<TypeParam, MPC_NX> x{
      5.000000000000319, 3.1860763223252986e-13, 22.00000000000041,
      4.0719955015507926e-13};
  Ops<TypeParam>::expect_equal(MyMPC.x[0], x);

  const std::array<TypeParam, MPC_NU> u{3.1860763223252986e-13,
                                        4.0719955015507926e-13};
  Ops<TypeParam>::expect_equal(MyMPC.u[0], u);
}

TYPED_TEST(data, initializer_solve) {
  solver<TypeParam, uint16_t> MyMPC{};
  MyMPC.initialize(this->MyData);
  MyMPC.set_x0(this->x0);
  MyMPC.solve();

  const std::array<TypeParam, MPC_NX> x{
      0.2500002350554479, 2.350554854076226e-7, 1.1000002863213076,
      2.863214727537976e-7};
  Ops<TypeParam>::expect_equal(MyMPC.x[0], x);
}

int main(int argc, char *argv[]) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
