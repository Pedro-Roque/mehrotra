#!/usr/bin/env bash
tests_dir=$(pwd)
compiled_test_name="test.out"
failed_tests_gcc=0
failed_tests_clang=0

function compile_test_gcc() {
    local file=$1
    local custom_blas=$2
    g++ ${file} -o ${tests_dir}/${compiled_test_name}   \
        -Wall -Wpedantic                                \
        -I ../include -lblas -llapack -lgtest           \
        -DMPC_GTEST_COMPILE ${custom_blas}
}

function compile_test_clang() {
    local file=$1
    local custom_blas=$2
    clang++ ${file} -o ${tests_dir}/${compiled_test_name}   \
        -Wall -Wpedantic                                    \
        -I ../include -lblas -llapack -lgtest               \
        -DMPC_GTEST_COMPILE ${custom_blas}
}

function compile_test(){
    if [[ $# -lt 2 ]]; then
        echo "compile_test expected at least 2 argument1. ${$#} arguments provided."
        exit 1
    fi
    local file=$1
    local compiler=$2
    local custom_blas=$3

    if [[ ${compiler} == "gcc" ]]; then
        compile_test_gcc ${file} ${custom_blas}
    else
        compile_test_clang ${file} ${custom_blas}
    fi
}

# Run tests with gcc
for file in ${tests_dir}/*; do
    if [[ -f ${file} ]] && [[ "${file}" == *_test.cpp ]]; then
        # Compile with openblas
        compile_test ${file} gcc
        if [[ $? -ne 0 ]]; then
            echo "Failed to compile ${file} with gcc"
            exit 2
        fi

        ./${compiled_test_name}
        failed_tests_gcc=$((failed_tests_gcc+$?))

        # Compile with custom blas
        compile_test ${file} gcc -DMPC_NO_BLAS
        if [[ $? -ne 0 ]]; then
            echo "Failed to compile ${file} with gcc and custom blas installation"
            exit 2
        fi

        ./${compiled_test_name}
        failed_tests_gcc=$((failed_tests_gcc+$?))
    fi
done

# Run tests with clang
for file in ${tests_dir}/*; do
    if [[ -f ${file} ]] && [[ "${file}" == *_test.cpp ]]; then
        # Compile with openblas
        compile_test ${file} clang
        if [[ $? -ne 0 ]]; then
            echo "Failed to compile ${file} with clang"
            exit 3
        fi

        ./${compiled_test_name}
        failed_tests_clang=$((failed_tests_clang+$?))

        # Compile with custom blas
        compile_test ${file} clang -DMPC_NO_BLAS
        if [[ $? -ne 0 ]]; then
            echo "Failed to compile ${file} with clang and custom blas installation"
            exit 3
        fi

        ./${compiled_test_name}
        failed_tests_clang=$((failed_tests_clang+$?))
    fi
done

echo -e "\n\n\n***********************************"
echo "TEST REPORT:"
echo -e "\t${failed_tests_gcc} tests failed to run with GCC"
echo -e "\t${failed_tests_clang} tests failed to run with CLANG"

rm test.out