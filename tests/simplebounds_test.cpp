#include <gtest/gtest.h>

#define MPC_NU 2
#define MPC_NX 4
#define MPC_HL 10

#define MPC_HCU simplebounds
#define MPC_HCX simplebounds
#define MPC_HCT simplebounds

#define MPC_P PDMat

#include "mehrotra.hpp"

#include "ops.hpp"

using namespace mehrotra;

template <class float_t> class data : public ::testing::Test {
public:
  static constexpr size_t nx{MPC_NX}, nu{MPC_NU}, T{MPC_HL};

  std::array<float_t, nx * nx> A{1,  2, 1, 1, -1, 4, 2, -5,
                                 10, 9, 1, 2, 0,  0, 1, -1};
  std::array<float_t, nu * nx> B{1, 0, 1, 0, 0, 1, 0, 1};
  std::array<float_t, nx> x0{0.5, 0.5, 0.5, 0.5};

  std::array<float_t, nu> ubu{5, 5}, lbu{};
  std::array<float_t, nx> ubx{10, 10, 10, 10}, lbx{-10, -10, -10, -10};
  std::array<float_t, nx> ubt{ubx}, lbt{lbx};

  std::array<float_t, nu> R{0.5, 5};
  std::array<float_t, nx> Q{0.1, 1, 10, 100};
  std::array<float_t, nx * nx> P;

  mpcdata<float_t, uint16_t> MyData;
  data() {
    std::transform(A.cbegin(), A.cend(), A.begin(),
                   [](float_t a) { return a / 10; });
    MyData.A = A;
    MyData.B = B;
    MyData.R = R;
    MyData.Q = Q;
    P.fill(1);
    for (size_t idx = 0; idx < nx; idx++) {
      P[idx + nx * idx] += 8;
    }
    MyData.P = P;
    MyData.ubu = ubu;
    MyData.lbu = lbu;
    MyData.ubx = ubx;
    MyData.lbx = lbx;
    MyData.ubt = ubx;
    MyData.lbt = lbx;
  }
  void SetUp() {}
  void TearDown() {}
  ~data() {}
};

typedef ::testing::Types<float, double> MyTypes;

TYPED_TEST_SUITE(data, MyTypes,);

TYPED_TEST(data, empty_constructor) {
  solver<TypeParam, size_t> EmptyMPC;
  Ops<TypeParam>::expect_equal(EmptyMPC.fea_tol, 1e-2);
}

TYPED_TEST(data, constructor) {
  std::array<TypeParam, MPC_NU> u, uref{2.5, 2.5};
  std::array<TypeParam, MPC_NX> x, xref{};
  solver<TypeParam, uint16_t> MyMPC(this->MyData);
  for (size_t step = 0; step < MPC_HL; step++) {
    u = MyMPC.get_input(step);
    for (size_t idx = 0; idx < u.size(); idx++) {
      Ops<TypeParam>::expect_equal(u[idx], uref[idx]);
    }
  }
  for (size_t step = 0; step < MPC_HL; step++) {
    x = MyMPC.get_state(step);
    for (size_t idx = 0; idx < x.size(); idx++) {
      Ops<TypeParam>::expect_equal(x[idx], xref[idx]);
    }
  }
}

TYPED_TEST(data, initializer) {
  std::array<TypeParam, MPC_NU> u, uref{2.5, 2.5};
  std::array<TypeParam, MPC_NX> x, xref{};
  solver<TypeParam, uint16_t> MyMPC;
  MyMPC.initialize(this->MyData);
  for (size_t step = 0; step < MPC_HL; step++) {
    u = MyMPC.get_input(step);
    for (size_t idx = 0; idx < u.size(); idx++) {
      Ops<TypeParam>::expect_equal(u[idx], uref[idx]);
    }
  }
  for (size_t step = 0; step < MPC_HL; step++) {
    x = MyMPC.get_state(step);
    for (size_t idx = 0; idx < x.size(); idx++) {
      Ops<TypeParam>::expect_equal(x[idx], xref[idx]);
    }
  }
}

TYPED_TEST(data, eq_residual) {
  const std::vector<TypeParam> re0{2.75, 2.5, 3.6, 2.5},
      re1{2.5, 2.5, 2.5, 2.5};

  solver<TypeParam, uint16_t> MyMPC(this->MyData);
  MyMPC.set_x0(this->x0);
  MyMPC.equality_residual();
  auto re = MyMPC.nu_aff[0];
  for (size_t idx = 0; idx < MPC_NX; idx++) {
    Ops<TypeParam>::expect_equal(re[idx], re0[idx]);
  }
  for (size_t step = 1; step < MPC_HL; step++) {
    re = MyMPC.nu_aff[step];
    for (size_t idx = 0; idx < MPC_NX; idx++) {
      Ops<TypeParam>::expect_equal(re[idx], re1[idx]);
    }
  }
}

TYPED_TEST(data, affine_step) {
  solver<TypeParam, uint16_t> MyMPC(this->MyData);
  MyMPC.set_x0(this->x0);
  MyMPC.duality_gap();
  Ops<TypeParam>::expect_equal(MyMPC.mu, 150);
  MyMPC.equality_residual();
  MyMPC.affine_step();

  const std::array<TypeParam, MPC_NU> u_aff{-2.61448283629856,
                                            -2.318325107907701};
  Ops<TypeParam>::expect_equal(MyMPC.u_aff[0], u_aff);

  const std::array<TypeParam, MPC_NX> nu_aff{
      14.733954501807844, 27.155012297118386, 28.45499148826375,
      7.729835777797996};
  Ops<TypeParam>::expect_equal(MyMPC.nu_aff[0], nu_aff);

  const std::array<TypeParam, MPC_NX> x_aff{
      0.13551716370143985, -0.11448283629856015, 1.281674892092297,
      0.18167489209229803};
  Ops<TypeParam>::expect_equal(MyMPC.x_aff[0], x_aff);
}

TYPED_TEST(data, linesearch) {
  solver<TypeParam, uint16_t> MyMPC(this->MyData);
  MyMPC.set_x0(this->x0);
  MyMPC.equality_residual();
  MyMPC.duality_gap();
  MyMPC.affine_step();
  MyMPC.linesearch();
  Ops<TypeParam>::expect_equal(MyMPC.alpha, 0.48880797531609166);
}

TYPED_TEST(data, centering) {
  solver<TypeParam, uint16_t> MyMPC(this->MyData);
  MyMPC.set_x0(this->x0);
  MyMPC.equality_residual();
  MyMPC.duality_gap();
  MyMPC.affine_step();
  MyMPC.linesearch();
  MyMPC.centering();
  Ops<TypeParam>::expect_equal(MyMPC.mu_aff, 73.19292663538286);
  EXPECT_NEAR(MyMPC.sigma, 0.11618058566847458, 1e-6);
}

TYPED_TEST(data, terminate) {
  solver<TypeParam, uint16_t> MyMPC(this->MyData);
  MyMPC.set_x0(this->x0);
  EXPECT_EQ(MyMPC.terminate(), false);
  EXPECT_EQ(MyMPC.k, 0);
  MyMPC.k = 15;
  MyMPC.set_max_iterations(15);
  EXPECT_EQ(MyMPC.terminate(), true);
}

TYPED_TEST(data, full_stepsize) {
  solver<TypeParam, uint16_t> MyMPC(this->MyData);
  MyMPC.set_x0(this->x0);
  MyMPC.equality_residual();
  MyMPC.duality_gap();
  Ops<TypeParam>::expect_equal(MyMPC.mu, 150);

  MyMPC.affine_step();
  MyMPC.linesearch();
  MyMPC.centering();
  MyMPC.cc_step();
  MyMPC.linesearch();

  EXPECT_NEAR(MyMPC.alpha, 0.9545959989063437, 1e-6);
}

TYPED_TEST(data, terminate_side_calculations) {
  solver<TypeParam, uint16_t> MyMPC(this->MyData);
  MyMPC.set_x0(this->x0);
  MyMPC.equality_residual();
  MyMPC.duality_gap();

  MyMPC.affine_step();
  MyMPC.linesearch();
  MyMPC.centering();
  MyMPC.cc_step();
  MyMPC.linesearch();
  MyMPC.take_step();

  MyMPC.terminate();

  EXPECT_NEAR(MyMPC.mu, 29.22991201742355, 1e-3);

  std::array<TypeParam, MPC_NX> eq_res{0.15111239297747947, 0.13737490270679953,
                                       0.19781985989779383,
                                       0.13737490270680033};
  Ops<TypeParam>::expect_equal(MyMPC.nu_aff[0], eq_res);
}

TYPED_TEST(data, solve) {
  solver<TypeParam, uint16_t> MyMPC(this->MyData);
  MyMPC.set_x0(this->x0);
  MyMPC.solve();

  const std::array<TypeParam, MPC_NX> x{
      0.2500002350554479, 2.350554854076226e-7, 1.1000002863213076,
      2.863214727537976e-7};
  Ops<TypeParam>::expect_equal(MyMPC.x[0], x);

  auto other_x = MyMPC.get_state();
  Ops<TypeParam>::expect_equal(other_x, x);

}

TYPED_TEST(data, initializer_solve) {
  solver<TypeParam, uint16_t> MyMPC{};
  MyMPC.initialize(this->MyData);
  MyMPC.set_x0(this->x0);
  MyMPC.solve();

  const std::array<TypeParam, MPC_NX> x{
      0.2500002350554479, 2.350554854076226e-7, 1.1000002863213076,
      2.863214727537976e-7};
  Ops<TypeParam>::expect_equal(MyMPC.x[0], x);

  const std::array<TypeParam, MPC_NX> nu{5.3166845630249195, 8.9751729655173,
                                         14.53111728399386,
                                         -3.0965597381977465};
  Ops<TypeParam>::expect_equal(MyMPC.nu[0], nu);
}

int main(int argc, char *argv[]) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
