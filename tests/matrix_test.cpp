#include <gtest/gtest.h>

#include "mehrotra.hpp"
#include "ops.hpp"

template <class T> class diagmat : public ::testing::Test {
public:
  static constexpr size_t N{5};
  std::array<T, N> x, y, zeros, z;
  mehrotra::DiagMat<T, N> D1, D2, D3;
  diagmat()
      : x{1, 1, 1, 1, 1}, y{-1, -2, -3, -4, -5}, zeros{0}, z{5, 5, 6, 7, 9},
        D1{}, D2{x}, D3{y} {}
  void SetUp() {}
  void TearDown() {}
  ~diagmat() {}
};

template <class T> class pdmat : public ::testing::Test {
public:
  static constexpr size_t N{3};
  std::array<T, N * N> x, x2, y, y2, zero1;
  std::array<T, N *(N + 1) / 2> a, b, zero2;
  mehrotra::PDMat<T, N> P1, P2, P3;
  pdmat()
      : x{1, 2, 3, 4, 5, 6, 7, 8, 9}, x2{1, 2, 3, 5, 6, 9}, y{5,  10, 5, 10, 5,
                                                              10, 8,  7, 6},
        y2{5, 10, 5, 5, 10, 6}, zero1{0}, a{6, 5, 4, 3, 2, 1},
        b{9, 19, 199, 299, 201, 200}, zero2{0}, P1{}, P2{x}, P3{a} {}
  void SetUp() {}
  void TearDown() {}
  ~pdmat() {}
};

template <class T> class gemat : public ::testing::Test {
public:
  static constexpr size_t N{2}, M{4};
  std::array<T, N * M> x, y, zero;
  mehrotra::GeMat<T, N, M> G1, G2, G3;
  gemat()
      : x{1, 5, 2, 6, 3, 7, 4, 8}, y{1, 0, 1, 0, 1, 0, 1, 0}, zero{0}, G1{},
        G2{x}, G3{y} {}
  void SetUp() {}
  void TearDown() {}
  ~gemat() {}
};

typedef ::testing::Types<float, double> MyTypes;

TYPED_TEST_SUITE(diagmat, MyTypes,);
TYPED_TEST_SUITE(pdmat, MyTypes,);
TYPED_TEST_SUITE(gemat, MyTypes,);

TYPED_TEST(diagmat, constructor) {
  for (size_t idx = 0; idx < this->D1.mat.size(); idx++) {
    Ops<TypeParam>::expect_equal(this->D1.mat[idx], this->zeros[idx]);
    Ops<TypeParam>::expect_equal(this->D2.mat[idx], this->x[idx]);
    Ops<TypeParam>::expect_equal(this->D3.mat[idx], this->y[idx]);
  }
}

TYPED_TEST(diagmat, copy_assignment) {
  this->D1 = this->D2;
  this->D3 = this->z;
  for (size_t idx = 0; idx < this->D1.mat.size(); idx++) {
    Ops<TypeParam>::expect_equal(this->D1.mat[idx], this->x[idx]);
    Ops<TypeParam>::expect_equal(this->D3.mat[idx], this->z[idx]);
  }
}

TYPED_TEST(pdmat, constructor) {
  for (size_t idx = 0; idx < this->P1.mat.size(); idx++) {
    Ops<TypeParam>::expect_equal(this->P1.mat[idx], this->zero2[idx]);
    Ops<TypeParam>::expect_equal(this->P2.mat[idx], this->x2[idx]);
    Ops<TypeParam>::expect_equal(this->P3.mat[idx], this->a[idx]);
  }
  const std::array<TypeParam, TestFixture::N> d{8, 5, 8};
  mehrotra::DiagMat<TypeParam, TestFixture::N> D{d};
  const std::array<TypeParam, TestFixture::N *(TestFixture::N + 1) / 2> r{
      8, 0, 0, 5, 0, 8};
  this->P1 = D;
  for (size_t idx = 0; idx < this->P1.mat.size(); idx++) {
    Ops<TypeParam>::expect_equal(this->P1.mat[idx], r[idx]);
  }
}

TYPED_TEST(pdmat, copy_assignment) {
  this->P1 = this->P2;
  this->P3 = this->b;
  this->P2 = {9, 8, 7, 6, 6, 5, 4, 4, 4};
  for (size_t idx = 0; idx < this->P1.mat.size(); idx++) {
    Ops<TypeParam>::expect_equal(this->P1.mat[idx], this->x2[idx]);
    Ops<TypeParam>::expect_equal(this->P3.mat[idx], this->b[idx]);
    Ops<TypeParam>::expect_equal(this->P2.mat[idx], 9 - idx);
  }
}

TYPED_TEST(gemat, constructor) {
  for (size_t idx = 0; idx < this->G1.mat.size(); idx++) {
    Ops<TypeParam>::expect_equal(this->G1.mat[idx], this->zero[idx]);
    Ops<TypeParam>::expect_equal(this->G2.mat[idx], this->x[idx]);
    Ops<TypeParam>::expect_equal(this->G3.mat[idx], this->y[idx]);
  }
}

TYPED_TEST(gemat, copy_assignment) {
  this->G1 = this->G2;
  this->G3 = this->x;
  this->G2 = {1, 2, 3, 4, 5, 6, 7, 8};
  for (size_t idx = 0; idx < this->G1.mat.size(); idx++) {
    Ops<TypeParam>::expect_equal(this->G1.mat[idx], this->x[idx]);
    Ops<TypeParam>::expect_equal(this->G3.mat[idx], this->G1.mat[idx]);
    Ops<TypeParam>::expect_equal(this->G2.mat[idx], idx + 1);
  }
}

int main(int argc, char *argv[]) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
