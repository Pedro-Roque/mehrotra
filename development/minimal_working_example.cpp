/* Solve the following optimization problem

    min     sum_{k=0}^{k=N-1}( u_k^T*R*u_k + x_k^T*Q*x_k ) + x_N^T*P*x_N

    s.t.    x_{k+1} = A * x_k + B * u_k     for k = 0 ... N-1
            l_u <= u_k <= u_u               for k = 0 ... N-1
            l <= x_k <- u                   for k = 1 ... N
            x_0 = x0

    where
            A =     ( 0.1 -0.1   1    0 )
                    ( 0.2  0.4 0.9    0 )
                    ( 0.1  0.2 0.1  0.1 )
                    ( 0.1 -0.5 0.2 -0.1 )

            B =     ( 1  0 )
                    ( 0  1 )
                    ( 1  0 )
                    ( 0  1 )

            x0 =    ( 0.5 )
                    ( 0.5 )
                    ( 0.5 )
                    ( 0.5 )

            R =     ( 0.5  0 )
                    (   0  5 )

            Q =     ( 0.1  0  0   0 )
                    (   0  1  0   0 )
                    (   0  0 10   0 )
                    (   0  0  0 100 )

            P =     ( 9 1 1 1 )
                    ( 1 9 1 1 )
                    ( 1 1 9 1 )
                    ( 1 1 1 9 )

            l_u =   ( 0 )
                    ( 0 )
            u_u =   ( 5 )
                    ( 5 )

            u=-l=   ( 10 )
                    ( 10 )
                    ( 10 )
                    ( 10 )

    ***********************************************************************************
    To compile and run the program, use the following lines

    WITHOUT BLAS LIBRARY:
        g++ development/minimal_working_example.cpp -o development/mve.out -I
        include -DMPC_NO_BLAS;
        ./development/mve.out;

    WITH BLAS LIBRARY:
        g++ development/minimal_working_example.cpp -o development/mve.out -I
        include -lblas -llapack;
        ./development/mve.out;

        NOTE: If you are willing to use BLAS/LAPACK for mathematical operations,
        make sure that you have the required libraries installed and that they
        are included in the library path!

*/

#define MPC_NU 2
#define MPC_NX 4
#define MPC_HL 10

#define MPC_HCU simplebounds
#define MPC_HCX simplebounds
#define MPC_HCT simplebounds

#define MPC_P PDMat

#include "mehrotra.hpp" // Important to define this one after the constants
#include <iostream>

using namespace mehrotra;

void print_results(const solver<double, int> &MyMPC, const int steps);

int main() {
  constexpr int nx{MPC_NX}, nu{MPC_NU}, T{MPC_HL};

  std::array<double, 16> A{0.1, 0.2, 0.1, 0.1, -0.1, 0.4, 0.2, -0.5,
                           1,   0.9, 0.1, 0.2, 0,    0,   0.1, -0.1};
  std::array<double, 8> B{1, 0, 1, 0, 0, 1, 0, 1};
  std::array<double, 4> x0{0.5, 0.5, 0.5, 0.5};

  std::array<double, 2> u_u{5, 5}, l_u{};
  std::array<double, 4> u{10, 10, 10, 10}, l{-10, -10, -10, -10};

  std::array<double, 2> R{0.5, 5};
  std::array<double, 4> Q{0.1, 1, 10, 100};
  std::array<double, 16> P{9, 1, 1, 1, 1, 9, 1, 1, 1, 1, 9, 1, 1, 1, 1, 9};

  mpcdata<double, int> MyData;
  MyData.A = A;
  MyData.B = B;
  MyData.R = R;
  MyData.Q = Q;
  MyData.P = P;
  MyData.ubu = u_u;
  MyData.lbu = l_u;
  MyData.ubx = u;
  MyData.lbx = l;
  MyData.ubt = u;
  MyData.lbt = l;

  solver<double, int> MyMPC(MyData);
  MyMPC.set_x0(x0);
  int number_of_steps = MyMPC.solve();

  print_results(MyMPC, number_of_steps);

  return 0;
}

void print_results(const solver<double, int> &MyMPC, const int steps) {
  std::cout << "Optimization problem solved in " << steps << " iterations."
            << std::endl;

  std::cout << "\nThe optimal input to the system is: " << std::endl;
  std::array<double, MPC_NU> u{MyMPC.get_input()};
  std::cout << "\tu[0] = [" << u[0] << ", " << u[1] << "]" << std::endl;

  std::cout << "\nThe evolution of the system along the prediction horizon is:"
            << std::endl;
  auto x = MyMPC.get_state();
  for (auto idx = 1; idx <= MPC_HL; idx++) {
    x = MyMPC.get_state(idx);
    std::cout << "\tx[" << idx << "] = [" << x[0] << ", " << x[1] << ", "
              << x[2] << ", " << x[3] << "]" << std::endl;
  }
  std::cout
      << "\nThe evolution of the optimal input along the prediction horizon is:"
      << std::endl;
  for (auto idx = 0; idx < MPC_HL; idx++) {
    u = MyMPC.get_input(idx);
    std::cout << "\tu[" << idx << "] = [" << u[0] << ", " << u[1] << "]"
              << std::endl;
  }
}