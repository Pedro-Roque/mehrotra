// Problem dimensions
#define MPC_NU 2
#define MPC_NX 4
#define MPC_HL 10

// HC_NU

// SC_NU

// TC_N

// Problem constraints
#define MPC_HCU simplebounds
#define MPC_SCX simplebounds
#define MPC_HCT simplebounds

// Problem penalties
#define MPC_P PDMat

// standard library
#include <iostream>

// include the library
#include "mehrotra.hpp"

using namespace mehrotra;

int main() {
  constexpr size_t nx{MPC_NX}, nu{MPC_NU};

  // Define matrices
  std::array<double, nx * nx> A{0.1, 0.2, 0.1, 0.1, -0.1, 0.4, 0.2, -0.5,
                                1.0, 0.9, 0.1, 0.2, 0.0,  0.0, 0.1, -0.1};
  std::array<double, nu * nx> B{1, 0, 1, 0, 0, 1, 0, 1};

  // Define initial state
  std::array<double, nx> x0{2, 4, 3, 25};

  // Define bounds
  std::array<double, nu> ubu{5, 5}, lbu{0, 0};
  std::array<double, nx> ubx{10, 10, 10, 10}, lbx{-10, -10, -10, -10};

  // Define penalties
  std::array<double, nu> R{0.5, 5};
  std::array<double, nx> Q{0.1, 1, 10, 100};
  std::array<double, nx * nx> P;
  P.fill(1);
  for (size_t idx = 0; idx < nx; idx++) {
    P[idx + nx * idx] += 8;
  }

  // Group data
  mpcdata<double, size_t> MyData;
  MyData.A = A;
  MyData.B = B;
  MyData.R = R;
  MyData.Q = Q;
  MyData.P = P;

  auto r = MyData.input_constraints({5, 5}, {0, 0});
  
  // MyData.input_constraints(ubu,lbu);

  MyData.subx = ubx;
  MyData.slbx = lbx;
  MyData.ubt = ubx;
  MyData.lbt = lbx;

  // Build solver
  solver<double, size_t> MyMPC(MyData);
  MyMPC.set_feasibility_tolerance(1e-5);
  MyMPC.set_optimality_tolerance(1e-6);

  // Give initial value and solve the problem
  MyMPC.set_x0(x0);
  size_t iter = MyMPC.solve();

  // std::cout << "Total numer of iterations required: " << iter << std::endl;
  // std::cout << "Duality gap attained: " << MyMPC.get_mu() << std::endl;
  // std::cout << "Equality residual norm: " << MyMPC.get_eq_residual_norm()
  //           << std::endl;

  // auto u_opt = MyMPC.get_input();
  // std::cout << "\nOptimal input:";
  // for (auto &elem : u_opt)
  //   std::cout << "\n\t" << elem;

  // auto x_opt = MyMPC.get_state();
  // std::cout << "\n\nOptimal state:";
  // for (auto &elem : x_opt)
  //   std::cout << "\n\t" << elem;

  // std::cout << "\n\n";

  return r;
}