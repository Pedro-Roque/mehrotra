#ifndef MPCDATA_HPP
#define MPCDATA_HPP

#include "matrix/matrix.hpp"
#include "interface/mpc_standard_define.hpp"

namespace mehrotra {

template <class float_t, class int_t,
          template <class, size_t> class R_type = MPC_R,
          template <class, size_t> class Q_type = MPC_Q,
          template <class, size_t> class P_type = MPC_P, size_t nu = MPC_NU,
          size_t nx = MPC_NX, size_t T = MPC_HL, size_t ku = MPC_KU,
          size_t kx = MPC_KX, size_t kt = MPC_KT, size_t sku = MPC_SKU,
          size_t skx = MPC_SKX, size_t skt = MPC_SKT>
struct mpcdata {
  // Dynamics matrices
  GeMat<float_t, nx, nx> A{};
  GeMat<float_t, nu, nx> B{};

  // Penalty matrices
  R_type<float_t, nu> R{};
  Q_type<float_t, nx> Q{};
  P_type<float_t, nx> P{};

  // Penalty vectors
  std::array<float_t, nu> r{};
  std::array<float_t, nx> q{}, p{};

  // Upper and lower bounds
  std::array<float_t, nu> lbu, ubu, slbu, subu{};
  std::array<float_t, nx> lbx, ubx, slbx, subx{};
  std::array<float_t, nx> lbt, ubt, slbt, subt{};

  // Hard linear constraints
  GeMat<float_t, ku, nu> Fu{};
  std::array<float_t, ku> fu{};
  GeMat<float_t, kx, nx> Fx{};
  std::array<float_t, kx> fx{};
  GeMat<float_t, kt, nx> Ft{};
  std::array<float_t, kt> ft{};

  // Soft linear constraints
  GeMat<float_t, sku, nu> sFu{};
  std::array<float_t, sku> sfu{};
  GeMat<float_t, skx, nx> sFx{};
  std::array<float_t, skx> sfx{};
  GeMat<float_t, skt, nx> sFt{};
  std::array<float_t, skt> sft{};

  // Setters
  void input_constraints(std::array<float_t, nu> _ub,
                         std::array<float_t, nu> _lb, GeMat<float_t, ku, nu> _F,
                         std::array<float_t, ku> _f) {
    ubu = std::move(_ub);
    lbu = std::move(_lb);
    Fu = std::move(_F);
    fu = std::move(_f);
  }
  void input_constraints(std::array<float_t, nu> _ub,
                         std::array<float_t, nu> _lb) {
    ubu = std::move(_ub);
    lbu = std::move(_lb);
  }
  void input_constraints(GeMat<float_t, ku, nu> _F,
                         std::array<float_t, ku> _f) {
    Fu = std::move(_F);
    fu = std::move(_f);
  }

  int input_constraints(std::initializer_list<float_t> _ub,
                        std::initializer_list<float_t> _lb,
                        std::initializer_list<float_t> _F,
                        std::initializer_list<float_t> _f) {
    int r{0};
    if (_ub.size() == nu) {
      std::copy(_ub.begin(), _ub.end(), ubu.begin());
    } else {
      r = 1;
    }
    if (_lb.size() == nu) {
      std::copy(_lb.begin(), _lb.end(), lbu.begin());
    } else {
      r = 1;
    }
    if (_F.size() == nu * ku) {
      std::copy(_F.begin(), _F.end(), Fu.mat.begin());
    } else {
      r = 1;
    }
    if (_f.size() == fu) {
      std::copy(_f.begin(), _f.end(), fu.begin());
    } else {
      r = 1;
    }
    return r;
  }

  int input_constraints(std::initializer_list<float_t> x,
                        std::initializer_list<float_t> y) {
    int r{0};
    if ((x.size() == nu) && (y.size() == nu)) {
      std::copy(x.begin(), x.end(), ubu.begin());
      std::copy(y.begin(), y.end(), lbu.begin());
    } else if ((x.size() == nu * ku) && (y.size() == ku)) {
      std::copy(x.begin(), x.end(), Fu.mat.begin());
      std::copy(y.begin(), y.end(), fu.begin());
    } else {
      r = 1;
    }

    return r;
  }
};
} // namespace mehrotra

#endif