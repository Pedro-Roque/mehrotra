#ifndef MPC_STANDARD_DEFINES_
#define MPC_STANDARD_DEFINES_

#ifndef MPC_R
#define MPC_R DiagMat
#endif

#ifndef MPC_Q
#define MPC_Q DiagMat
#endif

#ifndef MPC_P
#define MPC_P DiagMat
#endif

#ifndef MPC_HCU
#define MPC_HCU unconstrained
#endif

#ifndef MPC_SCU
#define MPC_SCU unconstrained
#endif

#ifndef MPC_HCX
#define MPC_HCX unconstrained
#endif

#ifndef MPC_SCX
#define MPC_SCX unconstrained
#endif

#ifndef MPC_HCT
#define MPC_HCT unconstrained
#endif

#ifndef MPC_SCT
#define MPC_SCT unconstrained
#endif

#ifndef MPC_NU
#define MPC_NU 1
#endif

#ifndef MPC_NX
#define MPC_NX 1
#endif

#ifndef MPC_HL
#define MPC_HL 2
#endif

#ifndef MPC_KU
#define MPC_KU 0
#endif

#ifndef MPC_KX
#define MPC_KX 0
#endif

#ifndef MPC_KT
#define MPC_KT 0
#endif

#ifndef MPC_SKU
#define MPC_SKU 0
#endif

#ifndef MPC_SKX
#define MPC_SKX 0
#endif

#ifndef MPC_SKT
#define MPC_SKT 0
#endif

#endif