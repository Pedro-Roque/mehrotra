#ifndef SHELL_HPP_
#define SHELL_HPP_

#include "mpc_standard_define.hpp"
#include "mpcdata.hpp"

#include "constraints/constraints.hpp"
#include "constraints/hardcons.hpp"
#include "constraints/softcons.hpp"

namespace mehrotra {

template <class float_t, class int_t,
          template <class, size_t> class R_type = mehrotra::MPC_R,
          template <class, size_t> class Q_type = mehrotra::MPC_Q,
          template <class, size_t> class P_type = mehrotra::MPC_P,
          template <class, class, size_t, size_t, size_t> class inco =
              mehrotra::hardcons::MPC_HCU,
          template <class, class, size_t, size_t, size_t> class soinco =
              mehrotra::softcons::MPC_SCU,
          template <class, class, size_t, size_t, size_t> class stco =
              mehrotra::hardcons::MPC_HCX,
          template <class, class, size_t, size_t, size_t> class sostco =
              mehrotra::softcons::MPC_SCX,
          template <class, class, size_t, size_t, size_t> class teco =
              mehrotra::hardcons::MPC_HCT,
          template <class, class, size_t, size_t, size_t> class soteco =
              mehrotra::softcons::MPC_SCT,
          size_t num_u = MPC_NU, size_t nx = MPC_NX, size_t T = MPC_HL,
          size_t ku = MPC_KU, size_t kx = MPC_KX, size_t kt = MPC_KT,
          size_t sku = MPC_SKU, size_t skx = MPC_SKX, size_t skt = MPC_SKT>
class solver : protected constraint<float_t, int_t, R_type, inco, soinco, num_u,
                                    nx, T, ku, sku, detail::input>,
               protected constraint<float_t, int_t, Q_type, stco, sostco, nx,
                                    nx, T - 1, kx, skx, detail::state>,
               protected constraint<float_t, int_t, P_type, teco, soteco, nx,
                                    nx, 1, kt, skt, detail::terminal> {
#ifdef MPC_GTEST_COMPILE
public:
#else
private:
#endif
  using input_cons = constraint<float_t, int_t, R_type, inco, soinco, num_u, nx,
                                T, ku, sku, detail::input>;
  using state_cons = constraint<float_t, int_t, Q_type, stco, sostco, nx, nx,
                                T - 1, kx, skx, detail::state>;
  using terminal_cons = constraint<float_t, int_t, P_type, teco, soteco, nx, nx,
                                   1, kt, skt, detail::terminal>;

  GeMat<float_t, nx, nx> A{};
  GeMat<float_t, num_u, nx> B{};

  R_type<float_t, num_u> R{};
  Q_type<float_t, nx> Q{};
  P_type<float_t, nx> P{};

  std::array<float_t, num_u> r{};
  std::array<float_t, nx> q{}, p{};

  std::array<PDMat<float_t, nx>, T> L{};
  std::array<GeMat<float_t, nx, nx>, T - 1> M{};
  std::array<std::array<float_t, nx>, T> rsc{};

  std::array<std::array<float_t, num_u>, T> u;
  std::array<std::array<float_t, num_u>, T> u_aff{}, u_cc{};

  std::array<std::array<float_t, nx>, T> x;
  std::array<std::array<float_t, nx>, T> x_aff{}, x_cc{};

  std::array<std::array<float_t, nx>, T> nu{};
  std::array<std::array<float_t, nx>, T> nu_aff{}, nu_cc{};

  std::array<float_t, nx> x0{};

  float_t mu{1}, mu_aff{1}, sigma{0};
  float_t alpha{0};
  float_t eq_res_norm{0};
  float_t fea_tol{1e-2}, opt_tol{1e-4};
  float_t eta{0.99};

  float_t rho0{100}, rho_update{100};

  size_t k{0}, K{100};

  void forward_solve() {
    detail::MatOps<float_t>::cholfact(L[0]);
    detail::MatOps<float_t>::backsolve('N', L[0], nu_aff[0]);
    detail::MatOps<float_t>::backsolve('N', L[0], M[0]);
  }

  void forward_solve_last() {

    detail::MatOps<float_t>::rankkt(float_t{-1}, L[T - 1], M[T - 2]);
    detail::MatOps<float_t>::cholfact(L[T - 1]);
    detail::MatOps<float_t>::template matvec<'T'>(
        float_t{1}, M[T - 2], nu_aff[T - 2], float_t{1}, nu_aff[T - 1]);
    detail::MatOps<float_t>::backsolve('N', L[T - 1], nu_aff[T - 1]);
  }

  void forward_solve(const size_t step) {
    detail::MatOps<float_t>::rankkt(float_t{-1}, L[step], M[step - 1]);
    detail::MatOps<float_t>::cholfact(L[step]);
    detail::MatOps<float_t>::backsolve('N', L[step], M[step]);
    detail::MatOps<float_t>::template matvec<'T'>(
        float_t{1}, M[step - 1], nu_aff[step - 1], float_t{1}, nu_aff[step]);
    detail::MatOps<float_t>::backsolve('N', L[step], nu_aff[step]);
  }

  void backward_solve() {
    detail::MatOps<float_t>::backsolve('T', L[T - 1], nu_aff[T - 1]);
  }

  void backward_solve(const size_t step) {
    detail::MatOps<float_t>::template matvec<'N'>(
        float_t{1}, M[step], nu_aff[step + 1], float_t{1}, nu_aff[step]);
    detail::MatOps<float_t>::backsolve('T', L[step], nu_aff[step]);
  }

  /**
   * @brief Forward solve against the right hand side of the centering and
   * correction step, for time step 0.
   *
   * The right-hand side is stored in nu_cc, whereas L[0] stores the first
   * diagonal block in the factorization of the shur complement.
   *
   */
  void cc_forward_solve() {
    detail::MatOps<float_t>::backsolve('N', L[0], nu_cc[0]);
  }

  /**
   * @brief Forward solve against the right hand side of the centering and
   * correction step, for the given time step.
   *
   * On entry, nu_cc[step-1] contains the intermediate variable that arises from
   * the forward solve at step step-1.
   * M[step-1] constaints the "step-1" block in the factorization of the shur
   * complement. nu_cc[step] containts the right-hand side for the given time
   * step in the centering and correction step.
   *
   * Remember that the factorization of the Shur Complement has blocks M_i in
   * the main subdiagonal. However, the blocks M[i] store the value -M_i'
   * (transposed and negated)
   *
   * @param step Time step, in range [1, T-2]
   */
  void cc_forward_solve(const size_t step) {
    detail::MatOps<float_t>::template matvec<'T'>(
        float_t{1}, M[step - 1], nu_cc[step - 1], float_t{1}, nu_cc[step]);
    detail::MatOps<float_t>::backsolve('N', L[step], nu_cc[step]);
  }

  void cc_backward_solve() {
    detail::MatOps<float_t>::backsolve('T', L[T - 1], nu_cc[T - 1]);
  }

  void cc_backward_solve(const size_t step) {
    detail::MatOps<float_t>::template matvec<'N'>(
        float_t{1}, M[step], nu_cc[step + 1], float_t{1}, nu_cc[step]);
    detail::MatOps<float_t>::backsolve('T', L[step], nu_cc[step]);
  }

  /**
   * @brief Evaluates the equality residual, and stores it in nu_aff.
   * The equality residual is evaluated for the current values of x0 and vectors
   * x and u.
   *
   */
  void equality_residual() {
    // First step
    nu_aff[0] = x[0];
    detail::MatOps<float_t>::template matvec<'T'>(float_t{1}, A, x0,
                                                  float_t{-1}, nu_aff[0]);
    detail::MatOps<float_t>::template matvec<'T'>(float_t{1}, B, u[0],
                                                  float_t{1}, nu_aff[0]);
    // Remaining steps
    for (size_t step = 1; step < T; step++) {
      nu_aff[step] = x[step];
      detail::MatOps<float_t>::template matvec<'T'>(float_t{1}, A, x[step - 1],
                                                    float_t{-1}, nu_aff[step]);
      detail::MatOps<float_t>::template matvec<'T'>(float_t{1}, B, u[step],
                                                    float_t{1}, nu_aff[step]);
    }
  }

  /**
   * @brief Evaluates the duality gap and stores it in mu.
   * The duality gap is evaluated for the current values of the vectors s
   * (slacks) and lambda (Lagrange multipliers).
   *
   */
  void duality_gap() {
    mu = 0;
    mu += input_cons::duality_gap();
    mu += state_cons::duality_gap();
    mu += terminal_cons::duality_gap();
    // BEWARE!! Use of booleans in arithmetic operations
    mu /= input_cons::has_bounds * (2 * num_u * T) +
          input_cons::has_linear * ku * T +
          state_cons::has_bounds * (2 * nx * (T - 1)) +
          state_cons::has_linear * kx * (T - 1) +
          terminal_cons::has_bounds * (2 * nx) + input_cons::has_linear * kt;
  }

  void affine_step() {
    // Forward solve
    L[0].mat.fill(float_t{0});
    input_cons::aff_forward(R, r, B, u[0], nu[0], L[0], nu_aff[0], 0);
    state_cons::aff_forward(Q, q, A, x[0], nu[0], nu[1], L[0], L[1], M[0],
                            nu_aff[0], nu_aff[1], 0);

    forward_solve();
    for (size_t step = 1; step < T - 1; step++) {
      input_cons::aff_forward(R, r, B, u[step], nu[step], L[step], nu_aff[step],
                              step);
      state_cons::aff_forward(Q, q, A, x[step], nu[step], nu[step + 1], L[step],
                              L[step + 1], M[step], nu_aff[step],
                              nu_aff[step + 1], step);
      forward_solve(step);
    }

    input_cons::aff_forward(R, r, B, u[T - 1], nu[T - 1], L[T - 1],
                            nu_aff[T - 1], T - 1);
    terminal_cons::aff_forward(P, p, x[T - 1], nu[T - 1], L[T - 1],
                               nu_aff[T - 1], T - 1);
    forward_solve_last();

    // Backwards solve
    backward_solve();
    input_cons::aff_backward(nu_aff[T - 1], u[T - 1], u_aff[T - 1], T - 1);
    terminal_cons::aff_backward(nu_aff[T - 1], x[T - 1], x_aff[T - 1], T - 1);

    for (size_t step = 2; step < T + 1; step++) {
      backward_solve(T - step);
      input_cons::aff_backward(nu_aff[T - step], u[T - step], u_aff[T - step],
                               T - step);
      state_cons::aff_backward(nu_aff[T - step], nu_aff[T - step + 1],
                               x[T - step], x_aff[T - step], T - step);
    }
  }

  void linesearch() {
    alpha = std::numeric_limits<float_t>::lowest();
    input_cons::linesearch(alpha);
    state_cons::linesearch(alpha);
    terminal_cons::linesearch(alpha);
    alpha = -alpha;
  }

  void centering() {
    mu_aff = 0;
    mu_aff += input_cons::affine_gap(alpha);
    mu_aff += state_cons::affine_gap(alpha);
    mu_aff += terminal_cons::affine_gap(alpha);
    // BEWARE!! Use of booleans in arithmetic operations
    mu_aff /= input_cons::has_bounds * (2 * num_u * T) +
              input_cons::has_linear * ku * T +
              state_cons::has_bounds * (2 * nx * (T - 1)) +
              state_cons::has_linear * kx * (T - 1) +
              terminal_cons::has_bounds * (2 * nx) +
              input_cons::has_linear * kt;
    // BEWARE!! Assumes that the duality gap has already been calculated
    sigma = (mu_aff * mu_aff * mu_aff) / (mu * mu * mu);
  }

  void cc_step() {
    // Forward solve
    nu_cc[0].fill(float_t{0});
    input_cons::cc_forward(nu_cc[0], sigma * mu_aff, 0);
    state_cons::cc_forward(nu_cc[0], nu_cc[1], sigma * mu_aff, 0);
    cc_forward_solve();
    for (size_t step = 1; step < T - 1; step++) {
      input_cons::cc_forward(nu_cc[step], sigma * mu_aff, step);
      state_cons::cc_forward(nu_cc[step], nu_cc[step + 1], sigma * mu_aff,
                             step);
      cc_forward_solve(step);
    }
    input_cons::cc_forward(nu_cc[T - 1], sigma * mu_aff, T - 1);
    terminal_cons::cc_forward(nu_cc[T - 1], sigma * mu_aff, T - 1);
    cc_forward_solve(T - 1);

    // Backwards solve
    cc_backward_solve();
    input_cons::cc_backward(nu_cc[T - 1], u[T - 1], u_cc[T - 1], T - 1);
    terminal_cons::cc_backward(nu_cc[T - 1], x[T - 1], x_cc[T - 1], T - 1);
    detail::MatOps<float_t>::vecsum(float_t{1}, nu_cc[T - 1], nu_aff[T - 1]);
    detail::MatOps<float_t>::vecsum(float_t{1}, u_cc[T - 1], u_aff[T - 1]);
    detail::MatOps<float_t>::vecsum(float_t{1}, x_cc[T - 1], x_aff[T - 1]);

    for (size_t step = 2; step < T + 1; step++) {
      cc_backward_solve(T - step);
      input_cons::cc_backward(nu_cc[T - step], u[T - step], u_cc[T - step],
                              T - step);
      state_cons::cc_backward(nu_cc[T - step], nu_cc[T - step + 1], x[T - step],
                              x_cc[T - step], T - step);
      detail::MatOps<float_t>::vecsum(float_t{1}, nu_cc[T - step],
                                      nu_aff[T - step]);
      detail::MatOps<float_t>::vecsum(float_t{1}, u_cc[T - step],
                                      u_aff[T - step]);
      detail::MatOps<float_t>::vecsum(float_t{1}, x_cc[T - step],
                                      x_aff[T - step]);
    }
  }

  void take_step() {
    alpha *= eta;
    for (size_t step = 0; step < T; step++) {
      detail::MatOps<float_t>::vecsum(alpha, nu_aff[step], nu[step]);
      detail::MatOps<float_t>::vecsum(alpha, x_aff[step], x[step]);
      detail::MatOps<float_t>::vecsum(alpha, u_aff[step], u[step]);
    }
    input_cons::take_step(alpha);
    state_cons::take_step(alpha);
    terminal_cons::take_step(alpha);
  }

  /**
   * @brief Returns TRUE if the algorithm has terminated.
   * Returns FALSE otherwise.
   *
   * In first place, it checks whether the maximum iterate count has been hit.
   *
   * In second place, it evaluates the duality gap and stores it in mu.
   * The duality gap is compared against the optimality tolerance.
   *
   * In third place, it computes the equality residual and stores it in the
   * vector nu_aff (which is hence initialized). The norm of the equality
   * residual is compared against the feasibility tolerance.
   *
   * @return true The algorithm has terminated
   * @return false The algorithm has not terminated
   */
  bool terminate() {
    // Evaluate duality gap
    duality_gap();

    // Evaluate equality residual
    equality_residual();
    eq_res_norm = 0;
    for (size_t step = 1; step < T; step++) {
      eq_res_norm +=
          detail::MatOps<float_t>::dotproduct(nu_aff[step], nu_aff[step]);
    }

    // Check the three conditions
    return ((mu < opt_tol) & (eq_res_norm < fea_tol * fea_tol)) | (k >= K);
  }

public:
  solver() = default;
  solver(mpcdata<float_t, int_t, R_type, Q_type, P_type, num_u, nx, T, ku, kx,
                 kt, sku, skx, skt>
             x)
      : input_cons(x.B, x.R, std::move(x.ubu), std::move(x.lbu),
                   std::move(x.Fu), std::move(x.fu), std::move(x.subu),
                   std::move(x.slbu), std::move(x.sFu), std::move(x.sfu)),
        state_cons(x.A, x.Q, std::move(x.ubx), std::move(x.lbx),
                   std::move(x.Fx), std::move(x.fx), std::move(x.subx),
                   std::move(x.slbx), std::move(x.sFx), std::move(x.sfx)),
        terminal_cons(x.A, x.P, std::move(x.ubt), std::move(x.lbt),
                      std::move(x.Ft), std::move(x.ft), std::move(x.subt),
                      std::move(x.slbt), std::move(x.sFt), std::move(x.sft)),
        A{std::move(x.A)}, B{std::move(x.B)}, R{std::move(x.R)}, Q{std::move(
                                                                     x.Q)},
        P{std::move(x.P)}, r{std::move(x.r)}, q{std::move(x.q)}, p{std::move(
                                                                     x.p)} {
    // detail::transpose(A);
    // detail::transpose(B);

    input_cons::initial_value(this->u[0]);
    state_cons::initial_value(this->x[0]);
    terminal_cons::initial_value(this->x[T - 1]);

    input_cons::set_rho0(rho0);
    state_cons::set_rho0(rho0);
    terminal_cons::set_rho0(rho0);

    u.fill(u[0]);
    std::fill(this->x.begin(), this->x.end() - 1, this->x[0]);
  }

  void initialize(mpcdata<float_t, int_t, R_type, Q_type, P_type, num_u, nx, T,
                          ku, kx, kt, sku, skx, skt>
                      x) {
    input_cons::initialize(x.B, x.R, this->u[0], std::move(x.ubu),
                           std::move(x.lbu), std::move(x.Fu), std::move(x.fu),
                           std::move(x.subu), std::move(x.slbu),
                           std::move(x.sFu), std::move(x.sfu));
    state_cons::initialize(x.A, x.Q, this->x[0], std::move(x.ubx),
                           std::move(x.lbx), std::move(x.Fx), std::move(x.fx),
                           std::move(x.subx), std::move(x.slbx),
                           std::move(x.sFx), std::move(x.sfx));
    terminal_cons::initialize(
        x.A, x.P, this->x[T - 1], std::move(x.ubt), std::move(x.lbt),
        std::move(x.Ft), std::move(x.ft), std::move(x.subt), std::move(x.slbt),
        std::move(x.sFt), std::move(x.sft));
    u.fill(u[0]);
    std::fill(this->x.begin(), this->x.end() - 1, this->x[0]);
    A = std::move(x.A);
    B = std::move(x.B);
    R = std::move(x.R);
    Q = std::move(x.Q);
    P = std::move(x.P);
    r = std::move(x.r);
    q = std::move(x.q);
    p = std::move(x.p);
  }

  size_t solve() {
    equality_residual();
    duality_gap();
    k = 0;

    do {
      input_cons::update(rho_update);
      state_cons::update(rho_update);
      terminal_cons::update(rho_update);

      affine_step();
      linesearch();
      centering();
      cc_step();
      linesearch();
      take_step();

      k++;
    } while (!terminate());
    return k;
  }

  void set_x0(std::array<float_t, nx> x_new) { x0 = std::move(x_new); }

  std::array<float_t, num_u> get_input(size_t step = 0) const {
    return u[step];
  }
  std::array<float_t, nx> get_state(size_t step = 1) const {
    return x[step - 1];
  }
  float_t get_mu() {
    duality_gap();
    return mu;
  }
  float_t get_eq_residual_norm() {
    equality_residual();
    return eq_res_norm;
  }

  void set_optimality_tolerance(const float_t x) { opt_tol = x; }
  void set_feasibility_tolerance(const float_t x) { fea_tol = x; }
  void set_max_iterations(const size_t x) { K = x; }
  void set_imbalance_damp(const float_t x) { eta = x; }
}; // namespace mehrotra

} // namespace mehrotra
#endif