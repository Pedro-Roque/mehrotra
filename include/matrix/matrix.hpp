#ifndef MATRIX_HPP_
#define MATRIX_HPP_

#include <array>

namespace mehrotra {

template <class float_t, size_t n> struct DiagMat {
  std::array<float_t, n> mat{};

  DiagMat() = default;

  DiagMat(std::array<float_t, n> x) : mat(std::move(x)) {}

  void operator=(std::array<float_t, n> x) { mat = std::move(x); }
};

template <class float_t, size_t n> struct PDMat {
  std::array<float_t, n *(n + 1) / 2> mat{};

  PDMat() = default;

  PDMat(std::array<float_t, n *(n + 1) / 2> x) : mat{std::move(x)} {}

  PDMat(const std::array<float_t, n * n> &x) {
    uint16_t skip = 0;
    for (size_t col = 0; col < n; col++) {
      for (size_t row = 0; row < n - col; row++) {
        mat[skip + row] = x[col * n + row + col];
      }
      skip += n - col;
    }
  }

  PDMat(const DiagMat<float_t, n> &x) {
    mat.fill(0);
    uint16_t skip = 0;
    for (size_t idx = 0; idx < n; idx++) {
      mat[skip] = x.mat[idx];
      skip += n - idx;
    }
  }

  void operator=(const std::array<float_t, n * n> &x) {
    uint16_t skip = 0;
    for (size_t col = 0; col < n; col++) {
      for (size_t row = 0; row < n - col; row++) {
        mat[skip + row] = x[col * n + row + col];
      }
      skip += n - col;
    }
  }

  void operator=(const DiagMat<float_t, n> &x) {
    mat.fill(0);
    uint16_t skip = 0;
    for (size_t idx = 0; idx < n; idx++) {
      mat[skip] = x.mat[idx];
      skip += n - idx;
    }
  }
};

template <class float_t, size_t n, size_t m> struct GeMat {
  std::array<float_t, n * m> mat{};

  GeMat() = default;

  GeMat(std::array<float_t, n * m> x) : mat(std::move(x)) {}

  void operator=(std::array<float_t, n * m> x) { mat = std::move(x); }
};

} // namespace mehrotra

#endif