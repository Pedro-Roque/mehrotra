#ifndef MEHROTRA_HPP_
#define MEHROTRA_HPP_

#include "detail/matops.hpp"

#ifndef MPC_NO_BLAS
#include "detail/matops_double.hpp"
#include "detail/matops_float.hpp"
#endif

#include "interface/shell.hpp"

#endif