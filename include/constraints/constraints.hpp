#ifndef CONS_HPP_
#define CONS_HPP_

#include <type_traits>

#include "constraints/hardcons.hpp"
#include "constraints/softcons.hpp"
#include "detail/traits.hpp"
#include "matrix/matrix.hpp"

namespace mehrotra {

// *********************************************************************** //
// ************** Auxiliar base class for precomputations **************** //
// *********************************************************************** //
template <class float_t, size_t n, size_t nx,
          template <class, size_t> class matrix, bool only_linear,
          bool has_no_cons>
struct precomputations {
  precomputations() = default;
  precomputations(const matrix<float_t, n> &) {}
  void initialize(const matrix<float_t, n> &) {}
};

template <class float_t, size_t n, size_t nx,
          template <class, size_t> class matrix>
struct precomputations<float_t, n, nx, matrix, true, false> {
  precomputations() = default;
  precomputations(const matrix<float_t, n> &x) {
    factor = x;
    detail::MatOps<float_t>::cholfact(factor);
  }
  void initialize(const matrix<float_t, n> &x) {
    factor = x;
    detail::MatOps<float_t>::cholfact(factor);
  }

  PDMat<float_t, n> factor;
};

template <class float_t, size_t n, size_t nx,
          template <class, size_t> class matrix>
struct precomputations<float_t, n, nx, matrix, false, true> {
  precomputations() = default;

  precomputations(const PDMat<float_t, n> &phi) : chol_block() {}

  PDMat<float_t, nx> chol_block{};
};

// *********************************************************************** //
// **************** Auxiliar class for operator brackets ***************** //
// *********************************************************************** //
template <class matrix> struct vec_replacement {
  matrix x;
  matrix &operator[](const size_t) { return x; };
};

// *********************************************************************** //
// **************************** Manager class **************************** //
// *********************************************************************** //
template <class float_t, class int_t, template <class, size_t> class matrix,
          template <class, class, size_t, size_t, size_t> class hc,
          template <class, class, size_t, size_t, size_t> class sc, size_t n,
          size_t nx, size_t T, size_t k, size_t sk, class cons_type>
class constraint
    : public hc<float_t, int_t, n, k, T>,
      public sc<float_t, int_t, n, sk, T>,
      public precomputations<
          float_t, n, nx, matrix,
          (detail::has_linear<hc>::value | detail::has_linear<sc>::value) &
              (!detail::has_bounds<hc>::value) &
              (!detail::has_bounds<sc>::value),
          (!detail::has_linear<hc>::value) & (!detail::has_bounds<hc>::value) &
              (!detail::has_linear<sc>::value) &
              (!detail::has_bounds<sc>::value)> {
#ifdef MPC_GTEST_COMPILE
public:
#else
private:
#endif
  using factored_penalty = precomputations<
      float_t, n, nx, matrix,
      (detail::has_linear<hc>::value | detail::has_linear<sc>::value) &
          (!detail::has_bounds<hc>::value) & (!detail::has_bounds<sc>::value),
      (!detail::has_linear<hc>::value) & (!detail::has_bounds<hc>::value) &
          (!detail::has_linear<sc>::value) & (!detail::has_bounds<sc>::value)>;

  static constexpr bool has_no_cons{
      (!detail::has_linear<hc>::value) & (!detail::has_bounds<hc>::value) &
      (!detail::has_linear<sc>::value) & (!detail::has_bounds<sc>::value)};

  static constexpr bool only_linear{
      (detail::has_linear<hc>::value | detail::has_linear<sc>::value) &
      (!detail::has_bounds<hc>::value) & (!detail::has_bounds<sc>::value)};

  /**
   * @brief Matrix PHI
   *
   * If TERMINAL CONSTRAINTS
   *    PHI is of type matrix<float_t,n>
   * elseif HAS_NO_CONS
   *    PHI is of type vec_replacement<matrix<float_t,n>>
   * elseif (hc has LINEAR) or (sc has LINEAR)
   *    PHI is of type std::array<PDMat,T>
   * else
   *    PHI is of type std::array<matrix,T>
   * end
   *
   */
  typename std::conditional<
      has_no_cons | std::is_same<cons_type, detail::terminal>::value,
      typename std::conditional<
          std::is_same<cons_type, detail::terminal>::value, matrix<float_t, n>,
          vec_replacement<matrix<float_t, n>>>::type,
      typename std::conditional<
          detail::has_linear<hc>::value | detail::has_linear<sc>::value,
          std::array<PDMat<float_t, n>, T>,
          std::array<matrix<float_t, n>, T>>::type>::type phi{};

  typename std::conditional<
      has_no_cons | std::is_same<cons_type, detail::terminal>::value,
      typename std::conditional<
          std::is_same<cons_type, detail::terminal>::value,
          GeMat<float_t, n, nx>, vec_replacement<GeMat<float_t, n, nx>>>::type,
      std::array<GeMat<float_t, n, nx>, T>>::type phixt{};

  typename std::conditional<std::is_same<cons_type, detail::terminal>::value,
                            std::array<float_t, n>,
                            std::array<std::array<float_t, n>, T>>::type rd{};

  /**
   * @brief Creates one block in the main diagonal of the Shur Complement for
   * dense augmented hessian
   *
   * Augments one block in the mian diagonal of the Shur Complement:
   *
   * L <- L + B * Phi^-1 * B'
   *
   * At the same time, creates the semiproduct
   * Phi^-1*B'. This method only compiles succesfully for input constraints.
   *
   * @tparam std::is_same<cons_type, detail::input>::value True for input
   * constraints; false otherwise.
   * @param D Dynamics matrix (Normally, B' --> Note the transposition)
   * @param L PDMat (Shur Complement main diagonal block)
   * @param phi PDMat (Augmented hessian)
   * @param phixt Semiproduct
   * @return std::enable_if<b>::type Void for input constraints. Ill-formed
   * otherwise.
   */
  template <bool b = std::is_same<cons_type, detail::input>::value>
  typename std::enable_if<b>::type
  shur_complement(const GeMat<float_t, n, nx> &D, PDMat<float_t, nx> &L,
                  PDMat<float_t, n> &phi, GeMat<float_t, n, nx> &phixt,
                  const std::false_type) {
    phixt = D;
    detail::MatOps<float_t>::backsolve('N', phi, phixt);
    detail::MatOps<float_t>::rankkt(float_t{1}, L, phixt);
    detail::MatOps<float_t>::backsolve('T', phi, phixt);
    detail::MatOps<float_t>::invert(phi);
  }

  /**
   * @brief Creates one block in the main diagonal of the Shur Complement for
   * diagonal augmented hessian
   *
   * Augments one block in the mian diagonal of the Shur Complement:
   *
   * L <- L + B * Phi^-1 * B'
   *
   * At the same time, creates the semiproduct
   * Phi^-1*B'. This method only compiles succesfully for input constraints.
   *
   * @tparam std::is_same<cons_type, detail::input>::value True for input
   * constraints; false otherwise.
   * @param D Dynamics matrix (Normally, B' --> Note the transposition)
   * @param L PDMat (Shur Complement main diagonal block)
   * @param phi DiagMat (Augmented hessian)
   * @param phixt Semiproduct
   * @return std::enable_if<b>::type Void for input constraints. Ill-formed
   * otherwise.
   */
  template <bool b = std::is_same<cons_type, detail::input>::value>
  typename std::enable_if<b>::type
  shur_complement(const GeMat<float_t, n, nx> &D, PDMat<float_t, nx> &L,
                  const DiagMat<float_t, n> &phi, GeMat<float_t, n, nx> &phixt,
                  const std::false_type) {
    detail::MatOps<float_t>::rankkt(phi.mat, L, D);
    phixt = D;
    for (size_t idx = 0; idx < nx; idx++) {
      std::transform(phi.mat.cbegin(), phi.mat.cend(),
                     phixt.mat.cbegin() + idx * n, phixt.mat.begin() + idx * n,
                     std::multiplies<float_t>());
    }
  }

  /**
   * @brief Same as before, but for unconstrained variables.
   *
   * In this case, the semiproduct Phi^-1*B' has already been precomputed, and
   * needs not be calculated again. Similarily, the product B*Phi^-1*B' is
   * precomputed, and the augmentation of L is a simple matrix addition.
   *
   * @tparam matrix Type of the phi matrix. May resolve to DiagMat or PDMat
   * @tparam std::is_same<cons_type, detail::input>::value True for input
   * constraints; false otherwise.
   * @param D Dynamics matrix (Normally, B' --> Note the transposition)
   * @param L PDMat (Shur Complement main diagonal block)
   * @param phi Contains the inverse of the penalty matrix
   * @param phixt Contains the precomputed semiproduct
   * @return std::enable_if<b>::type Void for input constraints. Ill-formed
   * otherwise.
   */
  template <template <class, size_t> class anymatrix,
            bool b = std::is_same<cons_type, detail::input>::value>
  typename std::enable_if<b>::type
  shur_complement(const GeMat<float_t, n, nx> &D, PDMat<float_t, nx> &L,
                  anymatrix<float_t, n> &phi, GeMat<float_t, n, nx> &phixt,
                  const std::true_type) {
    detail::MatOps<float_t>::matsum(L, factored_penalty::chol_block);
  }

  /**
   * @brief Creates one block in the main diagonal of the Shur Complement for
   * dense augmented hessian, also one block in the subdiagonal.
   *
   * Augments one block in the mian diagonal of the Shur Complement:
   *
   * L <- L + Phi^-1
   *
   * Initializes the next block in the main diagonal
   *
   * L2 <- A * Phi^-1 * A'
   *
   * Initializes one block in the main subdiagonal
   *
   * M <- Phi^-1 * A'
   *
   *
   * At the same time, creates the semiproduct
   * Phi^-1*A'. This method only compiles succesfully for state constraints.
   *
   * @tparam std::is_same<cons_type, detail::state>::value True for state
   * constraints; false otherwise.
   * @param D Dynamics matrix (Normally, B' --> Note the transposition)
   * @param L PDMat (Shur Complement main diagonal block)
   * @param L2 PDMat (next block in the Shur Complement)
   * @param M GeMat (Subdiagonal block in the Shur Complement)
   * @param phi PDMat (Augmented hessian)
   * @param phixt Semiproduct
   * @return std::enable_if<b>::type Void for state constraints. Ill-formed
   * otherwise.
   */
  template <bool b = std::is_same<cons_type, detail::state>::value>
  typename std::enable_if<b>::type
  shur_complement(const GeMat<float_t, n, nx> &D, PDMat<float_t, nx> &L,
                  PDMat<float_t, nx> &L2, GeMat<float_t, nx, nx> &M,
                  PDMat<float_t, n> &phi, GeMat<float_t, n, nx> &phixt,
                  const std::false_type) {
    L2 = phi;
    detail::MatOps<float_t>::invert(L2);
    detail::MatOps<float_t>::matsum(L, L2);
    phixt = D;
    detail::MatOps<float_t>::backsolve('N', phi, phixt);
    L2.mat.fill(float_t{0});
    detail::MatOps<float_t>::rankkt(float_t{1}, L2, phixt);
    detail::MatOps<float_t>::backsolve('T', phi, phixt);
    M = phixt;
    detail::MatOps<float_t>::invert(phi);
  }

  /**
   * @brief Creates one block in the main diagonal of the Shur Complement for
   * diagonal augmented hessian, also one block in the subdiagonal.
   *
   * Augments one block in the mian diagonal of the Shur Complement:
   *
   * L <- L + Phi^-1
   *
   * Initializes the next block in the main diagonal
   *
   * L2 <- A * Phi^-1 * A'
   *
   * Initializes one block in the main subdiagonal
   *
   * M <- Phi^-1 * A'
   *
   *
   * At the same time, creates the augmented hessian phi and the semiproduct
   * Phi^-1*A'. This method only compiles succesfully for state constraints.
   *
   * @tparam std::is_same<cons_type, detail::state>::value True for state
   * constraints; false otherwise.
   * @param D Dynamics matrix (Normally, B' --> Note the transposition)
   * @param L PDMat (Shur Complement main diagonal block)
   * @param L2 PDMat (next block in the Shur Complement)
   * @param M GeMat (Subdiagonal block in the Shur Complement)
   * @param phi DiagMat (Augmented hessian)
   * @param phixt Semiproduct
   * @return std::enable_if<b>::type Void for state constraints. Ill-formed
   * otherwise.
   */
  template <bool b = std::is_same<cons_type, detail::state>::value>
  typename std::enable_if<b>::type
  shur_complement(const GeMat<float_t, n, nx> &A, PDMat<float_t, nx> &L,
                  PDMat<float_t, nx> &L2, GeMat<float_t, nx, nx> &M,
                  const DiagMat<float_t, n> &phi, GeMat<float_t, n, nx> &phixt,
                  const std::false_type) {
    detail::MatOps<float_t>::matsum(L, phi);
    phixt = A;
    for (size_t idx = 0; idx < nx; idx++) {
      std::transform(phi.mat.cbegin(), phi.mat.cend(),
                     phixt.mat.cbegin() + idx * n, phixt.mat.begin() + idx * n,
                     std::multiplies<float_t>());
    }
    M = phixt;
    L2.mat.fill(float_t{0});
    detail::MatOps<float_t>::rankkt(phi.mat, L2, A);
  }

  template <template <class, size_t> class anymatrix,
            bool b = std::is_same<cons_type, detail::state>::value>
  typename std::enable_if<b>::type
  shur_complement(const GeMat<float_t, n, nx> &A, PDMat<float_t, nx> &L,
                  PDMat<float_t, nx> &L2, GeMat<float_t, nx, nx> &M,
                  const anymatrix<float_t, n> &phi,
                  GeMat<float_t, n, nx> &phixt, const std::true_type) {
    detail::MatOps<float_t>::matsum(L, phi);
    L2 = factored_penalty::chol_block;
    M = phixt;
  }

  /**
   *  @brief Creates one block in the main diagonal of the Shur Complement
   * for dense augmented hessian
   *
   * Augments one block in the mian diagonal of the Shur Complement:
   *
   * L <- L + B * Phi^-1 * B'
   *
   * The semiproduct Phi^-1*B' is not created, since it is not needed in the
   * case of terminal constraints. This method only compiles succesfully for
   * terminal constraints.
   *
   * @tparam std::is_same<cons_type, detail::terminal>::value True for input
   * constraints; false otherwise.
   * @param L  PDMat (Shur Complement main diagonal --last-- block)
   * @param phi PDMat (Augmented hessian)
   * @return std::enable_if<b>::type Void for input constraints. Ill-formed
   * otherwise.
   */
  template <bool b = std::is_same<cons_type, detail::terminal>::value>
  typename std::enable_if<b>::type shur_complement(PDMat<float_t, nx> &L,
                                                   PDMat<float_t, n> &phi,
                                                   const std::false_type) {
    detail::MatOps<float_t>::invert(phi);
    detail::MatOps<float_t>::matsum(L, phi);
  }

  /**
   * @brief Creates one block in the main diagonal of the Shur Complement for
   * diagonal augmented hessian
   *
   * Augments one block in the mian diagonal of the Shur Complement:
   *
   * L <- L + B * Phi^-1 * B'
   * The semiproduct Phi^-1*B' is not created, since it is not needed in the
   * case of terminal constraints. This method only compiles succesfully for
   * terminal constraints.
   *
   * @tparam std::is_same<cons_type, detail::terminal>::value True for input
   * constraints; false otherwise.
   * @param L  PDMat (Shur Complement main diagonal --last-- block)
   * @param phi DiagMat (Augmented hessian)
   * @return std::enable_if<b>::type Void for input constraints. Ill-formed
   * otherwise.
   */
  template <bool b = std::is_same<cons_type, detail::terminal>::value>
  typename std::enable_if<b>::type
  shur_complement(PDMat<float_t, nx> &L, const DiagMat<float_t, n> &phi,
                  const std::false_type) {
    detail::MatOps<float_t>::matsum(L, phi);
  }

  template <template <class, size_t> class anymatrix,
            bool b = std::is_same<cons_type, detail::terminal>::value>
  typename std::enable_if<b>::type
  shur_complement(PDMat<float_t, nx> &L, const anymatrix<float_t, n> &phi,
                  const std::true_type) {
    detail::MatOps<float_t>::matsum(L, phi);
  }

  /**
   * @brief Helper function to initialize the augmented hessian.
   *
   * The augmented hessian shall be initialized to the corresponding penalty
   * matrix, unless the variable is --only-- linearly constrained. If the
   * given variable is linearly constrained, we will not build the augmented
   * hessian but update its factorization. In that case, phi shall not be
   * initialized to the penalty matrix but to the cholesky factoriaztion of
   * it. Finally, if the variable is unconstrained, phi shall not be
   * modified, since it is correctly initialized beforehand.
   *
   * This method is used in the general case. Phi is initialized to the
   * penalty matrix.
   *
   * To call this method, o the third
   * argument shall be of type std::false_type
   *
   * @tparam mat DiagMat or PDMat (matrix type of the penalty matrix)
   * @param X Penalty matrix
   * @param phi Augmented hessian to be initialized
   */
  template <template <class, size_t> class mat>
  void initialize_phi(const matrix<float_t, n> &X, mat<float_t, n> &phi,
                      const std::false_type, const std::false_type) {
    phi = X;
  }

  /**
   * @brief Helper function to initialize the augmented hessian.
   *
   * The augmented hessian shall be initialized to the corresponding penalty
   * matrix, unless the variable is --only-- linearly constrained. If the given
   * variable is linearly constrained, we will not build the augmented hessian
   * but update its factorization. In that case, phi shall not be initialized to
   * the penalty matrix but to the cholesky factoriaztion of it. Finally, if the
   * variable is unconstrained, phi shall not be modified, since it is correctly
   * initialized beforehand.
   *
   * This method is used in the second case: phi is initialized to the
   * factorization of the penalty matrix. To call this method, the third
   * argument must be of some type other than std::false_type. The type
   * std::true_type is recommended.
   *
   * @tparam mat DiagMat or PDMat (matrix type of the penalty matrix)
   * @tparam only_linear Type of the third argument. If it resolves to
   * std::false_type, this method will not be called, since the method above is
   * a better match.
   * @param X Penalty matrix
   * @param phi Augmented hessian to be initialized
   */
  template <template <class, size_t> class mat, class only_linear>
  void initialize_phi(const matrix<float_t, n> &X, mat<float_t, n> &phi,
                      const only_linear, const std::false_type) {
    phi = factored_penalty::factor;
  }

  /**
   * @brief Helper function to initialize the augmented hessian.
   *
   * The augmented hessian shall be initialized to the corresponding penalty
   * matrix, unless the variable is --only-- linearly constrained. If the given
   * variable is linearly constrained, we will not build the augmented hessian
   * but update its factorization. In that case, phi shall not be initialized to
   * the penalty matrix but to the cholesky factoriaztion of it. Finally, if the
   * variable is unconstrained, phi shall not be modified, since it is correctly
   * initialized beforehand.
   *
   * This method is used in the latter case: phi is left untouched, since it
   * already contains the inverse of phi.
   *
   * @tparam mat DiagMat or PDMat (matrix type of the penalty matrix)
   * @tparam only_linear Type of the third argument. If it resolves to
   * std::false_type, this method will not be called, since the method above is
   * a better match.
   * @param X Penalty matrix
   * @param phi Augmented hessian to be initialized
   */
  template <template <class, size_t> class mat, class has_no_cons>
  void initialize_phi(const matrix<float_t, n> &X, mat<float_t, n> &phi,
                      const std::false_type, const has_no_cons) {
    // Do nothing
  }

#ifdef MPC_GTEST_COMPILE
public:
#else
protected:
#endif
  static constexpr bool has_bounds{(detail::has_bounds<hc>::value) |
                                   (detail::has_bounds<sc>::value)};
  static constexpr bool has_linear{(detail::has_linear<hc>::value) |
                                   (detail::has_linear<sc>::value)};

  constraint() = default;

  constraint(const GeMat<float_t, n, nx> &dynamics,
             const matrix<float_t, n> &penalty, std::array<float_t, n> ub,
             std::array<float_t, n> lb, GeMat<float_t, k, n> F,
             std::array<float_t, k> f, std::array<float_t, n> sub,
             std::array<float_t, n> slb, GeMat<float_t, sk, n> sF,
             std::array<float_t, sk> sf)
      : constraint(dynamics, penalty, ub, lb, F, f, sub, slb, sF, sf,
                   typename std::conditional<has_no_cons, std::true_type,
                                             std::false_type>::type{},
                   typename std::conditional<
                       std::is_same<cons_type, detail::terminal>::value,
                       std::true_type, std::false_type>::type{}) {}

  template <class is_terminal>
  constraint(const GeMat<float_t, n, nx> &dynamics,
             const matrix<float_t, n> &penalty, std::array<float_t, n> ub,
             std::array<float_t, n> lb, GeMat<float_t, k, n> F,
             std::array<float_t, k> f, std::array<float_t, n> sub,
             std::array<float_t, n> slb, GeMat<float_t, sk, n> sF,
             std::array<float_t, sk> sf, const std::false_type,
             const is_terminal)
      : hc<float_t, int_t, n, k, T>{std::move(ub), std::move(lb), std::move(F),
                                    std::move(f)},
        sc<float_t, int_t, n, sk, T>(std::move(sub), std::move(slb),
                                     std::move(sF), std::move(sf)),
        factored_penalty(penalty) {}

  constraint(const GeMat<float_t, n, nx> &dynamics,
             const PDMat<float_t, n> &penalty, std::array<float_t, n> ub,
             std::array<float_t, n> lb, GeMat<float_t, k, n> F,
             std::array<float_t, k> f, std::array<float_t, n> sub,
             std::array<float_t, n> slb, GeMat<float_t, sk, n> sF,
             std::array<float_t, sk> sf, std::true_type, std::false_type)
      : hc<float_t, int_t, n, k, T>{std::move(ub), std::move(lb), std::move(F),
                                    std::move(f)},
        sc<float_t, int_t, n, sk, T>(std::move(sub), std::move(slb),
                                     std::move(sF), std::move(sf)),
        factored_penalty() {
    phi.x = penalty;
    detail::MatOps<float_t>::cholfact(phi.x);
    phixt.x = dynamics;
    detail::MatOps<float_t>::backsolve('N', phi.x, phixt.x);
    detail::MatOps<float_t>::rankkt(float_t{1}, factored_penalty::chol_block,
                                    phixt.x);
    detail::MatOps<float_t>::backsolve('T', phi.x, phixt.x);
    detail::MatOps<float_t>::invert(phi.x);
  }

  constraint(const GeMat<float_t, n, nx> &dynamics,
             const PDMat<float_t, n> &penalty, std::array<float_t, n> ub,
             std::array<float_t, n> lb, GeMat<float_t, k, n> F,
             std::array<float_t, k> f, std::array<float_t, n> sub,
             std::array<float_t, n> slb, GeMat<float_t, sk, n> sF,
             std::array<float_t, sk> sf, std::true_type, std::true_type)
      : hc<float_t, int_t, n, k, T>{std::move(ub), std::move(lb), std::move(F),
                                    std::move(f)},
        sc<float_t, int_t, n, sk, T>(std::move(sub), std::move(slb),
                                     std::move(sF), std::move(sf)),
        factored_penalty() {
    phi = penalty;
    detail::MatOps<float_t>::cholfact(phi);
    phixt = dynamics;
    detail::MatOps<float_t>::backsolve('N', phi, phixt);
    detail::MatOps<float_t>::rankkt(float_t{1}, factored_penalty::chol_block,
                                    phixt);
    detail::MatOps<float_t>::backsolve('T', phi, phixt);
    detail::MatOps<float_t>::invert(phi);
  }

  constraint(const GeMat<float_t, n, nx> &dynamics,
             const DiagMat<float_t, n> &penalty, std::array<float_t, n> ub,
             std::array<float_t, n> lb, GeMat<float_t, k, n> F,
             std::array<float_t, k> f, std::array<float_t, n> sub,
             std::array<float_t, n> slb, GeMat<float_t, sk, n> sF,
             std::array<float_t, sk> sf, const std::true_type,
             const std::false_type)
      : hc<float_t, int_t, n, k, T>{std::move(ub), std::move(lb), std::move(F),
                                    std::move(f)},
        sc<float_t, int_t, n, sk, T>(std::move(sub), std::move(slb),
                                     std::move(sF), std::move(sf)),
        factored_penalty() {
    phi.x = penalty;
    detail::MatOps<float_t>::cholfact(phi.x);
    detail::MatOps<float_t>::rankkt(phi.x.mat, factored_penalty::chol_block,
                                    dynamics);
    phixt.x = dynamics;
    for (size_t idx = 0; idx < nx; idx++) {
      std::transform(phi.x.mat.cbegin(), phi.x.mat.cend(),
                     phixt.x.mat.cbegin() + idx * n,
                     phixt.x.mat.begin() + idx * n, std::multiplies<float_t>());
    }
  }

  constraint(const GeMat<float_t, n, nx> &dynamics,
             const DiagMat<float_t, n> &penalty, std::array<float_t, n> ub,
             std::array<float_t, n> lb, GeMat<float_t, k, n> F,
             std::array<float_t, k> f, std::array<float_t, n> sub,
             std::array<float_t, n> slb, GeMat<float_t, sk, n> sF,
             std::array<float_t, sk> sf, const std::true_type,
             const std::true_type)
      : hc<float_t, int_t, n, k, T>{std::move(ub), std::move(lb), std::move(F),
                                    std::move(f)},
        sc<float_t, int_t, n, sk, T>(std::move(sub), std::move(slb),
                                     std::move(sF), std::move(sf)),
        factored_penalty() {
    phi = penalty;
    detail::MatOps<float_t>::cholfact(phi);
    detail::MatOps<float_t>::rankkt(phi.mat, factored_penalty::chol_block,
                                    dynamics);
    phixt.x = dynamics;
    for (size_t idx = 0; idx < nx; idx++) {
      std::transform(phi.mat.cbegin(), phi.mat.cend(),
                     phixt.mat.cbegin() + idx * n, phixt.mat.begin() + idx * n,
                     std::multiplies<float_t>());
    }
  }

  void initialize(const GeMat<float_t, n, nx> &dynamics,
                  const matrix<float_t, n> &penalty,
                  std::array<float_t, n> &var, std::array<float_t, n> ub,
                  std::array<float_t, n> lb, GeMat<float_t, k, n> F,
                  std::array<float_t, k> f, std::array<float_t, n> sub,
                  std::array<float_t, n> slb, GeMat<float_t, sk, n> sF,
                  std::array<float_t, sk> sf) {
    initialize(dynamics, penalty, var, ub, lb, F, f, sub, slb, sF, sf,
               typename std::conditional<has_no_cons, std::true_type,
                                         std::false_type>::type{});
  }

  void initialize(const GeMat<float_t, n, nx> &dynamics,
                  const matrix<float_t, n> &penalty,
                  std::array<float_t, n> &var, std::array<float_t, n> ub,
                  std::array<float_t, n> lb, GeMat<float_t, k, n> F,
                  std::array<float_t, k> f, std::array<float_t, n> sub,
                  std::array<float_t, n> slb, GeMat<float_t, sk, n> sF,
                  std::array<float_t, sk> sf, std::false_type) {
    hc<float_t, int_t, n, k, T>::initialize(var, std::move(ub), std::move(lb),
                                            std::move(F), std::move(f));
    sc<float_t, int_t, n, sk, T>::initialize(std::move(sub), std::move(slb),
                                             std::move(sF), std::move(sf));
    factored_penalty::initialize(penalty);
  }

  void initialize(const GeMat<float_t, n, nx> &dynamics,
                  const PDMat<float_t, n> &penalty, std::array<float_t, n> &var,
                  std::array<float_t, n> ub, std::array<float_t, n> lb,
                  GeMat<float_t, k, n> F, std::array<float_t, k> f,
                  std::array<float_t, n> sub, std::array<float_t, n> slb,
                  GeMat<float_t, sk, n> sF, std::array<float_t, sk> sf,
                  std::true_type) {
    hc<float_t, int_t, n, k, T>::initialize(var, std::move(ub), std::move(lb),
                                            std::move(F), std::move(f));
    sc<float_t, int_t, n, sk, T>::initialize(std::move(sub), std::move(slb),
                                             std::move(sF), std::move(sf));
    phi = penalty;
    detail::MatOps<float_t>::cholfact(phi);
    phixt = dynamics;
    detail::MatOps<float_t>::backsolve('N', phi, phixt);
    factored_penalty::chol_block.mat.fill(float_t{0});
    detail::MatOps<float_t>::rankkt(float_t{1}, factored_penalty::chol_block,
                                    phixt);
    detail::MatOps<float_t>::backsolve('T', phi, phixt);
    detail::MatOps<float_t>::invert(phi);
  }

  void initialize(const GeMat<float_t, n, nx> &dynamics,
                  const DiagMat<float_t, n> &penalty,
                  std::array<float_t, n> &var, std::array<float_t, n> ub,
                  std::array<float_t, n> lb, GeMat<float_t, k, n> F,
                  std::array<float_t, k> f, std::array<float_t, n> sub,
                  std::array<float_t, n> slb, GeMat<float_t, sk, n> sF,
                  std::array<float_t, sk> sf, std::true_type) {
    hc<float_t, int_t, n, k, T>::initialize(var, std::move(ub), std::move(lb),
                                            std::move(F), std::move(f));
    sc<float_t, int_t, n, sk, T>::initialize(std::move(sub), std::move(slb),
                                             std::move(sF), std::move(sf));
    phi = penalty;
    detail::MatOps<float_t>::cholfact(phi);
    factored_penalty::chol_block.mat.fill(float_t{0});
    detail::MatOps<float_t>::rankkt(phi.mat, factored_penalty::chol_block,
                                    dynamics);
    phixt = dynamics;
    for (size_t idx = 0; idx < nx; idx++) {
      std::transform(phi.mat.cbegin(), phi.mat.cend(),
                     phixt.mat.cbegin() + idx * n, phixt.mat.begin() + idx * n,
                     std::multiplies<float_t>());
    }
  }

  void new_x0(const float_t delta, const float_t rho0) {
    hc<float_t, int_t, n, k, T>::new_x0(delta);
    sc<float_t, int_t, n, sk, T>::new_x0(rho0);
  }

  template <bool b = std::is_same<cons_type, detail::input>::value>
  typename std::enable_if<b>::type
  aff_forward(const matrix<float_t, n> &R, const std::array<float_t, n> &r,
              const GeMat<float_t, n, nx> &B, const std::array<float_t, n> &var,
              const std::array<float_t, nx> &nu, PDMat<float_t, nx> &L,
              std::array<float_t, nx> &rsc, const size_t step) {
    // Initialize phi and rd
    initialize_phi(R, phi[step],
                   typename std::conditional<only_linear, std::true_type,
                                             std::false_type>::type{},
                   typename std::conditional<has_no_cons, std::true_type,
                                             std::false_type>::type{});
    rd[step] = r;
    detail::MatOps<float_t>::matvec(float_t{1}, R, var, float_t{1}, rd[step]);
    detail::MatOps<float_t>::template matvec<'N'>(float_t{1}, B, nu, float_t{1},
                                                  rd[step]);

    // Augment them with the constraints
    hc<float_t, int_t, n, k, T>::afs(
        var, phi[step], rd[step], step,
        typename std::conditional<only_linear, std::true_type,
                                  std::false_type>::type{});
    sc<float_t, int_t, n, sk, T>::afs(
        var, phi[step], rd[step], step,
        typename std::conditional<only_linear | has_no_cons, std::true_type,
                                  std::false_type>::type{});

    // Create the shur complement block
    shur_complement(B, L, phi[step], phixt[step],
                    typename std::conditional<has_no_cons, std::true_type,
                                              std::false_type>::type{});

    // Update right-hand side
    detail::MatOps<float_t>::template matvec<'T'>(float_t{-1}, phixt[step],
                                                  rd[step], float_t{1}, rsc);
  }

  template <bool b = std::is_same<cons_type, detail::state>::value>
  typename std::enable_if<b>::type
  aff_forward(const matrix<float_t, n> &Q, const std::array<float_t, n> &q,
              const GeMat<float_t, n, nx> &A, const std::array<float_t, n> &var,
              const std::array<float_t, nx> &nu,
              const std::array<float_t, nx> &nu2, PDMat<float_t, nx> &L,
              PDMat<float_t, nx> &L2, GeMat<float_t, nx, nx> &M,
              std::array<float_t, nx> &rsc, std::array<float_t, nx> &rsc2,
              const size_t step) {
    // Initialize phi and rd
    initialize_phi(Q, phi[step],
                   typename std::conditional<only_linear, std::true_type,
                                             std::false_type>::type{},
                   typename std::conditional<has_no_cons, std::true_type,
                                             std::false_type>::type{});
    rd[step] = q;
    detail::MatOps<float_t>::matvec(float_t{1}, Q, var, float_t{1}, rd[step]);
    detail::MatOps<float_t>::vecsum(float_t{-1}, nu, rd[step]);
    detail::MatOps<float_t>::template matvec<'N'>(float_t{1}, A, nu2,
                                                  float_t{1}, rd[step]);
    // Augment them with the constraints
    hc<float_t, int_t, n, k, T>::afs(
        var, phi[step], rd[step], step,
        typename std::conditional<only_linear, std::true_type,
                                  std::false_type>::type{});
    sc<float_t, int_t, n, sk, T>::afs(
        var, phi[step], rd[step], step,
        typename std::conditional<only_linear | has_no_cons, std::true_type,
                                  std::false_type>::type{});

    // Create the shur complement block
    shur_complement(A, L, L2, M, phi[step], phixt[step],
                    typename std::conditional<has_no_cons, std::true_type,
                                              std::false_type>::type{});
    // Update right-hand side
    detail::MatOps<float_t>::matvec(float_t{1}, phi[step], rd[step], float_t{1},
                                    rsc);
    // Augment next right-hand side
    detail::MatOps<float_t>::template matvec<'T'>(float_t{-1}, phixt[step],
                                                  rd[step], float_t{1}, rsc2);
  }

  template <bool b = std::is_same<cons_type, detail::terminal>::value>
  typename std::enable_if<b>::type
  aff_forward(const matrix<float_t, n> &P, const std::array<float_t, n> &p,
              const std::array<float_t, n> &var,
              const std::array<float_t, nx> &nu, PDMat<float_t, nx> &L,
              std::array<float_t, nx> &rsc, const size_t step) {
    // Initialize phi and rd
    initialize_phi(P, phi,
                   typename std::conditional<only_linear, std::true_type,
                                             std::false_type>::type{},
                   typename std::conditional<has_no_cons, std::true_type,
                                             std::false_type>::type{});
    rd = p;
    detail::MatOps<float_t>::matvec(float_t{1}, P, var, float_t{1}, rd);
    detail::MatOps<float_t>::vecsum(float_t{-1}, nu, rd);
    // Augment them with the constraints
    hc<float_t, int_t, n, k, T>::afs(
        var, phi, rd, 0,
        typename std::conditional<only_linear, std::true_type,
                                  std::false_type>::type{});
    sc<float_t, int_t, n, sk, T>::afs(
        var, phi, rd, 0,
        typename std::conditional<only_linear | has_no_cons, std::true_type,
                                  std::false_type>::type{});

    // Create the shur complement block
    shur_complement(L, phi,
                    typename std::conditional<has_no_cons, std::true_type,
                                              std::false_type>::type{});
    // Update right-hand side
    detail::MatOps<float_t>::matvec(float_t{1}, phi, rd, float_t{1}, rsc);
  }

  void aff_backward(const std::array<float_t, nx> &nu_aff,
                    const std::array<float_t, n> &u,
                    std::array<float_t, n> &u_aff, const size_t step) {
    aff_backward(
        nu_aff, u, u_aff, step,
        typename std::conditional<std::is_same<cons_type, detail::input>::value,
                                  std::true_type, std::false_type>::type{});
  }

  template <bool b = std::is_same<cons_type, detail::input>::value>
  typename std::enable_if<b>::type
  aff_backward(const std::array<float_t, nx> &nu_aff,
               const std::array<float_t, n> &u, std::array<float_t, n> &u_aff,
               const size_t step, std::true_type) {
    detail::MatOps<float_t>::template matvec<'N'>(float_t{-1}, phixt[step],
                                                  nu_aff, float_t{0}, u_aff);
    detail::MatOps<float_t>::matvec(float_t{-1}, phi[step], rd[step],
                                    float_t{1}, u_aff);
    hc<float_t, int_t, n, k, T>::abs(u, u_aff, step);
  }

  template <bool b = std::is_same<cons_type, detail::state>::value>
  typename std::enable_if<b>::type
  aff_backward(const std::array<float_t, nx> &nu_aff,
               const std::array<float_t, nx> &nu_aff_2,
               const std::array<float_t, n> &x, std::array<float_t, n> &x_aff,
               const size_t step) {
    std::transform(nu_aff.cbegin(), nu_aff.cend(), rd[step].cbegin(),
                   x_aff.begin(), std::minus<float_t>());
    std::array<float_t, n> stupid{x_aff};
    detail::MatOps<float_t>::matvec(float_t{1}, phi[step], stupid, float_t{0},
                                    x_aff);
    detail::MatOps<float_t>::template matvec<'N'>(float_t{-1}, phixt[step],
                                                  nu_aff_2, float_t{1}, x_aff);
    hc<float_t, int_t, n, k, T>::abs(x, x_aff, step);
  }

  template <bool b = std::is_same<cons_type, detail::terminal>::value>
  typename std::enable_if<b>::type
  aff_backward(const std::array<float_t, nx> &nu_aff,
               const std::array<float_t, n> &x, std::array<float_t, n> &x_aff,
               const size_t step, std::false_type) {
    std::transform(nu_aff.cbegin(), nu_aff.cend(), rd.cbegin(), x_aff.begin(),
                   std::minus<float_t>());

    std::array<float_t, n> stupid{x_aff};

    detail::MatOps<float_t>::matvec(float_t{1}, phi, stupid, float_t{0}, x_aff);
    hc<float_t, int_t, n, k, T>::abs(x, x_aff);
  }

  void cc_forward(std::array<float_t, nx> &rsc, const float_t sigmamu,
                  const size_t step) {
    cc_forward(
        rsc, sigmamu, step,
        typename std::conditional<std::is_same<cons_type, detail::input>::value,
                                  std::true_type, std::false_type>::type{});
  }

  /**
   * @brief Augments the right-hand side of the Shur Complement in the centering
   * plus correction step.
   *
   * This funtion is only enabled for input constraints, since the augmentation
   * corresponds to the input constraints.
   * First, the residual of the augmented hessian, rd_cc_u, is evaluated, with
   * the help of the hard constraints policy. Then, the right hand side (rsc) is
   * augmented as follows:
   *
   * rsc += - phixt * rd
   *
   * Where rd has been initialized properly by the method cfs.
   *
   * @tparam std::is_same<cons_type, detail::input>::value Resolves to TRUE for
   * input constraints, and resolves to FALSE otherwise
   * @param rsc Vector (right-hand size of the cc step, for the corresponding
   * time step)
   * @param sigmamu Scalar (product of the sentering paramenter sigma and the
   * scalar mu_aff)
   * @param step Integer (Time step)
   * @return std::enable_if<b>::type The return type is VOID for input
   * constraints, and is undefined otherwise.
   */
  template <bool b = std::is_same<cons_type, detail::input>::value>
  typename std::enable_if<b>::type
  cc_forward(std::array<float_t, nx> &rsc, const float_t sigmamu,
             const size_t step, const std::true_type) {
    hc<float_t, int_t, n, k, T>::cfs(rd[step], sigmamu, step);
    detail::MatOps<float_t>::template matvec<'T'>(float_t{-1}, phixt[step],
                                                  rd[step], float_t{1}, rsc);
  }

  template <bool b = std::is_same<cons_type, detail::state>::value>
  typename std::enable_if<b>::type
  cc_forward(std::array<float_t, nx> &rsc, std::array<float_t, nx> &rsc2,
             const float_t sigmamu, const size_t step) {
    hc<float_t, int_t, n, k, T>::cfs(rd[step], sigmamu, step);
    detail::MatOps<float_t>::matvec(float_t{1}, phi[step], rd[step], float_t{1},
                                    rsc);
    detail::MatOps<float_t>::template matvec<'T'>(float_t{-1}, phixt[step],
                                                  rd[step], float_t{0}, rsc2);
  }

  template <bool b = std::is_same<cons_type, detail::terminal>::value>
  typename std::enable_if<b>::type
  cc_forward(std::array<float_t, nx> &rsc, const float_t sigmamu,
             const size_t step, const std::false_type) {
    hc<float_t, int_t, n, k, T>::cfs(rd, sigmamu);
    detail::MatOps<float_t>::matvec(float_t{1}, phi, rd, float_t{1}, rsc);
  }

  void cc_backward(const std::array<float_t, nx> &nu_cc,
                   const std::array<float_t, n> &u,
                   std::array<float_t, n> &u_cc, const size_t step) {
    cc_backward(
        nu_cc, u, u_cc, step,
        typename std::conditional<std::is_same<cons_type, detail::input>::value,
                                  std::true_type, std::false_type>::type{});
  }

  template <bool b = std::is_same<cons_type, detail::input>::value>
  typename std::enable_if<b>::type
  cc_backward(const std::array<float_t, nx> &nu_cc,
              const std::array<float_t, n> &u, std::array<float_t, n> &u_cc,
              const size_t step, std::true_type) {
    detail::MatOps<float_t>::template matvec<'N'>(float_t{1}, phixt[step],
                                                  nu_cc, float_t{0}, u_cc);
    detail::MatOps<float_t>::matvec(float_t{-1}, phi[step], rd[step],
                                    float_t{-1}, u_cc);
    hc<float_t, int_t, n, k, T>::cbs(u_cc, step);
  }

  template <bool b = std::is_same<cons_type, detail::state>::value>
  typename std::enable_if<b>::type
  cc_backward(const std::array<float_t, nx> &nu_cc,
              const std::array<float_t, nx> &nu_cc_2,
              const std::array<float_t, n> &x, std::array<float_t, n> &x_cc,
              const size_t step) {
    std::transform(nu_cc.cbegin(), nu_cc.cend(), rd[step].cbegin(),
                   rd[step].begin(), std::minus<float_t>());
    detail::MatOps<float_t>::matvec(float_t{1}, phi[step], rd[step], float_t{0},
                                    x_cc);
    detail::MatOps<float_t>::template matvec<'N'>(float_t{-1}, phixt[step],
                                                  nu_cc_2, float_t{1}, x_cc);
    hc<float_t, int_t, n, k, T>::cbs(x_cc, step);
  }

  template <bool b = std::is_same<cons_type, detail::terminal>::value>
  typename std::enable_if<b>::type
  cc_backward(const std::array<float_t, nx> &nu_cc,
              const std::array<float_t, n> &x, std::array<float_t, n> &x_cc,
              const size_t step, std::false_type) {
    std::transform(nu_cc.cbegin(), nu_cc.cend(), rd.cbegin(), rd.begin(),
                   std::minus<float_t>());
    detail::MatOps<float_t>::matvec(float_t{1}, phi, rd, float_t{0}, x_cc);
    hc<float_t, int_t, n, k, T>::cbs(x_cc);
  }
};

} // namespace mehrotra

#endif