#ifndef HARD_CONS_HPP_
#define HARD_CONS_HPP_

#include <algorithm>

#include "matrix/matrix.hpp"

namespace mehrotra {
namespace hardcons {

/**
 * @brief Class managing the hard constraints... when there are no constraints
 *
 * @tparam float_t Floating point type
 * @tparam int_t Integer type
 * @tparam n Variable size (nu for inputs, nx for states)
 * @tparam k Number of linear constraints (should be zero)
 * @tparam T Number of steps (T for inpu  ts, T-1 for states, 1 for terminal)
 */
template <class float_t, class int_t, size_t n, size_t k, size_t T>
class unconstrained {
private:
  static_assert(k == 0,
                "Expected the number of linear constraints to be set to zero.");

#ifdef MPC_GTEST_COMPILE
public:
#else
protected:
#endif
  unconstrained() = default;

  unconstrained(std::array<float_t, n> ub, std::array<float_t, n> lb,
                GeMat<float_t, k, n> F, std::array<float_t, k> f)
      : unconstrained() {}

  void initialize(std::array<float_t, n> &x, std::array<float_t, n> _ub,
                  std::array<float_t, n> _lb, GeMat<float_t, k, n>,
                  std::array<float_t, k>) {
    x.fill(0);
  }

  template <template <class, size_t> class matrix,
            class only_linear = std::false_type>
  void afs(const std::array<float_t, n> &x, matrix<float_t, n> &phi,
           std::array<float_t, n> &rd, const size_t step = 0,
           const only_linear = only_linear{}) {
    return;
  }

  void abs(const std::array<float_t, n> &x, std::array<float_t, n> &x_aff,
           const size_t step = 0) {
    return;
  }

  void cfs(std::array<float_t, n> &rd, const float_t sigmamu,
           const size_t step = 0) {
    return;
  }

  void cbs(std::array<float_t, n> &x_cc, const size_t step = 0) { return; }

public:
  void initial_value(std::array<float_t, n> &var) const {
    var.fill(float_t{0});
  }

  float_t duality_gap() const { return float_t{0}; }

  float_t affine_gap(const float_t alpha) const { return float_t{0}; }

  void linesearch(float_t &alpha) const { return; }

  void take_step(const float_t alpha) { return; }

  void new_x0(const float_t delta) { return; }
};

/**
 * @brief Class managing the hard constraints when they consist just in simple
 * bounds
 *
 * @tparam float_t Floating point type
 * @tparam int_t Integer type
 * @tparam n Variable size (nu for inputs, nx for states)
 * @tparam k Number of linear constraints (should be zero)
 * @tparam T Number of steps (T for inputs, T-1 for states, 1 for terminal)
 */
template <class float_t, class int_t, size_t n, size_t k, size_t T>
class simplebounds {
private:
  static_assert(k == 0,
                "Expected the number of linear constraints to be set to zero.");

  static constexpr size_t N{20};

  std::array<float_t, n> ub{}, lb{};

  std::array<std::array<float_t, n>, T> s1{}, s1_aff{};
  std::array<std::array<float_t, n>, T> s2{}, s2_aff{};

  std::array<std::array<float_t, n>, T> lambda1{}, lambda1_aff{};
  std::array<std::array<float_t, n>, T> lambda2{}, lambda2_aff{};

  std::array<std::array<float_t, n>, T> d1{}, d2{};

  std::array<std::array<float_t, n>, T> ri1{}, ri2{};

#ifdef MPC_GTEST_COMPILE
public:
#else
protected:
#endif
  /**
   * @brief Construct a new simplebounds object.
   * Allocates memory for all private fields.
   *
   */
  simplebounds() = default;

  /**
   * @brief Construct a new simplebounds object
   *
   * Give values to the upper and lower bounds.
   * Initialize x to the middle point of the bounds.
   * Initialize the slacks to half the distance between bounds.
   * Initialize the Lagrange multipliers to a positive constant
   *
   * @param x Vector. First state vector in the sequence of predicted states
   * @param _ub Upper bound
   * @param _lb Lower bound
   */
  simplebounds(std::array<float_t, n> _ub, std::array<float_t, n> _lb,
               GeMat<float_t, k, n>, std::array<float_t, k>)
      : ub(std::move(_ub)), lb(std::move(_lb)) {
    // Initialize the slacks to half the distance between bounds
    std::transform(ub.cbegin(), ub.cend(), lb.cbegin(), s1[0].begin(),
                   [](float_t u, float_t l) { return (u - l) / 2; });
    s1.fill(s1[0]);
    s2.fill(s1[0]);

    // Initialize the lambda's to the static const value N
    lambda1[0].fill(N);
    lambda1.fill(lambda1[0]);
    lambda2.fill(lambda1[0]);
  }

  /**
   * @brief Initializes the object with the given data.
   *
   * This function shall be used when the object has been default constructed,
   * but no data has been provided. In such scenario, the memory required by the
   * object has been allocated, but there is no data in that memory. This method
   * allows to provide all the data at once.
   *
   * This function may be used as well whenever the user wants to reset the
   * object.
   *
   * @param x Vector. First element in the sequence of state variables, which
   * shall be initialized.
   * @param _ub Vector. Upper bound.
   * @param _lb Vector. Lower bound.
   */
  void initialize(std::array<float_t, n> &x, std::array<float_t, n> _ub,
                  std::array<float_t, n> _lb, GeMat<float_t, k, n>,
                  std::array<float_t, k>) {
    ub = std::move(_ub);
    lb = std::move(_lb);

    // Initialize 'x' to the middle point of the constraints
    std::transform(ub.cbegin(), ub.cend(), lb.cbegin(), x.begin(),
                   [](float_t u, float_t l) { return (u + l) / 2; });

    // Initialize the slacks to half the distance between bounds
    std::transform(ub.cbegin(), ub.cend(), lb.cbegin(), s1[0].begin(),
                   [](float_t u, float_t l) { return (u - l) / 2; });
    s1.fill(s1[0]);
    s2.fill(s1[0]);

    // Initialize the lambda's to the static const value N
    lambda1[0].fill(N);
    lambda1.fill(lambda1[0]);
    lambda2.fill(lambda1[0]);
  }

  /**
   * @brief Contribution of the hard constraints to the affine forward step
   *
   * On output, the function augments both the given matrix phi and the
   * corresponding right hand side rd. Internally, it also updates the vectors
   * 'd' and 'ri'.
   *
   * @tparam matrix Template parameter, may resolve to PDMat or DiagMat
   * @param x Variable of the corresponding time step
   * @param phi Matrix to be augmented
   * @param rd Right-hand side to be augmented
   * @param step Time step, 0 <= step < T
   */
  template <template <class, size_t> class matrix, class only_linear>
  void afs(const std::array<float_t, n> &x, matrix<float_t, n> &phi,
           std::array<float_t, n> &rd, const size_t step = 0,
           const only_linear = only_linear{}) {
    // Evaluate lambda/s
    std::transform(lambda1[step].cbegin(), lambda1[step].cend(),
                   s1[step].cbegin(), d1[step].begin(),
                   [](float_t l, float_t s) { return l / s; });
    std::transform(lambda2[step].cbegin(), lambda2[step].cend(),
                   s2[step].cbegin(), d2[step].begin(),
                   [](float_t l, float_t s) { return l / s; });

    // Augment phi
    mehrotra::detail::MatOps<float_t>::diagsum(phi, d1[step], d2[step]);

    // Evaluate ri and augment rd
    for (size_t idx = 0; idx < ri1[step].size(); idx++) {
      ri1[step][idx] = x[idx] + s1[step][idx] - ub[idx];
      ri2[step][idx] = -x[idx] + s2[step][idx] + lb[idx];
      rd[idx] += lambda1[step][idx] * ri1[step][idx] -
                 lambda2[step][idx] * ri2[step][idx];
    }
  }

  /**
   * @brief Contribution of the hard constraints to the affine backwards step
   *
   * Uses x and x_aff to recover lambda_aff and s_aff
   *
   * @param x Variable of the corresponding time step
   * @param x_aff Affine variable of the corresponding time step
   * @param step Time step
   */
  void abs(const std::array<float_t, n> &x, const std::array<float_t, n> &x_aff,
           const size_t step = 0) {
    // Recover lambda_aff
    for (size_t idx = 0; idx < lambda1_aff[step].size(); idx++) {
      lambda1_aff[step][idx] = d1[step][idx] * (x[idx] + x_aff[idx] - ub[idx]);
      lambda2_aff[step][idx] = d2[step][idx] * (-x[idx] - x_aff[idx] + lb[idx]);
    }

    // Recover s_aff
    std::transform(x_aff.cbegin(), x_aff.cend(), ri1[step].cbegin(),
                   s1_aff[step].begin(),
                   [](float_t x_aff, float_t ri) { return -x_aff - ri; });
    std::transform(x_aff.cbegin(), x_aff.cend(), ri2[step].cbegin(),
                   s2_aff[step].begin(),
                   [](float_t x_aff, float_t ri) { return x_aff - ri; });
  }

  /**
   * @brief Contribution of the hard constraints to the cc forward step
   *
   * Augments rd. The complementary slackness residual rs is calculated
   * and stored in ri.
   *
   * @param rd Right-hand side vector to be augmented
   * @param sigmamu Scalar
   * @param step Integer
   */
  void cfs(std::array<float_t, n> &rd, const float_t sigmamu,
           const size_t step = 0) {
    // Evaluate rs and store it in ri
    std::transform(s1_aff[step].cbegin(), s1_aff[step].cend(),
                   lambda1_aff[step].cbegin(), ri1[step].begin(),
                   [sigmamu](float_t s, float_t l) { return s * l - sigmamu; });
    std::transform(s2_aff[step].cbegin(), s2_aff[step].cend(),
                   lambda2_aff[step].cbegin(), ri2[step].begin(),
                   [sigmamu](float_t s, float_t l) { return s * l - sigmamu; });

    for (size_t idx = 0; idx < n; idx++) {
      rd[idx] =
          -ri1[step][idx] / s1[step][idx] + ri2[step][idx] / s2[step][idx];
    }
  }

  /**
   * @brief Contribution of the hard constraints to the cc backwards step
   *
   * Recovers the variables lambda_cc and s_cc from x_cc. Then, aggregates the
   * affine and cc steps and stores the sum in the affine vector. The variables
   * lambda_cc and s_cc are not created independently, but they are added
   * directly to the variables lambda_aff and s_aff.
   *
   * @param x_cc CC variable of the corresponding time step
   * @param step Integer
   */
  void cbs(std::array<float_t, n> &x_cc, const size_t step = 0) {
    // Recover lambda_cc
    for (size_t idx = 0; idx < n; idx++) {
      lambda1_aff[step][idx] +=
          d1[step][idx] * (x_cc[idx] - ri1[step][idx] / lambda1[step][idx]);
      lambda2_aff[step][idx] +=
          d2[step][idx] * (-x_cc[idx] - ri2[step][idx] / lambda2[step][idx]);
    }

    // Recover s_cc
    std::transform(s1_aff[step].cbegin(), s1_aff[step].cend(), x_cc.cbegin(),
                   s1_aff[step].begin(), std::minus<float_t>());
    std::transform(s2_aff[step].cbegin(), s2_aff[step].cend(), x_cc.cbegin(),
                   s2_aff[step].begin(), std::plus<float_t>());
  }

public:
  void initial_value(std::array<float_t, n> &var) const {
    // Initialize 'var' to the middle point of the constraints
    std::transform(ub.cbegin(), ub.cend(), lb.cbegin(), var.begin(),
                   [](float_t u, float_t l) { return (u + l) / 2; });
  }

  /**
   * @brief Evaluates the dotproduct lambda's
   *
   * @return float_t Dotproduct lambda's
   */
  float_t duality_gap() const {
    float_t r{0};
    for (size_t idx = 0; idx < T; idx++) {
      r += mehrotra::detail::MatOps<float_t>::dotproduct(s1[idx], lambda1[idx]);
      r += mehrotra::detail::MatOps<float_t>::dotproduct(s2[idx], lambda2[idx]);
    }
    return r;
  }

  /**
   * @brief Evaluates the dotproduct lambda's after the affine step is taken
   *
   * Internally, stores s+alpha*s_aff in a temporary, auxiliary variable (same
   * goes for lambda), where alpha is the step size
   *
   * @param alpha Scalar
   * @return float_t Dotproduct lambda's after the affine step is taken
   */
  float_t affine_gap(const float_t alpha) const {
    float_t r{0};
    std::array<float_t, n> l_aux, s_aux;
    for (size_t step = 0; step < T; step++) {
      std::transform(s1[step].cbegin(), s1[step].cend(), s1_aff[step].cbegin(),
                     s_aux.begin(), [alpha](float_t x, float_t x_aff) {
                       return x + alpha * x_aff;
                     });
      std::transform(
          lambda1[step].cbegin(), lambda1[step].cend(),
          lambda1_aff[step].cbegin(), l_aux.begin(),
          [alpha](float_t x, float_t x_aff) { return x + alpha * x_aff; });
      r += mehrotra::detail::MatOps<float_t>::dotproduct(s_aux, l_aux);

      std::transform(s2[step].cbegin(), s2[step].cend(), s2_aff[step].cbegin(),
                     s_aux.begin(), [alpha](float_t x, float_t x_aff) {
                       return x + alpha * x_aff;
                     });
      std::transform(
          lambda2[step].cbegin(), lambda2[step].cend(),
          lambda2_aff[step].cbegin(), l_aux.begin(),
          [alpha](float_t x, float_t x_aff) { return x + alpha * x_aff; });
      r += mehrotra::detail::MatOps<float_t>::dotproduct(s_aux, l_aux);
    }
    return r;
  }

  /**
   * @brief Maximum stepsize
   *
   * Modifies alpha such that no constraint is violated. Note that alpha is
   * negative, on input and output.
   *
   * @param alpha Scalar
   */
  void linesearch(float_t &alpha) const {
    std::array<float_t, n> aux{};
    for (size_t step = 0; step < T; step++) {
      std::transform(s1[step].cbegin(), s1[step].cend(), s1_aff[step].cbegin(),
                     aux.begin(), std::divides<float_t>());
      for (size_t idx = 0; idx < n; idx++) {
        if (aux[idx] < 0 && aux[idx] > alpha) {
          alpha = aux[idx];
        }
      }

      std::transform(s2[step].cbegin(), s2[step].cend(), s2_aff[step].cbegin(),
                     aux.begin(), std::divides<float_t>());
      for (size_t idx = 0; idx < n; idx++) {
        if (aux[idx] < 0 && aux[idx] > alpha) {
          alpha = aux[idx];
        }
      }

      std::transform(lambda1[step].cbegin(), lambda1[step].cend(),
                     lambda1_aff[step].cbegin(), aux.begin(),
                     std::divides<float_t>());
      for (size_t idx = 0; idx < n; idx++) {
        if (aux[idx] < 0 && aux[idx] > alpha) {
          alpha = aux[idx];
        }
      }

      std::transform(lambda2[step].cbegin(), lambda2[step].cend(),
                     lambda2_aff[step].cbegin(), aux.begin(),
                     std::divides<float_t>());
      for (size_t idx = 0; idx < n; idx++) {
        if (aux[idx] < 0 && aux[idx] > alpha) {
          alpha = aux[idx];
        }
      }
    }
  }

  /**
   * @brief Takes a step in the direction stored in the affine variable, with
   * stepsize alpha
   *
   * @param alpha Scalar
   */
  void take_step(const float_t alpha) {
    for (size_t step = 0; step < T; step++) {
      detail::MatOps<float_t>::vecsum(alpha, s1_aff[step], s1[step]);
      detail::MatOps<float_t>::vecsum(alpha, s2_aff[step], s2[step]);
      detail::MatOps<float_t>::vecsum(alpha, lambda1_aff[step], lambda1[step]);
      detail::MatOps<float_t>::vecsum(alpha, lambda2_aff[step], lambda2[step]);
    }
  }

  /**
   * @brief Hotstart within a sequence of QPs
   *
   * Assuming that a new MPC problem has to be solved in the receding horizon
   * scheme, provides some warm-start capability.
   *
   * @param delta Scalar
   */
  void new_x0(const float_t delta) {
    // Rotate
    std::rotate(s1.begin(), s1.begin() + 1, s1.end());
    std::rotate(s2.begin(), s2.begin() + 1, s2.end());
    std::rotate(lambda1.begin(), lambda1.begin() + 1, lambda1.end());
    std::rotate(lambda2.begin(), lambda2.begin() + 1, lambda2.end());
    // Complete the tail
    s1[T - 1] = s1[T - 2];
    s2[T - 1] = s2[T - 2];
    lambda1[T - 1] = lambda1[T - 2];
    lambda2[T - 1] = lambda2[T - 2];
    // Add some positive constant
    for (size_t step = 0; step < T; step++) {
      std::transform(s1.cbegin(), s1.cend(), s1.begin(),
                     [delta](float_t val) { return val + delta; });
      std::transform(s2.cbegin(), s2.cend(), s2.begin(),
                     [delta](float_t val) { return val + delta; });
      std::transform(lambda1.cbegin(), lambda1.cend(), lambda1.begin(),
                     [delta](float_t val) { return val + delta; });
      std::transform(lambda2.cbegin(), lambda2.cend(), lambda2.begin(),
                     [delta](float_t val) { return val + delta; });
    }
  }
};

template <class float_t, class int_t, size_t n, size_t k, size_t T>
class linearbounds {
private:
  static_assert(
      k > 0,
      "Expected the number of linear constraints to be set greater than zero.");

  static constexpr size_t N{20};

  GeMat<float_t, k, n> F{};
  std::array<float_t, k> f{};

  std::array<std::array<float_t, k>, T> s{}, s_aff{};

  std::array<std::array<float_t, k>, T> lambda{}, lambda_aff{};
  std::array<float_t, k> lambda_cc;

  std::array<std::array<float_t, k>, T> d{};

  std::array<std::array<float_t, k>, T> ri{};

#ifdef MPC_GTEST_COMPILE
public:
#else
protected:
#endif
  /**
   * @brief Construct a new simplebounds object.
   * Allocates memory for all private fields.
   *
   */
  linearbounds() = default;

  /**
   * @brief Construct a new linearbounds object
   *
   * Give values to the constraints coefficient matrix, and to the right hand
   * side of the inequality constraints.
   * Initialize the slack variables to their real value, assuming x=0. If x=0 is
   * not feasible for some constraint, initialize the corresponding slack to
   * some positive constant.
   * Initialize the Lagrange multipliers to a positive constant
   *
   * @param x
   * @param _F Constraints coefficient matrix
   * @param _f Right-hand side of the inequality constraint
   */
  linearbounds(std::array<float_t, n>, std::array<float_t, n>,
               GeMat<float_t, k, n> _F, std::array<float_t, k> _f)
      : F(std::move(_F)), f(std::move(_f)) {

    // Initialize the slacks to their real value, given that 'x' is initialized
    // to zero
    s[0] = f;

    // Make sure that the slack is initialized to a positive value
    for (size_t idx = 0; idx < k; idx++) {
      s[0][idx] = std::max(s[0][idx], float_t{1});
    }
    s.fill(s[0]);

    // Initialize the lambda's to the static const value N
    lambda[0].fill(N);
    lambda.fill(lambda[0]);
  }

  /**
   * @brief Initializes the object with the given data.
   *
   * This function shall be used when the object has been default constructed,
   * but no data has been provided. In such scenario, the memory required by the
   * object has been allocated, but there is no data in that memory. This method
   * allows to provide all the data at once.
   *
   * This function may be used as well whenever the user wants to reset the
   * object.
   *
   * @param x Vector. First element in the sequence of state variables, which
   * shall be initialized.
   * @param _F Constraints coefficient matrix
   * @param _f Right-hand side of the inequality constraint
   */
  void initialize(std::array<float_t, n> &x, std::array<float_t, n>,
                  std::array<float_t, n>, GeMat<float_t, k, n> _F,
                  std::array<float_t, k> _f) {
    F = std::move(_F);
    f = std::move(_f);

    // Initialize the slacks to their real value
    x = std::array<float_t, n>{0};
    s[0] = f;
    detail::MatOps<float_t>::template matvec<'N'>(float_t{-1}, F, x, float_t{1},
                                                  s[0]);
    // Make sure that the slack is initialized to a positive value
    for (size_t idx = 0; idx < k; idx++) {
      s[0][idx] = std::max(s[0][idx], float_t{1});
    }
    s.fill(s[0]);

    // Initialize the lambda's to the static const value N
    lambda[0].fill(N);
    lambda.fill(lambda[0]);
  }

  /**
   * @brief Contribution of the hard constraints to the affine forward step.
   *
   * On output, the function augments the given matrix phi and the corresponding
   * right-hand side rd. Internally, it also updates the vectors 'd' and 'ri'.
   *
   * This method assumes that the soft constraints include some sort of upper
   * and lower bounds. Hence, it does not perform Cholesky factor updates, but
   * just add the constraints to the phi matrix via rank-k updates.
   *
   * @param x Variable of the corresponding time step
   * @param phi Matrix to be augmented
   * @param rd Right-hand side to be augmented
   * @param step Time step, 0 <= step < T
   */
  void afs(const std::array<float_t, n> &x, PDMat<float_t, n> &phi,
           std::array<float_t, n> &rd, const size_t step,
           const std::false_type) {
    // Evaluate lambda/s
    std::transform(lambda[step].cbegin(), lambda[step].cend(), s[step].cbegin(),
                   d[step].begin(), std::divides<float_t>());

    // Augment phi
    mehrotra::detail::MatOps<float_t>::rankkt(d[step], phi, F);

    // Evaluate ri
    std::transform(s[step].cbegin(), s[step].cend(), f.cbegin(),
                   ri[step].begin(), std::minus<float_t>());
    detail::MatOps<float_t>::template matvec<'N'>(float_t{1}, F, x, float_t{1},
                                                  ri[step]);

    // Augment rd
    {
      std::array<float_t, k> aux;
      std::transform(d[step].cbegin(), d[step].cend(), ri[step].cbegin(),
                     aux.begin(), std::multiplies<float_t>());
      detail::MatOps<float_t>::template matvec<'T'>(float_t{1}, F, aux,
                                                    float_t{1}, rd);
    }
  }

  /**
   * @brief Contribution of the hard constraints to the affine forward step.
   *
   * On output, the function augments the given matrix phi and the corresponding
   * right-hand side rd. Internally, it also updates the vectors 'd' and 'ri'.
   *
   * This method assumes that the given variable is only linearly constrained.
   * In that case, it does not add the constraints to the matrix phi. Instead,
   * phi contains a Cholesky factorization, which is update via Cholesky rank-1
   * updates.
   *
   * @param x Variable of the corresponding time step
   * @param phi Cholesky factor to be updated
   * @param rd Right-hand side to be augmented
   * @param step Time step, 0 <= step < T
   */
  void afs(const std::array<float_t, n> &x, PDMat<float_t, n> &phi,
           std::array<float_t, n> &rd, const size_t step,
           const std::true_type) {
    // Evaluate lambda/s
    std::transform(lambda[step].cbegin(), lambda[step].cend(), s[step].cbegin(),
                   d[step].begin(), std::divides<float_t>());

    // Augment phi
    {
      std::array<float_t, n> aux;
      for (size_t row = 0; row < k; row++) {
        for (size_t idx = 0; idx < n; idx++) {
          aux[idx] = std::sqrt(d[step][row]) * F.mat[row + k * idx];
        }
        detail::MatOps<float_t>::cholupdate(phi, aux);
      }
    }

    // Evaluate ri
    std::transform(s[step].cbegin(), s[step].cend(), f.cbegin(),
                   ri[step].begin(), std::minus<float_t>());
    detail::MatOps<float_t>::template matvec<'N'>(float_t{1}, F, x, float_t{1},
                                                  ri[step]);

    // Augment rd
    {
      std::array<float_t, k> aux;
      std::transform(d[step].cbegin(), d[step].cend(), ri[step].cbegin(),
                     aux.begin(), std::multiplies<float_t>());
      detail::MatOps<float_t>::template matvec<'T'>(float_t{1}, F, aux,
                                                    float_t{1}, rd);
    }
  }

  /**
   * @brief Contribution of the hard constraints to the affine backwards step
   *
   * Uses x and x_aff to recover lambda_aff and s_aff
   *
   * @param x Variable of the corresponding time step
   * @param x_aff Affine variable of the corresponding time step
   * @param step Time step
   */
  void abs(const std::array<float_t, n> &x, const std::array<float_t, n> &x_aff,
           const size_t step = 0) {
    // Recover lambda_aff
    {
      std::array<float_t, n> aux;
      std::transform(x.cbegin(), x.cend(), x_aff.cbegin(), aux.begin(),
                     std::plus<float_t>());
      lambda_aff[step] = f;
      detail::MatOps<float_t>::template matvec<'N'>(
          float_t{1}, F, aux, float_t{-1}, lambda_aff[step]);
    }
    std::transform(d[step].cbegin(), d[step].cend(), lambda_aff[step].cbegin(),
                   lambda_aff[step].begin(), std::multiplies<float_t>());

    // Recover s_aff
    s_aff[step] = ri[step];
    detail::MatOps<float_t>::template matvec<'N'>(float_t{-1}, F, x_aff,
                                                  float_t{-1}, s_aff[step]);
  }

  /**
   * @brief Contribution of the hard constraints to the cc forward step
   *
   * Augments rd. The complementary slackness residual divided by s, rs/s, is
   * calculated and stored in ri.
   *
   * @param rd Right-hand side vector to be augmented
   * @param sigmamu Scalar
   * @param step Integer
   */
  void cfs(std::array<float_t, n> &rd, const float_t sigmamu,
           const size_t step = 0) {
    // Evaluate rs/s and store it in ri
    for (size_t idx = 0; idx < k; idx++) {
      ri[step][idx] =
          (s_aff[step][idx] * lambda_aff[step][idx] - sigmamu) / s[step][idx];
    }

    // Create rd
    detail::MatOps<float_t>::template matvec<'T'>(float_t{-1}, F, ri[step],
                                                  float_t{0}, rd);
  }

  /**
   * @brief Contribution of the hard constraints to the cc backwards step
   *
   * Recovers the variables lambda_cc and s_cc from x_cc. Then, aggregates the
   * affine and cc steps and stores the sum in the affine vector.
   *
   * The variables lambda_cc and s_cc are created in a temporary vector, but
   * they are added immediately to the variables lambda_aff and s_aff.
   *
   * BEWARE: the vector ri does not contain rs, but rs/s
   *
   * @param x_cc CC variable of the corresponding time step
   * @param step Integer
   */
  void cbs(std::array<float_t, n> &x_cc, const size_t step = 0) {
    // Construct lambda_cc
    std::transform(ri[step].cbegin(), ri[step].cend(), d[step].cbegin(),
                   lambda_cc.begin(), std::divides<float_t>());
    detail::MatOps<float_t>::template matvec<'N'>(float_t{1}, F, x_cc,
                                                  float_t{-1}, lambda_cc);
    std::transform(d[step].cbegin(), d[step].cend(), lambda_cc.cbegin(),
                   lambda_cc.begin(), std::multiplies<float_t>());
    // Aggregate cc and aff step
    detail::MatOps<float_t>::vecsum(float_t{1}, lambda_cc, lambda_aff[step]);

    // Construct s_cc by aggregating it to s_aff
    detail::MatOps<float_t>::template matvec<'N'>(float_t{-1}, F, x_cc,
                                                  float_t{1}, s_aff[step]);
  }

public:
  /**
   * @brief Initializes the input or state variable
   *
   * In the case of linear constraints, the given variables is initialized to
   * zero
   *
   * @param var Variable to be initialized
   */
  void initial_value(std::array<float_t, n> &var) const {
    var.fill(float_t{0});
  }

  /**
   * @brief Evaluates the dotproduct lambda's
   *
   * @return float_t Dotproduct lambda's
   */
  float_t duality_gap() const {
    float_t r{0};
    for (size_t idx = 0; idx < T; idx++) {
      r += mehrotra::detail::MatOps<float_t>::dotproduct(s[idx], lambda[idx]);
    }
    return r;
  }

  /**
   * @brief Evaluates the dotproduct lambda's after the affine step is taken
   *
   * Internally, stores s+alpha*s_aff in a temporary, auxiliary variable (same
   * goes for lambda), where alpha is the step size.
   *
   * @param alpha Scalar
   * @return float_t Dotproduct lambda's after the affine step is taken
   */
  float_t affine_gap(const float_t alpha) const {
    float_t r{0};
    std::array<float_t, k> l_aux, s_aux;
    for (size_t step = 0; step < T; step++) {
      std::transform(
          s[step].cbegin(), s[step].cend(), s_aff[step].cbegin(), s_aux.begin(),
          [alpha](float_t x, float_t x_aff) { return x + alpha * x_aff; });
      std::transform(
          lambda[step].cbegin(), lambda[step].cend(), lambda_aff[step].cbegin(),
          l_aux.begin(),
          [alpha](float_t x, float_t x_aff) { return x + alpha * x_aff; });
      r += mehrotra::detail::MatOps<float_t>::dotproduct(s_aux, l_aux);
    }
    return r;
  }

  /**
   * @brief Maximum stepsize
   *
   * Modifies alpha such that no constraint is violated. Note that alpha is
   * negative, on input and output.
   *
   * @param alpha Scalar
   */
  void linesearch(float_t &alpha) const {
    std::array<float_t, k> aux{};
    for (size_t step = 0; step < T; step++) {
      std::transform(s[step].cbegin(), s[step].cend(), s_aff[step].cbegin(),
                     aux.begin(), std::divides<float_t>());
      for (size_t idx = 0; idx < k; idx++) {
        if (aux[idx] < 0 && aux[idx] > alpha) {
          alpha = aux[idx];
        }
      }

      std::transform(lambda[step].cbegin(), lambda[step].cend(),
                     lambda_aff[step].cbegin(), aux.begin(),
                     std::divides<float_t>());
      for (size_t idx = 0; idx < k; idx++) {
        if (aux[idx] < 0 && aux[idx] > alpha) {
          alpha = aux[idx];
        }
      }
    }
  }

  /**
   * @brief Takes a step in the direction stored in the affine variable, with
   * stepsize alpha
   *
   * @param alpha Scalar
   */
  void take_step(const float_t alpha) {
    for (size_t step = 0; step < T; step++) {
      detail::MatOps<float_t>::vecsum(alpha, s_aff[step], s[step]);
      detail::MatOps<float_t>::vecsum(alpha, lambda_aff[step], lambda[step]);
    }
  }

  /**
   * @brief Hotstart within a sequence of QPs
   *
   * Assuming that a new MPC problem has to be solved in the receding horizon
   * scheme, provides some warm-start capability.
   *
   * @param delta Scalar
   */
  void new_x0(const float_t delta) {
    // Rotate
    std::rotate(s.begin(), s.begin() + 1, s.end());
    std::rotate(lambda.begin(), lambda.begin() + 1, lambda.end());
    // Complete the tail
    s[T - 1] = s[T - 2];
    lambda[T - 1] = lambda[T - 2];
    // Add some positive constant
    for (size_t step = 0; step < T; step++) {
      std::transform(s.cbegin(), s.cend(), s.begin(),
                     [delta](float_t val) { return val + delta; });
      std::transform(lambda.cbegin(), lambda.cend(), lambda.begin(),
                     [delta](float_t val) { return val + delta; });
    }
  }
};

template <class float_t, class int_t, size_t n, size_t k, size_t T>
class fullbounds {
private:
  static_assert(
      k > 0,
      "Expected the number of linear constraints to be set greater than zero.");

  static constexpr size_t N{20};

  std::array<float_t, n> ub{}, lb{};

  GeMat<float_t, k, n> F{};
  std::array<float_t, k> f{};

  std::array<std::array<float_t, n>, T> s1{}, s2{};
  std::array<std::array<float_t, k>, T> s3{};
  std::array<std::array<float_t, n>, T> s1_aff{}, s2_aff{};
  std::array<std::array<float_t, k>, T> s3_aff{};

  std::array<std::array<float_t, n>, T> lambda1{}, lambda2{};
  std::array<std::array<float_t, k>, T> lambda3{};
  std::array<std::array<float_t, n>, T> lambda1_aff{}, lambda2_aff{};
  std::array<std::array<float_t, k>, T> lambda3_aff{};

  std::array<std::array<float_t, n>, T> d1{}, d2{};
  std::array<std::array<float_t, k>, T> d3{};

  std::array<std::array<float_t, n>, T> ri1{}, ri2{};
  std::array<std::array<float_t, k>, T> ri3{};

protected:
  fullbounds() = default;

  fullbounds(std::array<float_t, n> _ub, std::array<float_t, n> _lb,
             GeMat<float_t, k, n> _F, std::array<float_t, k> _f)
      : ub{std::move(_ub)}, lb{std::move(_lb)}, F{std::move(_F)}, f{std::move(
                                                                      _f)} {
    // Initialize the slacks to half the distance between bounds
    std::transform(ub.cbegin(), ub.cend(), lb.cbegin(), s1[0].begin(),
                   [](float_t u, float_t l) { return (u - l) / 2; });
    s1.fill(s1[0]);
    s2.fill(s1[0]);

    // Initialize the other slack to abs(f)
    std::transform(f.cbegin(), f.cend(), s3[0].begin(),
                   [](float_t f) { return std::max(f, float_t{1}); });
    s3.fill(s3[0]);

    // Initialize the lambda's to the static const value N
    lambda1[0].fill(N);
    lambda1.fill(lambda1[0]);
    lambda2.fill(lambda1[0]);
    lambda3[0].fill(N);
    lambda3.fill(lambda3[0]);
  }

  void initialize(std::array<float_t, n> &var, std::array<float_t, n> _ub,
                  std::array<float_t, n> _lb, GeMat<float_t, k, n> _F,
                  std::array<float_t, k> _f) {
    ub = std::move(_ub);
    lb = std::move(_lb);
    F = std::move(_F);
    f = std::move(_f);

    // Initialize var to the middle point between bounds
    std::transform(ub.cbegin(), ub.cend(), lb.cbegin(), var.begin(),
                   [](float_t u, float_t l) { return (u + l) / 2; });

    // Initialize the slacks to half the distance between bounds
    std::transform(ub.cbegin(), ub.cend(), lb.cbegin(), s1[0].begin(),
                   [](float_t u, float_t l) { return (u - l) / 2; });
    s1.fill(s1[0]);
    s2.fill(s1[0]);

    // Initialize the other slack to abs(f)
    std::transform(f.cbegin(), f.cend(), s3[0].begin(),
                   [](float_t f) { return std::max(f, float_t{1}); });
    s3.fill(s3[0]);

    // Initialize the lambda's to the static const value N
    lambda1[0].fill(N);
    lambda1.fill(lambda1[0]);
    lambda2.fill(lambda1[0]);
    lambda3[0].fill(N);
    lambda3.fill(lambda3[0]);
  }

  /**
   * @brief Contribution of the hard constraints to the affine forward step.
   *
   * On output, the function augments the given matrix phi and the corresponding
   * right-hand side rd. Internally, it also updates the vectors 'd' and 'ri'.
   *
   * Because there are bounds on the variable, the linear constraints are not
   * added via Cholesky factor updates. Instead, they are added to the phi
   * matrix via rank-k updates.
   *
   * @tparam only_linear Can resolve to anything without affecting the
   * functionality. It should resolve to std::true_type or std::false_type,
   * though
   * @param x Variable of the corresponding time step
   * @param phi Matrix to be augmented
   * @param rd Right-hand side to be augmented
   * @param step Time step, 0 <= step < T
   */
  void afs(const std::array<float_t, n> &x, PDMat<float_t, n> &phi,
           std::array<float_t, n> &rd, const size_t step = 0,
           const std::false_type = std::false_type{}) {
    // Evaluate lambda/s
    std::transform(lambda1[step].cbegin(), lambda1[step].cend(),
                   s1[step].cbegin(), d1[step].begin(),
                   std::divides<float_t>());
    std::transform(lambda2[step].cbegin(), lambda2[step].cend(),
                   s2[step].cbegin(), d2[step].begin(),
                   std::divides<float_t>());
    std::transform(lambda3[step].cbegin(), lambda3[step].cend(),
                   s3[step].cbegin(), d3[step].begin(),
                   std::divides<float_t>());

    // if (step == 1) {
    //   std::cout << "\n********\nINNERMOST LOOP\ns1[0]:";
    //   for (auto &elem : s1[1])
    //     std::cout << "\n\t" << elem;
    //   std::cout << "\nlambda1[1]:";
    //   for (auto &elem : lambda1[1])
    //     std::cout << "\n\t" << elem;
    //   std::cout << "\nd1[1]:";
    //   for (auto &elem : d1[step])
    //     std::cout << "\n\t" << elem;
    //   std::cout << "\nd2[1]:";
    //   for (auto &elem : d2[step])
    //     std::cout << "\n\t" << elem;
    // }

    // if (step == 1) {
    //   std::cout << "\n********\nINNERMOST LOOP\nphi (1):";
    //   for (auto &elem : phi.mat)
    //     std::cout << "\n\t" << elem;
    // }

    // Augment phi
    mehrotra::detail::MatOps<float_t>::diagsum(phi, d1[step], d2[step]);

    // if (step == 1) {
    //   std::cout << "\n********\nINNERMOST LOOP\nphi (2):";
    //   for (auto &elem : phi.mat)
    //     std::cout << "\n\t" << elem;
    // }

    mehrotra::detail::MatOps<float_t>::rankkt(d3[step], phi, F);

    // if (step == 1) {
    //   std::cout << "\n********\nINNERMOST LOOP\nphi (3):";
    //   for (auto &elem : phi.mat)
    //     std::cout << "\n\t" << elem;
    // }

    // Evaluate ri1 and ri2 and augment rd
    for (size_t idx = 0; idx < n; idx++) {
      ri1[step][idx] = x[idx] + s1[step][idx] - ub[idx];
      ri2[step][idx] = -x[idx] + s2[step][idx] + lb[idx];
      rd[idx] += lambda1[step][idx] * ri1[step][idx] -
                 lambda2[step][idx] * ri2[step][idx];
    }

    // Evaluate ri3
    std::transform(s3[step].cbegin(), s3[step].cend(), f.cbegin(),
                   ri3[step].begin(), std::minus<float_t>());
    detail::MatOps<float_t>::template matvec<'N'>(float_t{1}, F, x, float_t{1},
                                                  ri3[step]);

    // Augment rd with ri3
    {
      std::array<float_t, k> aux;
      std::transform(d3[step].cbegin(), d3[step].cend(), ri3[step].cbegin(),
                     aux.begin(), std::multiplies<float_t>());
      detail::MatOps<float_t>::template matvec<'T'>(float_t{1}, F, aux,
                                                    float_t{1}, rd);
    }
  }

  /**
   * @brief Contribution of the hard constraints to the affine backwards step
   *
   * Uses x and x_aff to recover lambda_aff and s_aff
   *
   * @param x Variable of the corresponding time step
   * @param x_aff Affine variable of the corresponding time step
   * @param step Time step
   */
  void abs(const std::array<float_t, n> &x, const std::array<float_t, n> &x_aff,
           const size_t step = 0) {
    // Recover lambda1_aff and lambda2_aff
    for (size_t idx = 0; idx < lambda1_aff[step].size(); idx++) {
      lambda1_aff[step][idx] = d1[step][idx] * (x[idx] + x_aff[idx] - ub[idx]);
      lambda2_aff[step][idx] = d2[step][idx] * (-x[idx] - x_aff[idx] + lb[idx]);
    }

    // Recover lambda3_aff
    {
      std::array<float_t, n> aux;
      std::transform(x.cbegin(), x.cend(), x_aff.cbegin(), aux.begin(),
                     std::plus<float_t>());
      lambda3_aff[step] = f;
      detail::MatOps<float_t>::template matvec<'N'>(
          float_t{1}, F, aux, float_t{-1}, lambda3_aff[step]);
    }
    std::transform(d3[step].cbegin(), d3[step].cend(),
                   lambda3_aff[step].cbegin(), lambda3_aff[step].begin(),
                   std::multiplies<float_t>());

    // Recover s_aff
    std::transform(x_aff.cbegin(), x_aff.cend(), ri1[step].cbegin(),
                   s1_aff[step].begin(),
                   [](float_t x_aff, float_t ri) { return -x_aff - ri; });
    std::transform(x_aff.cbegin(), x_aff.cend(), ri2[step].cbegin(),
                   s2_aff[step].begin(),
                   [](float_t x_aff, float_t ri) { return x_aff - ri; });

    s3_aff[step] = ri3[step];
    detail::MatOps<float_t>::template matvec<'N'>(float_t{-1}, F, x_aff,
                                                  float_t{-1}, s3_aff[step]);
  }

  /**
   * @brief Contribution of the hard constraints to the cc forward step
   *
   * Augments rd. The complementary slackness residual divided by s, rs/s, is
   * calculated and stored in ri.
   *
   * @param rd Right-hand side vector to be augmented
   * @param sigmamu Scalar
   * @param step Integer
   */
  void cfs(std::array<float_t, n> &rd, const float_t sigmamu,
           const size_t step = 0) {
    // Evaluate rs/s and store it in ri
    std::transform(s1_aff[step].cbegin(), s1_aff[step].cend(),
                   lambda1_aff[step].cbegin(), ri1[step].begin(),
                   [sigmamu](float_t s, float_t l) { return s * l - sigmamu; });
    std::transform(s2_aff[step].cbegin(), s2_aff[step].cend(),
                   lambda2_aff[step].cbegin(), ri2[step].begin(),
                   [sigmamu](float_t s, float_t l) { return s * l - sigmamu; });

    for (size_t idx = 0; idx < k; idx++) {
      ri3[step][idx] = (s3_aff[step][idx] * lambda3_aff[step][idx] - sigmamu) /
                       s3[step][idx];
    }

    // Create rd
    for (size_t idx = 0; idx < n; idx++) {
      rd[idx] =
          -ri1[step][idx] / s1[step][idx] + ri2[step][idx] / s2[step][idx];
    }

    detail::MatOps<float_t>::template matvec<'T'>(float_t{-1}, F, ri3[step],
                                                  float_t{1}, rd);
  }

  /**
   * @brief Contribution of the hard constraints to the cc backwards step
   *
   * Recovers the variables lambda_cc and s_cc from x_cc. Then, aggregates the
   * affine and cc steps and stores the sum in the affine vector.
   *
   * The variables lambda_cc and s_cc are created in a temporary vector, but
   * they are added immediately to the variables lambda_aff and s_aff.
   *
   * BEWARE: the vector ri does not contain rs, but rs/s
   *
   * @param x_cc CC variable of the corresponding time step
   * @param step Integer
   */
  void cbs(std::array<float_t, n> &x_cc, const size_t step = 0) {
    // Construct lambda1_cc and lambra2_cc
    for (size_t idx = 0; idx < n; idx++) {
      lambda1_aff[step][idx] +=
          d1[step][idx] * (x_cc[idx] - ri1[step][idx] / lambda1[step][idx]);
      lambda2_aff[step][idx] +=
          d2[step][idx] * (-x_cc[idx] - ri2[step][idx] / lambda2[step][idx]);
    }

    // Construct lambda3_cc
    {
      std::array<float_t, k> aux;
      std::transform(ri3[step].cbegin(), ri3[step].cend(), d3[step].cbegin(),
                     aux.begin(), std::divides<float_t>());
      detail::MatOps<float_t>::template matvec<'N'>(float_t{1}, F, x_cc,
                                                    float_t{-1}, aux);
      std::transform(d3[step].cbegin(), d3[step].cend(), aux.cbegin(),
                     aux.begin(), std::multiplies<float_t>());
      // Aggregate cc and aff step
      detail::MatOps<float_t>::vecsum(float_t{1}, aux, lambda3_aff[step]);
    }

    // Construct s_cc by aggregating it to s_aff
    std::transform(s1_aff[step].cbegin(), s1_aff[step].cend(), x_cc.cbegin(),
                   s1_aff[step].begin(), std::minus<float_t>());
    std::transform(s2_aff[step].cbegin(), s2_aff[step].cend(), x_cc.cbegin(),
                   s2_aff[step].begin(), std::plus<float_t>());

    detail::MatOps<float_t>::template matvec<'N'>(float_t{-1}, F, x_cc,
                                                  float_t{1}, s3_aff[step]);
  }

public:
  void initial_value(std::array<float_t, n> &var) {
    // Initialize 'var' to the middle point of the bounds
    std::transform(ub.cbegin(), ub.cend(), lb.cbegin(), var.begin(),
                   [](float_t u, float_t l) { return (u + l) / 2; });
  }

  /**
   * @brief Evaluates the dotproduct lambda's
   *
   * @return float_t Dotproduct lambda's
   */
  float_t duality_gap() const {
    float_t r{0};
    for (size_t idx = 0; idx < T; idx++) {
      r += mehrotra::detail::MatOps<float_t>::dotproduct(s1[idx], lambda1[idx]);
      r += mehrotra::detail::MatOps<float_t>::dotproduct(s2[idx], lambda2[idx]);
      r += mehrotra::detail::MatOps<float_t>::dotproduct(s3[idx], lambda3[idx]);
    }
    return r;
  }

  /**
   * @brief Evaluates the dotproduct lambda's after the affine step is taken
   *
   * Internally, stores s+alpha*s_aff in a temporary, auxiliary variable (same
   * goes for lambda), where alpha is the step size
   *
   * @param alpha Scalar
   * @return float_t Dotproduct lambda's after the affine step is taken
   */
  float_t affine_gap(const float_t alpha) const {
    float_t r{0};
    std::array<float_t, n> l1_aux, s1_aux;
    std::array<float_t, k> l2_aux, s2_aux;
    for (size_t step = 0; step < T; step++) {
      std::transform(s1[step].cbegin(), s1[step].cend(), s1_aff[step].cbegin(),
                     s1_aux.begin(), [alpha](float_t x, float_t x_aff) {
                       return x + alpha * x_aff;
                     });
      std::transform(
          lambda1[step].cbegin(), lambda1[step].cend(),
          lambda1_aff[step].cbegin(), l1_aux.begin(),
          [alpha](float_t x, float_t x_aff) { return x + alpha * x_aff; });
      r += mehrotra::detail::MatOps<float_t>::dotproduct(s1_aux, l1_aux);

      std::transform(s2[step].cbegin(), s2[step].cend(), s2_aff[step].cbegin(),
                     s1_aux.begin(), [alpha](float_t x, float_t x_aff) {
                       return x + alpha * x_aff;
                     });
      std::transform(
          lambda2[step].cbegin(), lambda2[step].cend(),
          lambda2_aff[step].cbegin(), l1_aux.begin(),
          [alpha](float_t x, float_t x_aff) { return x + alpha * x_aff; });
      r += mehrotra::detail::MatOps<float_t>::dotproduct(s1_aux, l1_aux);

      std::transform(s3[step].cbegin(), s3[step].cend(), s3_aff[step].cbegin(),
                     s2_aux.begin(), [alpha](float_t x, float_t x_aff) {
                       return x + alpha * x_aff;
                     });
      std::transform(
          lambda3[step].cbegin(), lambda3[step].cend(),
          lambda3_aff[step].cbegin(), l2_aux.begin(),
          [alpha](float_t x, float_t x_aff) { return x + alpha * x_aff; });
      r += mehrotra::detail::MatOps<float_t>::dotproduct(s2_aux, l2_aux);
    }
    return r;
  }

  /**
   * @brief Maximum stepsize
   *
   * Modifies alpha such that no constraint is violated. Note that alpha is
   * negative, on input and output.
   *
   * @param alpha Scalar
   */
  void linesearch(float_t &alpha) const {
    std::array<float_t, (n > k) ? n : k> aux{};
    for (size_t step = 0; step < T; step++) {
      std::transform(s1[step].cbegin(), s1[step].cend(), s1_aff[step].cbegin(),
                     aux.begin(), std::divides<float_t>());
      for (size_t idx = 0; idx < n; idx++) {
        if (aux[idx] < 0 && aux[idx] > alpha) {
          alpha = aux[idx];
        }
      }

      std::transform(s2[step].cbegin(), s2[step].cend(), s2_aff[step].cbegin(),
                     aux.begin(), std::divides<float_t>());
      for (size_t idx = 0; idx < n; idx++) {
        if (aux[idx] < 0 && aux[idx] > alpha) {
          alpha = aux[idx];
        }
      }

      std::transform(s3[step].cbegin(), s3[step].cend(), s3_aff[step].cbegin(),
                     aux.begin(), std::divides<float_t>());
      for (size_t idx = 0; idx < k; idx++) {
        if (aux[idx] < 0 && aux[idx] > alpha) {
          alpha = aux[idx];
        }
      }

      std::transform(lambda1[step].cbegin(), lambda1[step].cend(),
                     lambda1_aff[step].cbegin(), aux.begin(),
                     std::divides<float_t>());
      for (size_t idx = 0; idx < n; idx++) {
        if (aux[idx] < 0 && aux[idx] > alpha) {
          alpha = aux[idx];
        }
      }

      std::transform(lambda2[step].cbegin(), lambda2[step].cend(),
                     lambda2_aff[step].cbegin(), aux.begin(),
                     std::divides<float_t>());
      for (size_t idx = 0; idx < n; idx++) {
        if (aux[idx] < 0 && aux[idx] > alpha) {
          alpha = aux[idx];
        }
      }

      std::transform(lambda3[step].cbegin(), lambda3[step].cend(),
                     lambda3_aff[step].cbegin(), aux.begin(),
                     std::divides<float_t>());
      for (size_t idx = 0; idx < k; idx++) {
        if (aux[idx] < 0 && aux[idx] > alpha) {
          alpha = aux[idx];
        }
      }
    }
  }

  /**
   * @brief Takes a step in the direction stored in the affine variable, with
   * stepsize alpha
   *
   * @param alpha Scalar
   */
  void take_step(const float_t alpha) {
    for (size_t step = 0; step < T; step++) {
      detail::MatOps<float_t>::vecsum(alpha, s1_aff[step], s1[step]);
      detail::MatOps<float_t>::vecsum(alpha, s2_aff[step], s2[step]);
      detail::MatOps<float_t>::vecsum(alpha, s3_aff[step], s3[step]);
      detail::MatOps<float_t>::vecsum(alpha, lambda1_aff[step], lambda1[step]);
      detail::MatOps<float_t>::vecsum(alpha, lambda2_aff[step], lambda2[step]);
      detail::MatOps<float_t>::vecsum(alpha, lambda3_aff[step], lambda3[step]);
    }

    // std::cout << "\n\ns1[9]";
    // for (auto &elem : s1[9])
    //   std::cout << "\n\t" << elem;
    // std::cout << "\n\ns2[9]";
    // for (auto &elem : s2[9])
    //   std::cout << "\n\t" << elem;
    // std::cout << "\n\ns3[9]";
    // for (auto &elem : s3[9])
    //   std::cout << "\n\t" << elem;

    // std::cout << "\n\nlambda1[9]";
    // for (auto &elem : lambda1[9])
    //   std::cout << "\n\t" << elem;
    // std::cout << "\n\nlambda2[9]";
    // for (auto &elem : lambda2[9])
    //   std::cout << "\n\t" << elem;
    // std::cout << "\n\nlambda3[9]";
    // for (auto &elem : lambda3[9])
    //   std::cout << "\n\t" << elem;
  }

  /**
   * @brief Hotstart within a sequence of QPs
   *
   * Assuming that a new MPC problem has to be solved in the receding horizon
   * scheme, provides some warm-start capability.
   *
   * @param delta Scalar
   */
  void new_x0(const float_t delta) {
    // Rotate
    std::rotate(s1.begin(), s1.begin() + 1, s1.end());
    std::rotate(s2.begin(), s2.begin() + 1, s2.end());
    std::rotate(s3.begin(), s3.begin() + 1, s3.end());
    std::rotate(lambda1.begin(), lambda1.begin() + 1, lambda1.end());
    std::rotate(lambda2.begin(), lambda2.begin() + 1, lambda2.end());
    std::rotate(lambda3.begin(), lambda3.begin() + 1, lambda3.end());
    // Complete the tail
    s1[T - 1] = s1[T - 2];
    s2[T - 1] = s2[T - 2];
    s3[T - 1] = s3[T - 2];
    lambda1[T - 1] = lambda1[T - 2];
    lambda2[T - 1] = lambda2[T - 2];
    lambda3[T - 1] = lambda3[T - 2];
    // Add some positive constant
    for (size_t step = 0; step < T; step++) {
      std::transform(s1.cbegin(), s1.cend(), s1.begin(),
                     [delta](float_t val) { return val + delta; });
      std::transform(s2.cbegin(), s2.cend(), s2.begin(),
                     [delta](float_t val) { return val + delta; });
      std::transform(s3.cbegin(), s3.cend(), s3.begin(),
                     [delta](float_t val) { return val + delta; });
      std::transform(lambda1.cbegin(), lambda1.cend(), lambda1.begin(),
                     [delta](float_t val) { return val + delta; });
      std::transform(lambda2.cbegin(), lambda2.cend(), lambda2.begin(),
                     [delta](float_t val) { return val + delta; });
      std::transform(lambda3.cbegin(), lambda3.cend(), lambda3.begin(),
                     [delta](float_t val) { return val + delta; });
    }
  }
};

} // namespace hardcons
} // namespace mehrotra

#endif