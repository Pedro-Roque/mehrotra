#ifndef MATOPS_HPP_
#define MATOPS_HPP_

#include <cmath>

#include "matrix/matrix.hpp"

namespace mehrotra {
namespace detail {
template <class float_t> struct MatOps {

  /**
   * @brief Computes the vector addition y <- a*x + y
   *
   * @tparam n Vector size
   * @param a Scalar
   * @param x (Const) vector of size n
   * @param y Vector of size n
   */
  template <size_t n>
  static void vecsum(const float_t a, const std::array<float_t, n> &x,
                     std::array<float_t, n> &y) {
    if (a == float_t{1}) {
      for (size_t idx = 0; idx < n; idx++) {
        y[idx] += x[idx];
      }
    } else if (a == float_t{-1}) {
      for (size_t idx = 0; idx < n; idx++) {
        y[idx] -= x[idx];
      }
    } else {
      for (size_t idx = 0; idx < n; idx++) {
        y[idx] += a * x[idx];
      }
    }
  }

  /**
   * @brief Computes the dot product of two vectors
   *
   * Use dotproduct(x,x) to evaluate the square of the euclidean norm of a
   * vector.
   *
   * @tparam n Vector dimension
   * @param x First vector
   * @param y Second vector
   * @return float_t Dot product of input vectors
   */
  template <size_t n>
  static float_t dotproduct(const std::array<float_t, n> &x,
                            const std::array<float_t, n> &y) {
    float_t r{0};
    for (size_t idx = 0; idx < n; idx++) {
      r += x[idx] * y[idx];
    }
    return r;
  }

  /**
   * @brief In place calculation of the cholesky factorization of a positive
   * definite matrix
   *
   * x <- cholfact(x)
   *
   * @tparam n Matrix dimension
   * @param x Positive definite matrix in packed format
   */
  template <size_t n> static void cholfact(mehrotra::PDMat<float_t, n> &A) {
    size_t skip{0};
    for (size_t col = 0; col < n; col++) {
      A.mat[skip] = std::sqrt(A.mat[skip]);
      if (col + 1 < n) {
        for (size_t row = 1; row < n - col; row++) {
          A.mat[skip + row] = A.mat[skip + row] / A.mat[skip];
        }
        rank1(n - col - 1, float_t{-1}, A.mat.data() + skip + n - col,
              A.mat.data() + skip + 1, size_t{1});
        skip += n - col;
      }
    }
  }

  /**
   * @brief In place inversion of a diagonal matrix.
   *
   * Note that, despite the name, this function DOES NOT return the cholesky
   * factor of the supplied matrix. Rather than this, it return the square of
   * the Choleky factor.
   *
   * x <- cholfact(x)
   *
   * @tparam n Matrix dimension
   * @param x Positive definite matrix in packed format
   */
  template <size_t n> static void cholfact(mehrotra::DiagMat<float_t, n> &x) {
    for (size_t idx = 0; idx < n; idx++) {
      x.mat[idx] = 1 / x.mat[idx];
    }
  }

  /**
   * @brief In place cholesky factor rank-1 update
   *
   * Given a factorization A = L*L', and a rank-1 update A2 = A + x*x',
   * computes the factorization A2 = L2*L2' without explicitely computing A2
   *
   * @tparam n Matrix dimension
   * @param L Cholesky factor (lower triangular matrix in packed format)
   * @param x Rank-1 update vector
   */
  template <size_t n>
  static void cholupdate(mehrotra::PDMat<float_t, n> &L,
                         std::array<float_t, n> x) {
    size_t skip{0};
    float_t r{0}, c{0}, s{0};
    float_t temp1{0}, temp2{0};
    for (size_t col = 0; col < n; col++) {
      givens_rotation(L.mat[skip], x[col], c, s, r);
      L.mat[skip] = r;
      for (size_t row = col + 1; row < n; row++) {
        temp1 = L.mat[skip + row - col];
        temp2 = x[row];
        L.mat[skip + row - col] = c * temp1 + s * temp2;
        x[row] = -s * temp1 + c * temp2;
      }
      skip += n - col;
    }
  }

  template <size_t n>
  static void cholupdate2(mehrotra::PDMat<float_t, n> &L,
                          std::array<float_t, n> x) {
    cholupdate(L, x);
  }

  /**
   * @brief Inversion of a positive definite matrix
   *
   * On input, L does not contain the positive definite matrix, but its cholesky
   * factorization. On output, it contains the inverse of the original positive
   * definite matrix.
   *
   * L <- inverse(L*L')
   *
   * @tparam n Matrix dimension
   * @param L PDMat that stores the cholesky factorization
   */
  template <size_t n> static void invert(PDMat<float_t, n> &L) {
    invert_triangular(L);
    size_t pivot{0};
    for (size_t col = 0; col < n; col++) {
      size_t next_pivot = pivot + n - col;
      L.mat[pivot] = L.mat[pivot] * L.mat[pivot];
      for (size_t row = 1; row < n - col; row++) {
        L.mat[pivot] += (L.mat[pivot + row] * L.mat[pivot + row]);
      }
      if (col < n - 1) {
        trimatvec_transpose(n - col - 1, L.mat.data() + next_pivot,
                            L.mat.data() + pivot + 1);
      }
      pivot = next_pivot;
    }
  }

  /**
   * @brief Matrix-vector product for a positive definite matrix
   *
   * Computes y <- alpha*A*x + beta*y
   *
   * @tparam n Vector size
   * @param alpha Scalar
   * @param A Positive definite matrix in packed form (lower triangular)
   * @param x Vector of size n
   * @param beta Scalar
   * @param y Vector of size n
   */
  template <size_t n>
  static void matvec(const float_t alpha, const mehrotra::PDMat<float_t, n> &A,
                     const std::array<float_t, n> &x, const float_t beta,
                     std::array<float_t, n> &y) {
    // Assign y <- beta*y
    if (beta != float_t{1}) {
      for (size_t idx = 0; idx < n; idx++) {
        y[idx] *= beta;
      }
    }
    // Do the product
    size_t kk{0}, k;
    if (alpha == float_t{1}) {
      float_t temp;
      for (size_t j = 0; j < n; j++) {
        temp = 0;
        y[j] += x[j] * A.mat[kk];
        k = kk + 1;
        for (size_t i = j + 1; i < n; i++) {
          y[i] += x[j] * A.mat[k];
          temp += A.mat[k] * x[i];
          k++;
        }
        y[j] += alpha * temp;
        kk += n - j;
      }
    } else {
      float_t temp1, temp2;
      for (size_t j = 0; j < n; j++) {
        temp1 = alpha * x[j];
        temp2 = 0;
        y[j] += temp1 * A.mat[kk];
        k = kk + 1;
        for (size_t i = j + 1; i < n; i++) {
          y[i] += temp1 * A.mat[k];
          temp2 += A.mat[k] * x[i];
          k++;
        }
        y[j] += alpha * temp2;
        kk += n - j;
      }
    }
  }

  /**
   * @brief Matrix-vector product for a general rectangular matrix
   *
   * Computes y <- alpha*A*x + beta*y
   *
   * @tparam m Number of rows
   * @tparam n Number of columns
   * @param trans Use 'T' for using the transpose of matrix A. Use 'N' otherwise
   * @param alpha Scalar
   * @param A Rectangular matrix of dimension (m x n)
   * @param x Vector of size n
   * @param beta Scalar
   * @param y Vector of size n
   */
  template <char t, size_t m, size_t n>
  static void
  matvec(const float_t alpha, const mehrotra::GeMat<float_t, m, n> &A,
         const std::array<float_t, (t == 'N') ? n : m> &x, const float_t beta,
         std::array<float_t, (t == 'N') ? m : n> &y) {
    matvec(alpha, A, x, beta, y,
           typename std::conditional<t == 'N', std::true_type,
                                     std::false_type>::type{});
  }

  /**
   * @brief Matrix-vector product for a diagonal matrix
   *
   * Computes y <- alpha*A*x + beta*y
   *
   * @tparam n Matrix dimension
   * @param alpha Scalar
   * @param A Diagonal matrix of dimension n
   * @param x Vector of size n
   * @param beta Scalar
   * @param y Vector of dimension n
   */
  template <size_t n>
  static void matvec(const float_t alpha,
                     const mehrotra::DiagMat<float_t, n> &A,
                     const std::array<float_t, n> &x, const float_t beta,
                     std::array<float_t, n> &y) {
    for (size_t idx = 0; idx < n; idx++) {
      y[idx] = alpha * A.mat[idx] * x[idx] + beta * y[idx];
    }
  }

  /**
   * @brief Computes the solution to the system Lx=b or L'x=b, where L is lower
   * triangular and b is a vector
   *
   * Computes x<-L\b or x<-L'\b
   *
   * @tparam n Matrix dimension (number of equations)
   * @param trans Use 'T' for using the transpose of matrix L. Use 'N' otherwise
   * @param L Lower-triangular matrix in packed format
   * @param b Vector of size n (right hand side)
   */
  template <size_t n>
  static void backsolve(const char trans, const mehrotra::PDMat<float_t, n> &L,
                        std::array<float_t, n> &b) {
    if (trans == 'N') {
      size_t kk{0}, jx{0};
      float_t temp;
      for (size_t j = 0; j < n; j++) {
        b[jx] = b[jx] / L.mat[kk];
        temp = b[jx];
        size_t ix = jx;
        for (size_t k = kk + 1; k < kk + n - j; k++) {
          ix++;
          b[ix] = b[ix] - temp * L.mat[k];
        }
        jx++;
        kk += n - j;
      }
    } else {
      size_t kk{n * (n + 1) / 2 - 1};
      float_t temp;
      for (size_t j = 0; j < n; j++) {
        temp = b[n - j - 1];
        size_t k{kk};
        for (size_t i = 0; i < j; i++) {
          temp = temp - L.mat[k] * b[n - i - 1];
          k--;
        }
        temp = temp / L.mat[kk - j];
        b[n - j - 1] = temp;
        kk = kk - j - 1;
      }
    }
  }

  /**
   * @brief Computes the solution to the system LX=B or L'X=B for a multiple
   * right-hand side, where L is lwoer triangular and B is a matrix
   *
   * Computes X<-L\B of x<-L'\B
   *
   * @tparam n Matrix dimension (number of equations)
   * @tparam m Number of right-hand sides
   * @param trans Use 'T' for using the transpose of matrix L. Use 'N' otherwise
   * @param L Lower-triangular matrix in packed format
   * @param B Rectangular matrix of dimensions (n x m)
   */
  template <size_t n, size_t m>
  static void backsolve(const char trans, const mehrotra::PDMat<float_t, n> &L,
                        mehrotra::GeMat<float_t, n, m> &B) {
    std::array<float_t, n> aux;
    for (size_t nrhs = 0; nrhs < m; nrhs++) {
      std::copy(B.mat.cbegin() + nrhs * n, B.mat.cbegin() + (nrhs + 1) * n,
                aux.begin());
      backsolve(trans, L, aux);
      std::copy(aux.cbegin(), aux.cend(), B.mat.begin() + nrhs * n);
    }
  }

  /**
   * @brief Add one vector to the diagonal of a positive definite matrix
   *
   * Computes A <- A + I*x, where I is the identity matrix
   *
   * @tparam n Matrix dimension
   * @param A Positive definite matrix in packed format (lower triangular)
   * @param x Vector of size n
   */
  template <size_t n>
  static void diagsum(mehrotra::PDMat<float_t, n> &A,
                      const std::array<float_t, n> &x) {
    size_t skip = 0;
    for (size_t idx = 0; idx < n; idx++) {
      A.mat[skip] += x[idx];
      skip += n - idx;
    }
  }

  /**
   * @brief Add two vectors to the diagonal of a positive definite matrix
   *
   * Computes A<-A+I*(x+y)
   *
   * @tparam n Matrix dimension
   * @param A Positive definite matrix in packed format (lower triangular)
   * @param x Vector of size n
   * @param y Vector of size n
   */
  template <size_t n>
  static void diagsum(mehrotra::PDMat<float_t, n> &A,
                      const std::array<float_t, n> &x,
                      const std::array<float_t, n> &y) {
    size_t skip = 0;
    for (size_t idx = 0; idx < n; idx++) {
      A.mat[skip] += x[idx] + y[idx];
      skip += n - idx;
    }
  }

  /**
   * @brief Add one vector to the diagonal of a diagonal matrix
   *
   * Computes A<-A + I*x, where I is the identity matrix
   *
   * @tparam n Matrix dimension
   * @param A Diagonal matrix of dimension n
   * @param x Vector of size n
   */
  template <size_t n>
  static void diagsum(mehrotra::DiagMat<float_t, n> &A,
                      const std::array<float_t, n> &x) {
    for (size_t idx = 0; idx < n; idx++) {
      A.mat[idx] += x[idx];
    }
  }

  /**
   * @brief Add two vectors to the diagonal of a diagonal matrix
   *
   * Computes A<-A+I*(x+y)
   *
   * @tparam n Matrix dimension
   * @param A Diagonal matrix of dimension n
   * @param x Vector of size n
   * @param y Vector of size n
   */
  template <size_t n>
  static void diagsum(mehrotra::DiagMat<float_t, n> &A,
                      const std::array<float_t, n> &x,
                      const std::array<float_t, n> &y) {
    for (size_t idx = 0; idx < n; idx++) {
      A.mat[idx] += x[idx] + y[idx];
    }
  }

  /**
   * @brief Rank-1 update of a positive definite matrix
   *
   * Computes A<-A + alpha*x*x'
   *
   * @tparam n Matrix dimension
   * @param alpha Scalar
   * @param x Vector of size n
   * @param A Positive definite matrix in packed format (lower triangular)
   */
  template <size_t n>
  static void rank1(const float_t alpha, mehrotra::PDMat<float_t, n> &A,
                    const std::array<float_t, n> &x) {
    size_t kk{0};
    if (alpha == float_t{1}) {
      for (size_t j = 0; j < n; j++) {
        size_t k{kk};
        for (size_t i = j; i < n; i++) {
          A.mat[k] += x[i] * x[j];
          k++;
        }
        kk += n - j;
      }
    } else {
      float_t temp;
      for (size_t j = 0; j < n; j++) {
        temp = alpha * x[j];
        size_t k{kk};
        for (size_t i = j; i < n; i++) {
          A.mat[k] += x[i] * temp;
          k++;
        }
        kk += n - j;
      }
    }
  }

  /**
   * @brief Rank-k update of a positive definite matrix
   *
   * Compute A<-A + alpha*X*X'
   *
   * @tparam n Matrix dimension
   * @tparam m Rank of the update matrix X*X'
   * @param alpha Scalar
   * @param X Rectangular matrix of dimension (n x m)
   * @param A Positive definite matrix in packed format (lower triangular)
   */
  template <size_t n, size_t m>
  static void rankk(const float_t alpha, mehrotra::PDMat<float_t, n> &A,
                    const mehrotra::GeMat<float_t, n, m> &X) {
    for (size_t idx = 0; idx < m; idx++) {
      rank1(n, alpha, A.mat.data(), X.mat.data() + idx * n, size_t{1});
    }
  }

  /**
   * @brief Rank-k update of a positive definite matrix
   *
   * Compute A<-A + alpha*X*X', where alpha is a vector of coefficients
   *
   * @tparam n Matrix dimension
   * @tparam m Rank of the update matrix X*X'
   * @param alpha Vector
   * @param X Rectangular matrix of dimension (n x m)
   * @param A Positive definite matrix in packed format (lower triangular)
   */
  template <size_t n, size_t m>
  static void rankk(const std::array<float_t, m> &alpha,
                    mehrotra::PDMat<float_t, n> &A,
                    const mehrotra::GeMat<float_t, n, m> &X) {
    for (size_t idx = 0; idx < m; idx++) {
      rank1(n, alpha[idx], A.mat.data(), X.mat.data() + idx * n, size_t{1});
    }
  }

  /**
   * @brief Rank-k update of a positive definite matrix
   *
   * Compute A<-A + alpha*X'*X
   *
   * @tparam n Matrix dimension
   * @tparam m Rank of the update matrix X'*X
   * @param alpha Scalar
   * @param X Rectangular matrix of dimension (m x n)
   * @param A Positive definite matrix in packed format (lower triangular)
   */
  template <size_t n, size_t m>
  static void rankkt(const float_t alpha, mehrotra::PDMat<float_t, n> &A,
                     const mehrotra::GeMat<float_t, m, n> &X) {
    for (size_t idx = 0; idx < m; idx++) {
      rank1(n, alpha, A.mat.data(), X.mat.data() + idx, m);
    }
  }

  /**
   * @brief Rank-k update of a positive definite matrix
   *
   * Compute A<-A + alpha*X'*X, where alpha is a vector of coefficients
   *
   * @tparam n Matrix dimension
   * @tparam m Rank of the update matrix X'*X
   * @param alpha Scalar
   * @param X Rectangular matrix of dimension (m x n)
   * @param A Positive definite matrix in packed format (lower triangular)
   */
  template <size_t n, size_t m>
  static void rankkt(const std::array<float_t, m> &alpha,
                     mehrotra::PDMat<float_t, n> &A,
                     const mehrotra::GeMat<float_t, m, n> &X) {
    for (size_t idx = 0; idx < m; idx++) {
      rank1(n, alpha[idx], A.mat.data(), X.mat.data() + idx, m);
    }
  }

  /**
   * @brief Addition of PDMat and PDMat
   *
   * Performs the operation A <- A+B, where A and B are positive definite
   * matrices in packed form
   *
   * @tparam n Matrix dimension
   * @param A Positive definite, packed matrix
   * @param B Positive definite, packed matrix
   */
  template <size_t n>
  static void matsum(PDMat<float_t, n> &A, const PDMat<float_t, n> &B) {
    for (size_t idx = 0; idx < n * (n + 1) / 2; idx++) {
      A.mat[idx] += B.mat[idx];
    }
  }

  /**
   * @brief Addition of PDMat and DiagMat
   *
   * Performs the operation A <- A+B, where A is positive definite (in packed
   * form) and B is diagonal
   *
   * @tparam n Matrix dimension
   * @param A Positive definite, packed matrix
   * @param B Positive definite, packed matrix
   */
  template <size_t n>
  static void matsum(PDMat<float_t, n> &A, const DiagMat<float_t, n> &B) {
    size_t skip{0};
    for (size_t idx = 0; idx < n; idx++) {
      A.mat[skip] += B.mat[idx];
      skip += n - idx;
    }
  }

private:
  template <size_t m, size_t n>
  static void matvec(const float_t alpha,
                     const mehrotra::GeMat<float_t, m, n> &A,
                     const std::array<float_t, n> &x, const float_t beta,
                     std::array<float_t, m> &y, const std::true_type) {
    // Assign y <- beta*y
    if (beta != float_t{1}) {
      for (size_t idx = 0; idx < m; idx++) {
        y[idx] *= beta;
      }
    }
    // Do the product
    size_t jx{0};
    if (alpha == float_t{1}) {
      for (size_t j = 0; j < n; j++) {
        for (size_t i = 0; i < m; i++) {
          y[i] += x[jx] * A.mat[i + m * j];
        }
        jx++;
      }
    } else {
      float_t temp;
      for (size_t j = 0; j < n; j++) {
        temp = alpha * x[jx];
        for (size_t i = 0; i < m; i++) {
          y[i] += temp * A.mat[i + m * j];
        }
        jx++;
      }
    }
  }

  template <size_t m, size_t n>
  static void matvec(const float_t alpha,
                     const mehrotra::GeMat<float_t, m, n> &A,
                     const std::array<float_t, m> &x, const float_t beta,
                     std::array<float_t, n> &y, const std::false_type) {
    // Assign y <- beta*y
    if (beta != float_t{1}) {
      for (size_t idx = 0; idx < n; idx++) {
        y[idx] *= beta;
      }
    }
    // Do the product
    size_t jy{0};
    float_t temp;
    if (alpha == float_t{1}) {
      for (size_t j = 0; j < n; j++) {
        temp = float_t{0};
        for (size_t i = 0; i < m; i++) {
          temp += A.mat[i + m * j] * x[i];
        }
        y[jy] += temp;
        jy++;
      }
    } else {
      for (size_t j = 0; j < n; j++) {
        temp = float_t{0};
        for (size_t i = 0; i < m; i++) {
          temp += A.mat[i + m * j] * x[i];
        }
        y[jy] += alpha * temp;
        jy++;
      }
    }
  }

  static void rank1(const size_t n, const float_t alpha, float_t *A,
                    const float_t *x, const size_t incx) {
    size_t kk{0}, jx{0};
    if (alpha == float_t{1}) {
      for (size_t j = 0; j < n; j++) {
        size_t ix{jx};
        for (size_t k = kk; k < kk + n - j; k++) {
          A[k] += x[ix] * x[jx];
          ix += incx;
        }
        jx += incx;
        kk += n - j;
      }
    } else {
      float_t temp;
      for (size_t j = 0; j < n; j++) {
        temp = alpha * x[jx];
        size_t ix{jx};
        for (size_t k = kk; k < kk + n - j; k++) {
          A[k] += x[ix] * temp;
          ix += incx;
        }
        jx += incx;
        kk += n - j;
      }
    }
  }

  static void trimatvec(const size_t n, const float_t *A, float_t *x) {
    size_t kk{n * (n + 1) / 2 - 1};
    float_t temp;
    for (size_t row = n; row > 0; row--) {
      temp = x[row - 1];
      size_t k{kk};
      for (size_t col = n; col >= row + 1; col--) {
        x[col - 1] += temp * A[k];
        k--;
      }
      x[row - 1] *= A[kk - n + row];
      kk = kk - (n - row + 1);
    }
  }

  static void trimatvec_transpose(const size_t n, const float_t *A,
                                  float_t *x) {
    size_t kk{0};
    float_t temp;
    for (size_t row = 0; row < n; row++) {
      temp = x[row] * A[kk];
      size_t k{kk + 1};
      for (size_t col = row + 1; col < n; col++) {
        temp += A[k] * x[col];
        k++;
      }
      x[row] = temp;
      kk += n - row;
    }
  }

  template <size_t n> static void invert_triangular(PDMat<float_t, n> &A) {
    size_t skip{n * (n + 1) / 2 - 1}, clast{0};
    float_t temp;
    for (size_t row = n; row >= 1; row--) {
      A.mat[skip] = 1 / A.mat[skip];
      if (row < n) {
        temp = -A.mat[skip];
        trimatvec(n - row, A.mat.data() + clast, A.mat.data() + skip + 1);
        for (size_t idx = 0; idx < n - row; idx++) {
          A.mat[skip + 1 + idx] *= temp;
        }
      }
      clast = skip;
      skip = skip - n + row - 2;
    }
  }

  static void givens_rotation(const float_t x, const float_t y, float_t &c,
                              float_t &s, float_t &r) {
    float_t scale = std::abs(x) + std::abs(y);
    if (scale == 0) {
      c = float_t{1};
      s = float_t{0};
      r = float_t{0};
    } else {
      r = std::sqrt(x * x + y * y);
      if (y < -x) {
        c = -x / r;
        s = -y / r;
      }
      c = x / r;
      s = y / r;
    }
  }

}; // namespace detail

template <class float_t, size_t n, size_t m>
void transpose(mehrotra::GeMat<float_t, n, m> &A) {
  std::array<float_t, n * m> aux{A.mat};
  for (size_t row = 0; row < n; row++)   // Rows in transposed matrix
    for (size_t col = 0; col < m; col++) // Columns in transposed matrix
      A.mat[col + row * m] = aux[row + col * n];
}

} // namespace detail
} // namespace mehrotra

#endif