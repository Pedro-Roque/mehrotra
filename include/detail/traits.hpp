#ifndef MPC_TRAITS_HPP
#define MPC_TRAITS_HPP

#include "constraints/hardcons.hpp"
#include "constraints/softcons.hpp"
#include "matrix/matrix.hpp"

namespace mehrotra {
namespace detail {

// Matrix traits
template <template <class, size_t> class> struct is_pdmat {
  static constexpr bool value = false;
};

template <> struct is_pdmat<mehrotra::PDMat> {
  static constexpr bool value = true;
};

// Existance of constraints: does it have bounds?
template <template <class, class, size_t, size_t, size_t> class>
struct has_bounds {
  static constexpr bool value = false;
};

template <> struct has_bounds<hardcons::simplebounds> {
  static constexpr bool value = true;
};

template <> struct has_bounds<softcons::simplebounds> {
  static constexpr bool value = true;
};

template <> struct has_bounds<hardcons::fullbounds> {
  static constexpr bool value = true;
};

template <> struct has_bounds<softcons::fullbounds> {
  static constexpr bool value = true;
};

// Existance of constraints: does it have linear constraints?
template <template <class, class, size_t, size_t, size_t> class>
struct has_linear {
  static constexpr bool value = false;
};

template <> struct has_linear<hardcons::linearbounds> {
  static constexpr bool value = true;
};

template <> struct has_linear<softcons::linearbounds> {
  static constexpr bool value = true;
};

template <> struct has_linear<hardcons::fullbounds> {
  static constexpr bool value = true;
};

template <> struct has_linear<softcons::fullbounds> {
  static constexpr bool value = true;
};

// Custom boolean types
struct nocons {};
struct linear {};
struct normal {};

// More custom boolean types
struct input {};
struct state {};
struct terminal {};

} // namespace detail
} // namespace mpc

#endif