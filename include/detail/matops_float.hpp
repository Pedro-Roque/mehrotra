#ifndef MATOPS_SINGLE_HPP_
#define MATOPS_SINGLE_HPP_

#include <algorithm>
#include <array>

#include "detail/matops.hpp"
#include "matrix/matrix.hpp"

extern "C" {
// Vector sum
void saxpy_(const int *, const float *, const float *, const int *, float *,
            const int *);
// Dot product
float sdot_(const int *, const float *, const int *, const float *,
            const int *);
// Positive definite matrix factorization and inversion
void spptrf_(const char *, const int *, float *, int *);
void spptri_(const char *, const int *, float *, int *);
// Matrix-vector product for symmetric, packed matrix
void sspmv_(const char *, const int *, const float *, const float *,
            const float *, const int *, const float *, float *, const int *);
// Matrix-vector product for general matrix
void sgemv_(const char *, const int *, const int *, const float *,
            const float *, const int *, const float *, const int *,
            const float *, float *, const int *);
// Backsolve for lower triangular coefficients matrix
void stptrs_(const char *, const char *, const char *, const int *, const int *,
             const float *, float *, const int *, const int *);
// Get the coefficients for a Givens rotation
void slartg_(const float *, const float *, float *, float *, float *);
// Execute Givens rotation
void srot_(const int *, float *, const int *, float *, const int *,
           const float *, const float *);
// Rank one update
void sspr_(const char *, const int *, const float *, const float *, const int *,
           float *);
}

namespace mehrotra {
namespace detail {

template <> struct MatOps<float> {

  /**
   * @brief Computes the vector addition y <- a*x + y
   *
   * @tparam n Vector size
   * @param a Scalar
   * @param x (Const) vector of size n
   * @param y Vector of size n
   */
  template <size_t n>
  static void vecsum(const float a, const std::array<float, n> &x,
                     std::array<float, n> &y) {
    constexpr int N{n}, inc{1};
    saxpy_(&N, &a, x.data(), &inc, y.data(), &inc);
  }

  /**
   * @brief Computes the dot product of two vectors
   *
   * Use dotproduct(x,x) to evaluate the square of the euclidean norm of a
   * vector.
   *
   * @tparam n Vector dimension
   * @param x First vector
   * @param y Second vector
   * @return float Dot product of input vectors
   */
  template <size_t n>
  static float dotproduct(const std::array<float, n> &x,
                          const std::array<float, n> &y) {
    constexpr int N{n}, inc{1};
    return sdot_(&N, x.data(), &inc, y.data(), &inc);
  }

  /**
   * @brief In place calculation of the cholesky factorization of a positive
   * definite matrix
   *
   * x <- cholfact(x)
   *
   * @tparam n Matrix dimension
   * @param x Positive definite matrix in packed format
   */
  template <size_t n> static void cholfact(mehrotra::PDMat<float, n> &x) {
    constexpr int N{n};
    constexpr char uplo{'L'};
    int info{0};
    spptrf_(&uplo, &N, x.mat.data(), &info);
  }

  /**
   * @brief In place inversion of a diagonal matrix.
   *
   * Note that, despite the name, this function DOES NOT return the cholesky
   * factor of the supplied matrix. Rather than this, it return the square of
   * the Choleky factor.
   *
   * x <- cholfact(x)
   *
   * @tparam n Matrix dimension
   * @param x Positive definite matrix in packed format
   */
  template <size_t n> static void cholfact(mehrotra::DiagMat<float, n> &x) {
    std::transform(x.mat.cbegin(), x.mat.cend(), x.mat.begin(),
                   [](float x) { return 1 / x; });
  }

  /**
   * @brief In place cholesky factor rank-1 update
   *
   * Given a factorization A = L*L', and a rank-1 update A2 = A + x*x',
   * computes the factorization A2 = L2*L2' without explicitely computing A2
   *
   * @tparam n Matrix dimension
   * @param L Cholesky factor (lower triangular matrix in packed format)
   * @param x Rank-1 update vector
   */
  template <size_t n>
  static void cholupdate(mehrotra::PDMat<float, n> &L, std::array<float, n> x) {
    constexpr int inc{1};
    float r{0}, c{0}, s{0};
    int skip{0}, N{n - 1};
    for (size_t col = 0; col < n; col++) {
      slartg_(&L.mat[skip], &x[col], &c, &s, &r);
      L.mat[skip] = r;
      srot_(&N, &L.mat[skip + 1], &inc, &x[col + 1], &inc, &c, &s);
      N--;
      skip += n - col;
    }
  }

  template <size_t n>
  static void cholupdate2(mehrotra::PDMat<float, n> &L,
                          std::array<float, n> x) {
    float r{0}, c{0}, s{0};
    float temp1{0}, temp2{0};
    int skip{0};
    for (size_t col = 0; col < n; col++) {
      slartg_(&L.mat[skip], &x[col], &c, &s, &r);
      L.mat[skip] = r;
      for (size_t row = col + 1; row < n; row++) {
        temp1 = L.mat[skip + row - col];
        temp2 = x[row];
        L.mat[skip + row - col] = c * temp1 + s * temp2;
        x[row] = -s * temp1 + c * temp2;
      }
      skip += n - col;
    }
  }

  /**
   * @brief Inversion of a positive definite matrix
   *
   * On input, L does not contain the positive definite matrix, but its cholesky
   * factorization. On output, it contains the inverse of the original positive
   * definite matrix.
   *
   * L <- inverse(L*L')
   *
   * @tparam n Matrix dimension
   * @param L PDMat that stores the cholesky factorization
   */
  template <size_t n> static void invert(PDMat<float, n> &L) {
    int info{0};
    constexpr char uplo{'L'};
    constexpr int N{n};
    spptri_(&uplo, &N, L.mat.data(), &info);
  }

  /**
   * @brief Matrix-vector product for a positive definite matrix
   *
   * Computes y <- alpha*A*x + beta*y
   *
   * @tparam n Vector size
   * @param alpha Scalar
   * @param A Positive definite matrix in packed form (lower triangular)
   * @param x Vector of size n
   * @param beta Scalar
   * @param y Vector of size n
   */
  template <size_t n>
  static void matvec(const float alpha, const mehrotra::PDMat<float, n> &A,
                     const std::array<float, n> &x, const float beta,
                     std::array<float, n> &y) {
    constexpr char uplo{'L'};
    constexpr int N{n}, inc{1};
    sspmv_(&uplo, &N, &alpha, A.mat.data(), x.data(), &inc, &beta, y.data(),
           &inc);
  }

  /**
   * @brief Matrix-vector product for a general rectangular matrix
   *
   * Computes y <- alpha*A*x + beta*y
   *
   * @tparam m Number of rows
   * @tparam n Number of columns
   * @param trans Use 'T' for using the transpose of matrix A. Use 'N' otherwise
   * @param alpha Scalar
   * @param A Rectangular matrix of dimension (m x n)
   * @param x Vector of size n
   * @param beta Scalar
   * @param y Vector of size n
   */
  template <char t, size_t m, size_t n>
  static void matvec(const float alpha, const mehrotra::GeMat<float, m, n> &A,
                     const std::array<float, (t == 'N') ? n : m> &x,
                     const float beta,
                     std::array<float, (t == 'N') ? m : n> &y) {
    constexpr int M{m}, N{n}, lda{m}, inc{1};
    constexpr char trans{t};
    sgemv_(&trans, &M, &N, &alpha, A.mat.data(), &lda, x.data(), &inc, &beta,
           y.data(), &inc);
  }

  /**
   * @brief Matrix-vector product for a diagonal matrix
   *
   * Computes y <- alpha*A*x + beta*y
   *
   * @tparam n Matrix dimension
   * @param alpha Scalar
   * @param A Diagonal matrix of dimension n
   * @param x Vector of size n
   * @param beta Scalar
   * @param y Vector of dimension n
   */
  template <size_t n>
  static void matvec(const float alpha, const mehrotra::DiagMat<float, n> &A,
                     const std::array<float, n> &x, const float beta,
                     std::array<float, n> &y) {
    for (size_t idx = 0; idx < n; idx++) {
      y[idx] = alpha * A.mat[idx] * x[idx] + beta * y[idx];
    }
  }

  /**
   * @brief Computes the solution to the system Lx=b or L'x=b, where L is lower
   * triangular and b is a vector
   *
   * Computes x<-L\b or x<-L'\b
   *
   * @tparam n Matrix dimension (number of equations)
   * @param trans Use 'T' for using the transpose of matrix L. Use 'N' otherwise
   * @param L Lower-triangular matrix in packed format
   * @param b Vector of size n (right hand side)
   */
  template <size_t n>
  static void backsolve(const char trans, const mehrotra::PDMat<float, n> &L,
                        std::array<float, n> &b) {
    constexpr char uplo{'L'}, diag{'N'};
    constexpr int N{n}, nrhs{1};
    int info{0};
    stptrs_(&uplo, &trans, &diag, &N, &nrhs, L.mat.data(), b.data(), &N, &info);
  }

  /**
   * @brief Computes the solution to the system LX=B or L'X=B for a multiple
   * right-hand side, where L is lwoer triangular and B is a matrix
   *
   * Computes X<-L\B of x<-L'\B
   *
   * @tparam n Matrix dimension (number of equations)
   * @tparam m Number of right-hand sides
   * @param trans Use 'T' for using the transpose of matrix L. Use 'N' otherwise
   * @param L Lower-triangular matrix in packed format
   * @param B Rectangular matrix of dimensions (n x m)
   */
  template <size_t n, size_t m>
  static void backsolve(const char trans, const mehrotra::PDMat<float, n> &L,
                        mehrotra::GeMat<float, n, m> &B) {
    constexpr char uplo{'L'}, diag{'N'};
    constexpr int N{n}, nrhs{m};
    int info{0};
    stptrs_(&uplo, &trans, &diag, &N, &nrhs, L.mat.data(), B.mat.data(), &N,
            &info);
  }

  /**
   * @brief Add one vector to the diagonal of a positive definite matrix
   *
   * Computes A <- A + I*x, where I is the identity matrix
   *
   * @tparam n Matrix dimension
   * @param A Positive definite matrix in packed format (lower triangular)
   * @param x Vector of size n
   */
  template <size_t n>
  static void diagsum(mehrotra::PDMat<float, n> &A,
                      const std::array<float, n> &x) {
    size_t skip = 0;
    for (size_t idx = 0; idx < n; idx++) {
      A.mat[skip] += x[idx];
      skip += n - idx;
    }
  }

  /**
   * @brief Add two vectors to the diagonal of a positive definite matrix
   *
   * Computes A<-A+I*(x+y)
   *
   * @tparam n Matrix dimension
   * @param A Positive definite matrix in packed format (lower triangular)
   * @param x Vector of size n
   * @param y Vector of size n
   */
  template <size_t n>
  static void diagsum(mehrotra::PDMat<float, n> &A,
                      const std::array<float, n> &x,
                      const std::array<float, n> &y) {
    size_t skip = 0;
    for (size_t idx = 0; idx < n; idx++) {
      A.mat[skip] += x[idx] + y[idx];
      skip += n - idx;
    }
  }

  /**
   * @brief Add one vector to the diagonal of a diagonal matrix
   *
   * Computes A<-A + I*x, where I is the identity matrix
   *
   * @tparam n Matrix dimension
   * @param A Diagonal matrix of dimension n
   * @param x Vector of size n
   */
  template <size_t n>
  static void diagsum(mehrotra::DiagMat<float, n> &A,
                      const std::array<float, n> &x) {
    for (size_t idx = 0; idx < n; idx++) {
      A.mat[idx] += x[idx];
    }
  }

  /**
   * @brief Add two vectors to the diagonal of a diagonal matrix
   *
   * Computes A<-A+I*(x+y)
   *
   * @tparam n Matrix dimension
   * @param A Diagonal matrix of dimension n
   * @param x Vector of size n
   * @param y Vector of size n
   */
  template <size_t n>
  static void diagsum(mehrotra::DiagMat<float, n> &A,
                      const std::array<float, n> &x,
                      const std::array<float, n> &y) {
    for (size_t idx = 0; idx < n; idx++) {
      A.mat[idx] += x[idx] + y[idx];
    }
  }

  /**
   * @brief Rank-1 update of a positive definite matrix
   *
   * Computes A<-A + alpha*x*x'
   *
   * @tparam n Matrix dimension
   * @param alpha Scalar
   * @param x Vector of size n
   * @param A Positive definite matrix in packed format (lower triangular)
   */
  template <size_t n>
  static void rank1(const float alpha, mehrotra::PDMat<float, n> &A,
                    const std::array<float, n> &x) {
    constexpr char uplo{'L'};
    constexpr int N{n}, inc{1};
    sspr_(&uplo, &N, &alpha, x.data(), &inc, A.mat.data());
  }

  /**
   * @brief Rank-k update of a positive definite matrix
   *
   * Compute A<-A + alpha*X*X'
   *
   * @tparam n Matrix dimension
   * @tparam m Rank of the update matrix X*X'
   * @param alpha Scalar
   * @param X Rectangular matrix of dimension (n x m)
   * @param A Positive definite matrix in packed format (lower triangular)
   */
  template <size_t n, size_t m>
  static void rankk(const float alpha, mehrotra::PDMat<float, n> &A,
                    const mehrotra::GeMat<float, n, m> &X) {
    constexpr char uplo{'L'};
    constexpr int N{n}, inc{1};
    for (size_t idx = 0; idx < m; idx++) {
      sspr_(&uplo, &N, &alpha, X.mat.data() + idx * N, &inc, A.mat.data());
    }
  }

  /**
   * @brief Rank-k update of a positive definite matrix
   *
   * Compute A<-A + alpha*X*X', where alpha is a vector of coefficients
   *
   * @tparam n Matrix dimension
   * @tparam m Rank of the update matrix X*X'
   * @param alpha Vector
   * @param X Rectangular matrix of dimension (n x m)
   * @param A Positive definite matrix in packed format (lower triangular)
   */
  template <size_t n, size_t m>
  static void rankk(const std::array<float, m> &alpha,
                    mehrotra::PDMat<float, n> &A,
                    const mehrotra::GeMat<float, n, m> &X) {
    constexpr char uplo{'L'};
    constexpr int N{n}, inc{1};
    for (size_t idx = 0; idx < m; idx++) {
      sspr_(&uplo, &N, &alpha[idx], X.mat.data() + idx * N, &inc, A.mat.data());
    }
  }

  /**
   * @brief Rank-k update of a positive definite matrix
   *
   * Compute A<-A + alpha*X'*X
   *
   * @tparam n Matrix dimension
   * @tparam m Rank of the update matrix X'*X
   * @param alpha Scalar
   * @param X Rectangular matrix of dimension (m x n)
   * @param A Positive definite matrix in packed format (lower triangular)
   */
  template <size_t n, size_t m>
  static void rankkt(const float alpha, mehrotra::PDMat<float, n> &A,
                     const mehrotra::GeMat<float, m, n> &X) {
    constexpr char uplo{'L'};
    constexpr int N{n}, inc{m};
    for (size_t idx = 0; idx < m; idx++) {
      sspr_(&uplo, &N, &alpha, X.mat.data() + idx, &inc, A.mat.data());
    }
  }

  /**
   * @brief Rank-k update of a positive definite matrix
   *
   * Compute A<-A + alpha*X'*X, where alpha is a vector of coefficients
   *
   * @tparam n Matrix dimension
   * @tparam m Rank of the update matrix X'*X
   * @param alpha Scalar
   * @param X Rectangular matrix of dimension (m x n)
   * @param A Positive definite matrix in packed format (lower triangular)
   */
  template <size_t n, size_t m>
  static void rankkt(const std::array<float, m> &alpha,
                     mehrotra::PDMat<float, n> &A,
                     const mehrotra::GeMat<float, m, n> &X) {
    constexpr char uplo{'L'};
    constexpr int N{n}, inc{m};
    for (size_t idx = 0; idx < m; idx++) {
      sspr_(&uplo, &N, &alpha[idx], X.mat.data() + idx, &inc, A.mat.data());
    }
  }

  /**
   * @brief Addition of PDMat and PDMat
   *
   * Performs the operation A <- A+B, where A and B are positive definite
   * matrices in packed form
   *
   * @tparam n Matrix dimension
   * @param A Positive definite, packed matrix
   * @param B Positive definite, packed matrix
   */
  template <size_t n>
  static void matsum(PDMat<float, n> &A, const PDMat<float, n> &B) {
    vecsum(float{1}, B.mat, A.mat);
  }

  /**
   * @brief Addition of PDMat and DiagMat
   *
   * Performs the operation A <- A+B, where A is positive definite (in packed
   * form) and B is diagonal
   *
   * @tparam n Matrix dimension
   * @param A Positive definite, packed matrix
   * @param B Positive definite, packed matrix
   */
  template <size_t n>
  static void matsum(PDMat<float, n> &A, const DiagMat<float, n> &B) {
    size_t skip{0};
    for (size_t idx = 0; idx < n; idx++) {
      A.mat[skip] += B.mat[idx];
      skip += n - idx;
    }
  }
};

} // namespace detail
} // namespace mehrotra

#endif